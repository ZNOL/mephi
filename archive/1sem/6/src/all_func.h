typedef struct Item
{
    char data;
    struct Item *prev;
    struct Item *next;
} Item;

typedef struct List {
    Item *head;
    Item *tail;
} List;

List *list_create();
List *list_input();
int list_insert(List *list, char data);
void list_print(List *list) ;
void list_clear(List *list);
void list_reverse(Item *left, Item *right);
List *list_processing(List *list);
void *list_processing2(List *list);

