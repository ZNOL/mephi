#include <stdio.h>
#include <stdlib.h>
#include "all_func.h"

List *list_create()
{
    return (List *)calloc(1, sizeof(List));
}

int list_insert(List *list, char data)
{
    Item *ptr = list->tail;

    Item *new = (Item *)malloc(sizeof(Item));
    if (!new) {
        return -1;
    }

    new->data = data;
    new->prev = NULL;
    new->next = NULL;
    if (ptr) {
        ptr->next = new;
        new->prev = ptr;
    }   
    else {
        list->head = new;
    }    
    list->tail = new;

    return 0;
}

void list_print(List *list) 
{
    printf("\"");
    Item *ptr = list->head;
    do
    {
        if (ptr) printf("%c", ptr->data);
        ptr = ptr->next;
    } while (ptr);    
    printf("\"\n");
}

void list_clear(List *list)
{
    Item *ptr = list->head;
    while (ptr) {
        Item *tmp_ptr = ptr->next;
        free(ptr);
        ptr = tmp_ptr;
    }
    free(list);
}

List *list_input()
{
    printf("Введите строку\n\n");
    char chr = getchar(), prev = -1;
    List *list = list_create();
    while (chr != -1 && chr != '\n')
    {
        if (prev != ' ' || (prev == ' ' && chr != ' ')) {
            list_insert(list, chr);
        }

        prev = chr;
        chr = getchar();
    }

    return list;
}

List *list_processing(List *list)
{
    List *new_list = list_create();

    Item *cur_ptr = list->tail;
    while (cur_ptr) {
        Item *last_ptr = cur_ptr, *prev = NULL;
        while (cur_ptr && cur_ptr->data != ' ')
        {
            prev = cur_ptr;
            cur_ptr = cur_ptr->prev;
        }
        if (cur_ptr) cur_ptr = cur_ptr->prev;

        Item *first_prt = prev;
        if (!first_prt) first_prt = last_ptr;

        while (1) {
            list_insert(new_list, first_prt->data);

            if (first_prt == last_ptr) {
                break;
            }
            first_prt = first_prt->next;
        }
        list_insert(new_list, ' ');
    }
    
    return new_list;
}

void list_reverse(Item *left, Item *right)
{
    while (left != right && left->prev != right)
    {
        char tmp = left->data;
        left->data = right->data;
        right->data = tmp;

        left = left->next;
        right = right->prev;
    }    
}

void *list_processing2(List *list)
{
    list_reverse(list->head, list->tail);

    Item *cur_ptr = list->head;
    while (cur_ptr) {
        Item *first_prt = cur_ptr, *prev = NULL;
        while (cur_ptr && cur_ptr->data != ' ') {
            prev = cur_ptr;
            cur_ptr = cur_ptr->next;
        }
        if (cur_ptr) cur_ptr = cur_ptr->next;

        // Item *left = first_prt, *right = prev;
        list_reverse(first_prt, prev);
    }
}
