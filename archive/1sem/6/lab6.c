#include <stdio.h>
#include <stdlib.h>
#include "src/all_func.h"

int main()
{    
    while (1) {
        List *list = list_input();

        if (!list->head) {
            free(list);
            break;
        }

        // List *new_list = list_processing(list);

        printf("Введённая строка: ");
        list_print(list);
        
        printf("Результат алгоритма: ");

        list_processing2(list);
        list_print(list);

        // list_print(new_list);

        list_clear(list);
        
        // list_clear(new_list);
    }
    
    return 0;
}