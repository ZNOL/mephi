#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <readline/readline.h>

int check_word(char str_in[])
{
    char temp[] = "eyuioaёуеыаоэяию";

    int idx = 0, ans = 0;
    while (str_in[idx]) {
        for (int j = 0; j < 16; ++j) {
            if ((char)tolower(str_in[idx]) == temp[j]) {
                ++ans;
                break;
            }
        }
        ++idx;
    }
    return (ans & 1);
}

char *solve(char *in)
{
    int ans_len = 0;
    char *ans = NULL;
    char *cur_str = strtok(in, " \t,-");
    while (cur_str) {
        if (check_word(cur_str)) {
            int word_len = strlen(cur_str);

            ans = (char *)realloc(ans, word_len + ans_len + 1);
            memcpy(ans + ans_len, cur_str, word_len);

            ans[word_len + ans_len] = ' ';
            ans_len = word_len + ans_len + 1;

        }
        cur_str = strtok(NULL, " \t,-");
    }
    if (ans_len) {
        ans[ans_len - 1] = '\0';
        free(cur_str);
    }
    else {
        ans = (char *)calloc(1, sizeof(char));
    }

    return ans;
}


int main()
{
    char *str_in = (char *)readline("Input string: ");
    while (str_in) {
        printf("OLD STR = \"%s\"\n", str_in);
        char *str_out = solve(str_in);

        printf("NEW STR = \"%s\"\n\n", str_out);

        free(str_in);
        free(str_out);

        str_in = (char *)readline("Input string: ");
    }
    free(str_in);

    return 0;
}
