#include <stdio.h>
#include <stdlib.h>

void memcpy_(char *out, char *in, int count)
{
    for (int i = 0; i < count; ++i) {
        out[i] = in[i];
    }
}

int strlen_(char str_in[])
{
    int ans = 0;
    while (str_in[ans]) {
        ++ans;
    }
    return ans;
}

char *readline_()
{
    char buf[BUFSIZ] = {0};
    char *res = NULL;
    int len = 0;
    int n = 0;

    do {
        n = scanf("%80[^\n]", buf);  
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        }
        else if (n > 0) {
            int chunk_len = strlen_(buf);
            int str_len = len + chunk_len;
            res = (char *)realloc(res, str_len + 1);
            memcpy_(res + len, buf, chunk_len);
            len = str_len;
        }
        else {
            scanf("%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    }
    else {
        res = (char *)calloc(1, sizeof(char));
    }

    return res;
}

int tolower_(int c)
{
    if (!(97 <= c && c <= 122)) {
        return c + 32;
    }
    else {
        return c;
    }
}

char *strchr_(const char * s, const char c) 
{
    while (*s && *s != c) {
        ++s;
    }        
    return (*s) ? (char*)s : NULL;
}
 
char *strtok_(char * str, const char * d) 
{
    static char *nxt_idx;
    
    if ( str ) {
        nxt_idx = str;
        while (*nxt_idx && strchr_(d, *nxt_idx)) {
            *nxt_idx++ = '\0';
        }
    }
    
    if (!*nxt_idx) return NULL;
    
    str = nxt_idx;    
    while (*nxt_idx && !strchr_(d, *nxt_idx)) {
        ++nxt_idx;
    }
    while (*nxt_idx && strchr_(d, *nxt_idx)) {
        *nxt_idx++ = '\0';
    }
    
    return str;
}
