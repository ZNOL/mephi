void memcpy_(char *out, char *in, int count);

int strlen_(char str_in[]);

char *readline_();

int tolower_(int c);

char *strchr_(const char * s, const char c);
 
char *strtok_(char * str, const char * delim);

int check_word(char str_in[]);

char *solve(char *in);