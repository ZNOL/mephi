#include <stdio.h>
#include <stdlib.h>
#include "mystring.h"

int main()
{
    char *str_in = (char *)readline_();
    while (str_in) {
        printf("OLD STR = \"%s\"\n", str_in);
        char *str_out = solve(str_in);

        printf("NEW STR = \"%s\"\n\n", str_out);

        free(str_in);   
        free(str_out);

        str_in = (char *)readline_();
    }
    free(str_in);

    return 0;
}