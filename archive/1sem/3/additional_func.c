#include <stdio.h>
#include <stdlib.h>
#include "mystring.h"

int check_word(char str_in[])
{
    char temp[] = "eyuioaёуеыаоэяию";

    int idx = 0, ans = 0;
    while (str_in[idx]) {
        for (int j = 0; j < 16; ++j) {
            if ((char)tolower_(str_in[idx]) == temp[j]) {
                ++ans;
                break;
            }
        }
        ++idx;
    }
    return (ans & 1);
}

char *solve(char *in)
{
    int ans_len = 0;
    char *ans = NULL;

    char *cur_str = strtok_(in, " ");
    while (cur_str) {
        if (check_word(cur_str)) {
            int word_len = strlen_(cur_str);

            ans = (char *)realloc(ans, word_len + ans_len + 1);
            memcpy_(ans + ans_len, cur_str, word_len);

            ans[word_len + ans_len] = ' ';
            ans_len = word_len + ans_len + 1;

        }
        cur_str = strtok_(NULL, " ");
    }
    if (ans_len) {
        ans[ans_len - 1] = '\0';
        free(cur_str);
    }
    else {
        ans = (char *)calloc(1, sizeof(char));
    }

    return ans;
}