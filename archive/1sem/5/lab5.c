#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "src/all_func.h"


int main()
{
    const char menu[][128] = {
        "", "1. Чтение из консоли", "2. Чтение из бинарного файла", "3. Генерация случайных данных",
        "4. Запись исходных в консоль", "5. Запись результата в консоль", "6. Запись исходных в bin файл", "7. Запись результата в bin файл",
        "8. Вставка строки в матрицу", "9. Вставка столбца в матрицу",
        "10. Удаление строки из матрицы", "11. Удаление столбца из матрицы",
        "12. Нахождение ранга матрицы", "13. Очистка матрицы", "14. Завершение работы", ""
    };

    enum MENU mn = 0;

    int ans = -1, n = 0, m = 0;
    int **arr = NULL;

    while (mn != Exit)
    {
        for (int i = 0; i < 16; ++i) {
            printf("%s\n", menu[i]);
        }

        mn = read_int(stdin, "Введите пункт для выбора меню: ");
        switch (mn)
        {
        case ConsoleRead:   
            arr = clear_arr(arr, &n, &m);
            arr = input(stdin, arr, &n, &m);         
            break;
        case BinRead:
            arr = clear_arr(arr, &n, &m);
            arr = file_input(arr, &n, &m);
            break;
        case RandomInput:
            arr = clear_arr(arr, &n, &m);
            arr = random_input(arr, &n, &m);
            break;
        case ConsoleInWrite:
            output(arr, &n, &m);
            break;
        case ConsoleOutWrite:
            if (ans != -1) {
                printf("Ранг матрицы = %d\n", ans);
            }
            else {
                printf("Матрица не была введена или ранг ещё не посчитан\n");
            }
            break;
        case BinInWrite:
            file_output(arr, &n, &m, &ans, 1);
            break;
        case BinOutWrite:
            if (ans == -1) {
                printf("Матрица не была введена или ранг ещё не посчитан\n");
                break;
            }
            file_output(arr, &n, &m, &ans, 2);
            break;
        case InsertRow:
            arr = insert(arr, &n, &m, 1);
            break;
        case InsertColumn:
            arr = insert(arr, &n, &m, 2);
            break;
        case DeleteRow:
            arr = delete(arr, &n, &m, 1);
            break;
        case DeleteColumn:
            arr = delete(arr, &n, &m, 2);
            break;
        case RankOfMatrix:
            ans = find_rank_of_matrix(arr, n, m);
            break;
        case Clear:
            arr = clear_arr(arr, &n, &m);
            break;
        case Exit:
            arr = clear_arr(arr, &n, &m);
            break;
        default:
            printf("Invalid input\n");
            break;
        }
    }

    return 0;
}