#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "all_func.h"

int **clear_arr(int **arr, int *n, int *m)
{
    if ((*n) <= 0) return arr;
    for (int i = 0; i < *n; ++i) {
        if (arr[i]) free(arr[i]);
    }
    free(arr);
    *n = 0, *m = 0;
    return arr;
}

int **delete(int **arr, int *n, int *m, int x)
{
    if (x == 1) {
        int idx = read_int(stdin, "Введите номер строки для удаления: ");

        if (!(1 <= idx && idx <= *n)) {
            printf("Неверный номер\n");
            return arr;
        }
        --idx;
        
        free(arr[idx]);

        if (idx + 1 < *n) {
            memmove(&arr[idx], &arr[idx + 1], sizeof(int *) * (*n - (idx + 1)));
        }

        *n -= 1;
        arr = (int **)realloc(arr, sizeof(int *) * (*n));

        return arr;
    }
    else {
        int idx = read_int(stdin, "Введите номер столбца для удаления: ");

        if (!(1 <= idx && idx <= *m)) {
            printf("Неверный номер\n");
            return arr;
        }
        --idx;

        for (int i = 0; i < *n; ++i) {
            if (idx + 1 < *m) {
                memmove(&arr[i][idx], &arr[i][idx + 1], sizeof(int) * (*m - (idx + 1)));
            }
            arr[i] = (int *)realloc(arr[i], sizeof(int) * (*m - 1));
        }
        *m -= 1;

        return arr;
    }
}
