#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "all_func.h"

char *readline_(FILE *stream, const char *prompt)
{
    if (stream == stdin) printf("%s", prompt);

    char buf[BUFSIZ] = {0};
    char *res = NULL;
    int len = 0;
    int n = 0;

    do {
        n = fscanf(stream, "%8192[^\n]", buf);  
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        }
        else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            res = (char *)realloc(res, str_len + 1);
            memcpy(res + len, buf, chunk_len);
            len = str_len;
        }
        else {
            fscanf(stream, "%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    }
    else {
        res = (char *)calloc(1, sizeof(char));
    }

    return res;
}

long long int read_int(FILE *stream, const char *promt) 
{
    char *input = readline_(stream, promt);

    // printf("\n\n\"%s\"\n\n", input);

    if (!input) {
        free(input);
        return -1;
    }

    int n = strlen(input);

    long long int ans = 0;
    for (int i = 0; i < n; ++i) {
        if (!(48 <= input[i] && input[i] <= 57)) {
            free(input);
            return -1;
        }
        ans = 10LL * ans + (input[i] - 48LL);
    }
    free(input);
    
    if (ans >= 0) return ans;
    else return -1;
}

int **input(FILE *stream, int **arr, int *n, int *m)
{
    int tmp_n = read_int(stream, "Введите кол-во строк: ");
    int tmp_m = read_int(stream, "Введите кол-во столбцов: ");

    if (tmp_n <= 0 || tmp_m <= 0) {
        printf("Неверно заданы размеры матрицы\n");
        return arr;
    }

    if (stream == stdin) printf("Введите %d строк с %d чисел в каждой строке\n", tmp_n, tmp_m);

    *m = tmp_m;
    arr = (int **)malloc(sizeof(int *) * tmp_n);
    for (int i = 0; i < tmp_n; ++i) {
        arr[i] = (int *)malloc(sizeof(int) * tmp_m);
        for (int j = 0; j < tmp_m; ++j) {
            if (fscanf(stream, "%d", &arr[i][j]) != 1) {
                if (stream == stdin) {
                    printf("Неверно введено число. Повторите снова.\n");
                    scanf("%*[^\n]");
                    --j;
                }
                else {
                    printf("Ошибка считывания\n");
                    arr = clear_arr(arr, n, m);
                    return arr;
                }                
            }
        }
        *n += 1;
    }
    scanf("%*[^\n]");
    getchar();

    return arr;
}

int **file_input(int **arr, int *n, int *m)
{
    char *filename = readline_(stdin, "Введити название файла: ");

    FILE *file = fopen(filename, "rb");
    free(filename);

    if (file) {
        fseek(file, 0, SEEK_END);
        int size = ftell(file);
        fseek(file, 0, SEEK_SET);

        int tmp_n = -1, tmp_m = -1;
        fread(&tmp_n, sizeof(int), 1, file);
        fread(&tmp_m, sizeof(int), 1, file);
        if (tmp_n <= 0 || tmp_m <= 0 || size < (2 + tmp_n * tmp_m) * sizeof(int)) {
            printf("Ошибка при считывании\n");
            return arr;
        }

        arr = (int **)malloc(sizeof(int *) * (tmp_n));
        for (int i = 0; i < tmp_n; ++i) {
            arr[i] = (int *)malloc(sizeof(int) * tmp_m);
            fread((int *)arr[i], sizeof(int), tmp_m, file);
        }
        *n = tmp_n, *m = tmp_m;

        fclose(file);
        return arr;
    }
    else {
        printf("Файл не найден!\n");
        return arr;
    }
}

int **random_input(int **arr, int *n, int *m) 
{
    int tmp_n = read_int(stdin, "Введите кол-во строк: ");
    int tmp_m = read_int(stdin, "Введите кол-во стобцов: ");

    if (tmp_n <= 0 || tmp_m <= 0) {
        printf("Неверно заданы размеры матрицы\n");
        return arr;
    }

    *n = tmp_n, *m = tmp_m;
    arr = (int **)malloc(sizeof(int *) * tmp_n);
    for (int i = 0; i < tmp_n; ++i) {
        arr[i] = (int *)malloc(sizeof(int) * tmp_m);
        for (int j = 0; j < tmp_m; ++j) {
            arr[i][j] = rand();
        }
    }

    return arr;
}
