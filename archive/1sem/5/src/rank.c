#include <stdio.h>
#include <stdlib.h>


int rank_of_matrix(int **arr, int *n, int *m)
{
    if (n <= 0 || m <= 0) return -1;

    int *used = (int *)calloc(*n, sizeof(int) * (*n));
    int rank = (*n < *m) ? *m : *n;
    for (int j = 0; j < *m; ++j) {
        int i;
        for (i = 0; i < *n; ++i) {
            if (!used[i] && arr[i][j] > 0) {
                break;
            }
        }
        if (i == *n) {
            --rank;
        }
        else {
            used[i] = 1;
            for (int p = j + 1; p < *m; ++p) {
                arr[i][p] /= arr[i][j];
            }
            for (int k = 0; k < *n; ++k) {
                if (k != i && arr[k][j] > 0) {
                    for (int p = j + 1; p < *m; ++p) {
                        arr[k][p] -= arr[i][p] * arr[k][j];
                    }
                }
            }
        }
    }
    free(used);
    return rank;
}

int **build_minor(int **arr, int n, int f_i, int f_j)
{
    int **new_arr = (int **)malloc(sizeof(int *) * (n - 1));
    for (int i = 0; i < n - 1; ++i) {
        new_arr[i] = (int *)malloc(sizeof(int) * (n - 1));
    }

    for (int i = 0, di = 0; i < n - 1; ++i) {
        int dj = 0;
        if (i == f_i) di = 1;
        for (int j = 0; j < n - 1; ++j) {
            if (j == f_j) dj = 1;
            new_arr[i][j] = arr[i + di][j + dj];
        }
    }

    return new_arr;
}

int determinant(int **arr, int k)
{
    int ans = 0;
    if (k == 1) {
        ans = arr[0][0];
    }
    else if (k == 2) {
        ans = arr[0][0] * arr[1][1] - arr[0][1] * arr[1][0];
    }
    else {
        int x = 1;
        for (int i = 0; i < k; ++i) {
            int **tmp_arr = build_minor(arr, k, 0, i);
            ans += x * arr[0][i] * determinant(tmp_arr, k - 1);
            x = -x;           

            for (int _i = 0; _i < k - 1; ++_i) {
                free(tmp_arr[_i]);
            }
            free(tmp_arr);
        }
    }
    return ans;
}

int check(int **arr, int n, int m, int k)
{
    for (int i = 0; i < n - k + 1; ++i) {
        for (int j = 0; j < m - k + 1; ++j) {
            
            int **tmp_arr = (int **)malloc(sizeof(int *) * k);
            for (int _i = 0; _i < k; ++_i) {
                tmp_arr[_i] = (int *)malloc(sizeof(int) * k);
                for (int _j = 0; _j < k; ++_j) {
                    tmp_arr[_i][_j] = arr[i + _i][j + _j];
                }
            }

            int d = determinant(tmp_arr, k);

            for (int _i = 0; _i < k; ++_i) {
                free(tmp_arr[_i]);
            }
            free(tmp_arr);
            
            if (d != 0) {
                return 1;
            }
        }
    }

    return 0;
}

int find_rank_of_matrix(int **arr, int n, int m)
{
    int l = -1, r = ((n < m) ? n : m) + 1, mid;
    while (r - l > 1) {
        mid = (l + r) >> 1;

        if (check(arr, n, m, mid)) {
            l = mid;
        }
        else {
            r = mid;
        }
    }
    if (l == -1) l = 0;

    printf("%d\n", l);
    return l;
}
