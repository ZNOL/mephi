#include <stdio.h>
#include <stdlib.h>
#include "all_func.h"

void output(int **arr, int *n, int *m)
{
    printf("\nМатрица %d x %d\n", *n, *m);
    for (int i = 0 ; i < *n; ++i) {
        for (int j = 0; j < *m; ++j) {
            printf("%5d ", arr[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}


void file_output(int **arr, int *n, int *m, int *ans, int x)
{
    if (*n == 0 || *m == 0) {
        printf("Пустая матрица!\n");
        return;
    }

    char *filename = readline_(stdin, "Введите название файла: ");

    FILE *file = fopen(filename, "wb");
    free(filename);

    if (x == 1) {
        fwrite(n, sizeof(int), 1, file);
        fwrite(m, sizeof(int), 1, file);
        for (int i = 0; i < *n; ++i) {
            fwrite(arr[i], sizeof(int), *m, file);
        }
    }
    else {
        fwrite(ans, sizeof(int), 1, file);
    }

    fclose(file);

    return;
}
