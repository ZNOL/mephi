#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "all_func.h"

int **insert(int **arr, int *n, int *m, int x)
{
    if (x == 1) {
        int idx = read_int(stdin, "Введите номер строки для встравки: ");
        if (!(1 <= idx && idx <= *n + 1)) {
            printf("Ввёден неверный номер!\n");
            return arr;
        }
        --idx;

        int *tmp = (int *)malloc(sizeof(int) * (*m));
        printf("Введите %d чисел через пробел\n", *m);
        for (int i = 0; i < *m; ++i) {
            if (scanf("%d", &tmp[i]) != 1) {
                printf("Неверно введено число. Повторите снова.\n");
                scanf("%*[^\n]");
                --i;    
            }
        }

        *n += 1;
        arr = (int **)realloc(arr, sizeof(int *) * (*n));
        if (idx + 1 < *n) {
            memmove(&arr[idx + 1], &arr[idx], sizeof(int *) * (*n - (idx + 1)));
        }
        arr[idx] = tmp;

        return arr;
    }
    else {
        int idx = read_int(stdin, "Введите номер столбца для встравки: ");
        if (!(1 <= idx && idx <= *m + 1)) {
            printf("Ввёден неверный номер!\n");
            return arr;
        }
        --idx;

        int *tmp = (int *)malloc(sizeof(int) * (*n));
        printf("Введите %d чисел через пробел\n", *n);
        for (int j = 0; j < *n; ++j) {
            if (scanf("%d", &tmp[j]) != 1) {
                printf("Неверно введено число. Повторите снова.\n");
                scanf("%*[^\n]");
                --j;    
            }
        }

        *m += 1;
        for (int i = 0; i < *n; ++i) {
            arr[i] = (int *)realloc(arr[i], sizeof(int) * (*m));
            if (idx + 1 < *m) {
                memmove(&arr[i][idx + 1], &arr[i][idx], sizeof(int) * (*m - (idx + 1)));
            }
            arr[i][idx] = tmp[i];
        }

        free(tmp);
        return arr;
    }
}
