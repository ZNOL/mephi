#include <stdio.h>

enum MENU {
    ConsoleRead=1,
    BinRead,
    RandomInput,
    ConsoleInWrite,
    ConsoleOutWrite,
    BinInWrite,
    BinOutWrite,
    InsertRow,
    InsertColumn,
    DeleteRow,
    DeleteColumn,
    RankOfMatrix,
    Clear,
    Exit,
};

char *readline_(FILE *stream, const char *prompt);
long long int read_int(FILE *stream, const char *promt);

int **input(FILE *stream, int **arr, int *n, int *m);
int **file_input(int **arr, int *n, int *m);
int **random_input(int **arr, int *n, int *m);

void output(int **arr, int *n, int *m);
void file_output(int **arr, int *n, int *m, int *ans, int x);

int **clear_arr(int **arr, int *n, int *m);
int **delete(int **arr, int *n, int *m, int x);
int **insert(int **arr, int *n, int *m, int x);

int rank_of_matrix(int **arr, int *n, int *m);
int **build_minor(int **arr, int n, int f_i, int f_j);
int determinant(int **arr, int k);
int check(int **arr, int n, int m, int k);
int find_rank_of_matrix(int **arr, int n, int m);
