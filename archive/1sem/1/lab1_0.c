#include <stdio.h>

int is_prime(int n)
{
	int flag = 1;
	if ((n & 1) == 0) {
		if (n != 2) {
			flag = 0;	
		}
	}
	else {
		for (int i = 3; i * i <= n; i += 2) {
			if ((n % i) == 0) {
				flag = 0;
				break;
			}
		}
	}
	return flag;
}

int main()
{
	int n = 2; 
	
	while (n >= 2) {
		printf("Input N\n");
		scanf("%d", &n);
		for (int i = 2; i <= n; ++i) {
			if (is_prime(i)) {
				printf("%d ", i);
			}
		}
		printf("\n");
	}
	printf("The end\n");

	return 0;
}
