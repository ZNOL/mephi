#include <stdio.h>

#define MAX_LENGTH 100000

int m[MAX_LENGTH];

void resheto(int n)
{
	int i = 2;
	while (i * i <= n) {
		if (m[i]) {
			for (int j = i * i; j <= n; j = j + i) {
				m[j] = 0;
			}
		}
		++i;
	}
}

int main()
{
	int n;

	printf("Введите число N\n");
	scanf("%d", &n);
	
	for (int i = 0; i <= n; ++i) m[i] = 1;
	m[0] = m[1] = 0;
	resheto(n);

	int k = 0;
	for (int i = 0; i <= n; ++i) {
		if (m[i]) {
		
			printf("%6d ", i);	
			if (k == 4) {
				printf("\n");
			}
			k = (k + 1) % 5;
		}
	}
	printf("\n");

	return 0;
}
