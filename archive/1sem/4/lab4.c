#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include "src/all_func.h"

int main()
{
    int length = 0;
    subscriber *phone_book = NULL;
    
    char menu[][128] = {
        "1. Чтение с клавиатуры", "2. Чтение из файла", "3. Генерация случайных файлов", 
        "4. Запись в стандартный поток", "5. Запись в текстовый файл", "6. Вставка в массив по индексу", 
        "7. Вставка в массив без нарушения порядка", "8. Удаление N элементов с индекса Idx", "9. Сортировка", 
        "10. Очистить структуру", "11. Завершение программы", ""
    };

    char sortTypeSeq[][128] = {
        "Выберите режим сортировки",
        "1. Чётно-нечётная сортировка",
        "2. Сортировка Шелла",
        "3. Пирамидальная сортровка",
        "4. <-Назад",
        "",
    };

    char sortModeSeq[][128] = {
        "1. Сортировать по ФИО по неубыванию",
        "2. Сортировать по ФИО по невозрастанию",
        "3. Сортировать по номеру по неубыванию",
        "4. Сортировать по номеру по невозрастанию",
        "5. Сортировать по последнему звонку по неубыванию",
        "6. Сортировать по последнему звонку по невозрастанию",
        "",
    };

    enum Menu mn = ConsoleRead;
    enum SortType type = GetBack;
    enum SortMode mode = -1;
    clock_t begin, end;

    while (mn != Exit) {
        // printf("\033c"); 
        for (int i = 0; i < 12; ++i) {
            printf("%s\n", menu[i]);
        }

        mn = read_int(stdin, "");
        switch (mn)
        {
        case ConsoleRead:
            phone_book = clear_book(phone_book, &length);
            phone_book = input(stdin, phone_book, &length);
            getch();
            break;
        case FileRead:
            phone_book = clear_book(phone_book, &length);
            phone_book = file_input(phone_book, &length);
            getch();
            break;
        case RandomData:
            phone_book = clear_book(phone_book, &length);
            phone_book = random_input(phone_book, &length);
            getch();
            break;
        case ConsoleWrite:
            output(phone_book, &length);
            getch();
            break;
        case FileWrite:
            file_output(phone_book, &length);
            getch();
            break;
        case IndexInsert: 
            phone_book = idx_insert(phone_book, &length);
            getch();
            break;
        case SortInsert:  
            phone_book = bin_insert(phone_book, &length, mode);
            getch();
            break;
        case Delete:       
            phone_book = delete(phone_book, &length);
            getch();
            break;
        case Sort:
            if (!length) {
                printf("Sequence is empty\n");
                break;
            }

            for (int i = 0; i < 6; ++i) {
                printf("%s\n", sortTypeSeq[i]);
            }

            type = read_int(stdin, "");
            if (type != GetBack) {
                for (int i = 0; i < 7; ++i) {
                    printf("%s\n", sortModeSeq[i]);
                }

                mode = read_int(stdin, "");
                begin = clock();
                switch (type) 
                {
                    case OddEvenSort:
                        switch (mode) 
                        {
                            case Fio:
                                odd_even_sort(phone_book, length, sizeof(subscriber), compare_fio);
                                break;
                            case FioReversed:
                                odd_even_sort(phone_book, length, sizeof(subscriber), compare_fio_reverse);
                                break;
                            case Number:
                                odd_even_sort(phone_book, length, sizeof(subscriber), compare_number);
                                break;
                            case NumberReversed:
                                odd_even_sort(phone_book, length, sizeof(subscriber), compare_number_reverse);
                                break;
                            case Time:
                                odd_even_sort(phone_book, length, sizeof(subscriber), compare_time);
                                break;
                            case TimeReversed:
                                odd_even_sort(phone_book, length, sizeof(subscriber), compare_time_reverse);
                                break;         
                            default:
                                printf("Invalid input\n");
                                break;                   
                        }
                        break;
                    case ShellSort:
                        switch (mode) 
                        {
                            case Fio:
                                shell_sort(phone_book, length, sizeof(subscriber), compare_fio);
                                break;
                            case FioReversed:
                                shell_sort(phone_book, length, sizeof(subscriber), compare_fio_reverse);
                                break;
                            case Number:
                                shell_sort(phone_book, length, sizeof(subscriber), compare_number);
                                break;
                            case NumberReversed:
                                shell_sort(phone_book, length, sizeof(subscriber), compare_number_reverse);
                                break;
                            case Time:
                                shell_sort(phone_book, length, sizeof(subscriber), compare_time);
                                break;
                            case TimeReversed:
                                shell_sort(phone_book, length, sizeof(subscriber), compare_time_reverse);
                                break;         
                            default:
                                printf("Invalid input\n");
                                break;                   
                        }
                        break;
                    case HeapSort:
                        switch (mode) 
                        {
                            case Fio:
                                heap_sort(phone_book, length, sizeof(subscriber), compare_fio);
                                break;
                            case FioReversed:
                                heap_sort(phone_book, length, sizeof(subscriber), compare_fio_reverse);
                                break;
                            case Number:
                                heap_sort(phone_book, length, sizeof(subscriber), compare_number);
                                break;
                            case NumberReversed:
                                heap_sort(phone_book, length, sizeof(subscriber), compare_number_reverse);
                                break;
                            case Time:
                                heap_sort(phone_book, length, sizeof(subscriber), compare_time);
                                break;
                            case TimeReversed:
                                heap_sort(phone_book, length, sizeof(subscriber), compare_time_reverse);
                                break;         
                            default:
                                printf("Invalid input\n");
                                break;                   
                        }
                        break;
                    default:
                        printf("Invalid input\n");
                        break; 
                }
            }          
            end = clock();
            printf("Время работы: %.15LF\n", (long double)(end - begin) / CLOCKS_PER_SEC);

            getch();
            break;        
        case Clear:
            phone_book = clear_book(phone_book, &length);
            getch();
        case Exit:
            phone_book = clear_book(phone_book, &length);
            break;
        case -1:
            printf("Invalid input\n");
            break;
        default:
            printf("Invalid input\n");
            break;
        }
    }

    return 0;
}
