#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "all_func.h"

char *readline_(FILE *stream, const char *prompt)
{
    if (stream == stdin) printf("%s", prompt);

    char buf[BUFSIZ] = {0};
    char *res = NULL;
    int len = 0;
    int n = 0;

    do {
        n = fscanf(stream, "%8192[^\n]", buf);  
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        }
        else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            res = (char *)realloc(res, str_len + 1);
            memcpy(res + len, buf, chunk_len);
            len = str_len;
        }
        else {
            fscanf(stream, "%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    }
    else {
        res = (char *)calloc(1, sizeof(char));
    }

    return res;
}

int check_str(char *in)
{
    if (!in) {
        return 0;
    }
    int n = strlen(in);
    for (int i = 0; i < n; ++i) {
        if (!(65 <= in[i] && in[i] <= 90 || 97 <= in[i] && in[i] <= 122 || in[i] == ' ' || in[i] == '+' && i == 0 || 48 <= in[i] && in[i] <= 57)) {
            return 0;
        }
    }
    return 1;
}

long long int read_int(FILE *stream, const char *promt) 
{
    char *input = readline_(stream, promt);

    if (!input) {
        free(input);
        return -1;
    }

    int n = strlen(input);

    long long int ans = 0;
    for (int i = 0; i < n; ++i) {
        if (!(48 <= input[i] && input[i] <= 57)) {
            free(input);
            return -1;
        }
        ans = 10LL * ans + (input[i] - 48LL);
    }
    free(input);
    
    if (ans >= 0) return ans;
    else return -1;
}

int make_subsriber(FILE *stream, subscriber *tmp)
{
    char *fio = readline_(stream, "Введите ФИО: ");
    char *number = readline_(stream, "Введите номер: ");
    long long int time = read_int(stream, "Введите время последнего звонка: ");

    if (!check_str(fio) || !check_str(number) || time < 0) {
        free(fio);
        free(number);
        return 0;
    }

    tmp->name = fio;

    int number_len = strlen(number);
    number_len = (number_len > 16) ? 16 : number_len;
    memcpy(tmp->number, number, number_len);
    tmp->number[number_len] = '\0';
    free(number);

    tmp->time = time;

    return 1;
}

subscriber *input(FILE *stream, subscriber *phone_book, int *length)
{
    int n = read_int(stream, "Введите кол-во элементов\n");

    if (n <= 0) {
        printf("Неверное количество\n");
        return phone_book;
    }

    if (stream == stdin) printf("Введите %d элементов\nКаждый элемент на 3 разных строках\n", n);

    phone_book = (subscriber *)malloc(n * sizeof(subscriber));    
    for (int i = 0; i < n; ++i) {
        subscriber tmp;
        int flag = make_subsriber(stream, &tmp);
        
        if (!flag) {
            printf("Неверное задание структуры\n");
            phone_book = clear_book(phone_book, length);
            return phone_book;
        }
        
        phone_book[i] = tmp;

        *length += 1;
    }

    return phone_book;
}

subscriber *file_input(subscriber *phone_book, int *length) 
{
    char *fileName = readline_(stdin, "Введите путь до файла: ");

    FILE *file = fopen(fileName, "r");
    free(fileName);

    if (file) {
        phone_book = input(file, phone_book, length);
        fclose(file);
    }
    else {
        printf("Неверный путь до файла\n");
    }   

    return phone_book;
}

subscriber *random_input(subscriber *phone_book, int *length)
{
    long long int n = read_int(stdin, "Введите количество элементов для генерации: ");

    if (n <= 0) {
        printf("Неверное количество элементов\n");
        return phone_book;
    }

    phone_book = (subscriber *)malloc(n * sizeof(subscriber));
    *length = n;
    for (int i = 0; i < n; ++i) {
        char *tmp_name = NULL;
        
        int l1 = 1 + rand() % 10;
        int l2 = 1 + rand() % 8;
        int l3 = 1 + rand() % 8;

        tmp_name = (char *)realloc(tmp_name, l1 + 1 + l2 + 1 + l3 + 1);
        
        tmp_name[0] = 65 + rand() % (90 - 65 + 1);
        for (int i = 1; i < l1; ++i) {
            tmp_name[i] = 97 + rand() % (122 - 97 + 1);
        }
        tmp_name[l1] = ' ';

        tmp_name[l1 + 1] = 65 + rand() % (90 - 65 + 1);
        for (int i = 1; i < l2; ++i) {
            tmp_name[l1 + 1 + i] = 97 + rand() % (122 - 97 + 1);
        }
        tmp_name[l1 + 1 + l2] = ' ';


        tmp_name[l1 + 1 + l2 + 1] = 65 + rand() % (90 - 65 + 1);
        for (int i = 1; i < l3; ++i) {
            tmp_name[l1 + 1 + l2 + 1 + i] = 97 + rand() % (122 - 97 + 1);
        }
        tmp_name[l1 + 1 + l2 + 1 + l3] = '\0';

        phone_book[i].name = tmp_name;
        phone_book[i].number[0] = '+';
        for (int j = 1; j < 16; ++j) {
            phone_book[i].number[j] = 48 + rand() % (57 - 48 + 1);
        }
        phone_book[i].number[16] = '\0';
        phone_book[i].time = rand() % (int)1e9;
    }

    printf("Было сгенерировано %d элментов\n", n);
    return phone_book;
}
