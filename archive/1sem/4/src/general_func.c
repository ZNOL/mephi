#include <stdlib.h>
#include <string.h>
#include "all_func.h"

void swap(void *a, void *b, size_t size)
{
    void *tmp = malloc(size);
    memcpy(tmp, a, size);
    memcpy(a, b, size);
    memcpy(b, tmp, size);
    free(tmp);
}

int compare_fio(const void *a, const void *b) 
{
    const char *str_a = (const char *)((subscriber *)a)->name;
    const char *str_b = (const char *)((subscriber *)b)->name;
    return strcmp(str_a, str_b) > 0;
}

m[i]  *(m + size(device) * i)

int compare_number(const void *a, const void *b) 
{
    const char *str_a = (const char *)((subscriber *)a)->number;  
    const char *str_b = (const char *)((subscriber *)b)->number;
    return strcmp(str_a, str_b) > 0;
}

int compare_time(const void *a, const void *b) 
{
    int first = ((subscriber *)a)->time, second = ((subscriber *)b)->time;
    return (first > second);
}

int compare_fio_reverse(const void *a, const void *b) 
{
    const char *str_a = (const char *)((subscriber *)a)->name;
    const char *str_b = (const char *)((subscriber *)b)->name;
    return strcmp(str_a, str_b) < 0;
}

int compare_number_reverse(const void *a, const void *b) 
{
    const char *str_a = (const char *)((subscriber *)a)->number;  
    const char *str_b = (const char *)((subscriber *)b)->number;
    return strcmp(str_a, str_b) < 0;
}

int compare_time_reverse(const void *a, const void *b) 
{
    int first = ((subscriber *)a)->time, second = ((subscriber *)b)->time;
    return (first < second);
}
