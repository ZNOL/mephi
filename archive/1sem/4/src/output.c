#include <stdio.h>
#include <stdlib.h>
#include "all_func.h"

void output(subscriber *phone_book, int *length)
{
    printf("Количество элементов = %d\n", *length);
    for (int i = 0; i < (*length); ++i) {
        printf("%50s %20s %15d\n", phone_book[i].name, phone_book[i].number, phone_book[i].time);
    }
}

void file_output(subscriber *phone_book, int *length)
{
    char *fileName = readline_(stdin, "Введите путь до файла: ");

    FILE *file = fopen(fileName, "w");
    free(fileName);

    fprintf(file, "%d\n", *length);
    for (int i = 0; i < (*length); ++i) {
        fprintf(file, "%s\n%s\n%d\n", phone_book[i].name, phone_book[i].number, phone_book[i].time);
    }
    fclose(file);
}
