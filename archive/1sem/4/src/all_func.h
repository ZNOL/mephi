#include <stdio.h>

typedef struct subscriber
{
    char *name;
    char number[17];
    long long int time;
} subscriber;

enum Menu {
    ConsoleRead = 1,
    FileRead,
    RandomData,
    ConsoleWrite,
    FileWrite,
    IndexInsert,
    SortInsert,
    Delete,
    Sort,
    Clear,
    Exit,
};
enum SortType {
    OddEvenSort = 1,
    ShellSort,
    HeapSort,
    GetBack,
};
enum SortMode {
    Fio = 1,
    FioReversed,
    Number,
    NumberReversed,
    Time,
    TimeReversed,
};

void swap(void *a, void *b, size_t size);
subscriber *clear_book(subscriber *phone_book, int *length);

char *readline_(FILE *stream, const char *prompt);
int check_str(char *in);
long long int read_int(FILE *stream, const char *promt);
int make_subsriber(FILE *stream, subscriber *tmp);
subscriber *input(FILE *stream, subscriber *phone_book, int *length);
subscriber *file_input(subscriber *phone_book, int *length);
subscriber *random_input(subscriber *phone_book, int *length);

void output(subscriber *phone_book, int *length);
void file_output(subscriber *phone_book, int *length);

subscriber *insert(subscriber *phone_book, int *length, int idx, subscriber *tmp);
subscriber *idx_insert(subscriber *phone_book, int *length);
int bin_search(void *tmp, void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *));
subscriber *bin_insert(subscriber *phone_book, int *length, enum SortMode mode);

subscriber *clear_book(subscriber *phone_book, int *length);
subscriber *delete(subscriber *phone_book, int *length);

void shift_down(int idx, void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *), void *tmp);
void shift_up(int idx, void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *), void *tmp);
void heap_sort(void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *));
void odd_even_sort(void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *));
void shell_sort(void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *));

int compare_fio(const void *a, const void *b); 
int compare_number(const void *a, const void *b);
int compare_time(const void *a, const void *b);
int compare_fio_reverse(const void *a, const void *b);
int compare_number_reverse(const void *a, const void *b);
int compare_time_reverse(const void *a, const void *b);
