#include <stdlib.h>
#include <string.h>
#include "all_func.h"

void shell_sort(void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *)) 
{
    void *tmp = malloc(size);
    int A102549[] = {1, 4, 10, 23, 57, 132, 301, 701, 1750};
    for (int idx = 8; idx >= 0; --idx) {
        int el = A102549[idx];
        for (int i = el; i < nmemb; ++i) {
            for (int j = i - el; j >= 0; j -= el) {
                if (compare(base + (j) * size, base + (j + el) * size)) {
                    memcpy(tmp, base + (j) * size, size);
                    memcpy(base + (j) * size, base + (j + el) * size, size);
                    memcpy(base + (j + el) * size, tmp, size);
                    // swap(base + (j) * size, base + (j + el) * size, size);
                }
            }
        }
    }
    free(tmp);
}
