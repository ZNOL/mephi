#include <stdlib.h>
#include <string.h>
#include "all_func.h"

void odd_even_sort(void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *))
{
    int sorted = 0;
    void *tmp = malloc(size);
    while (!sorted) {
        sorted = 1;
        for (int i = 1; i < nmemb - 1; i += 2) {
            if (compare(base + i * size, base + (i + 1) * size) > 0) {
                memcpy(tmp, base + i * size, size);
                memcpy(base + i * size, base + (i + 1) * size, size);
                memcpy(base + (i + 1) * size, tmp, size);
                // swap(base + i * size, base + (i + 1) * size, size);
                sorted = 0;
            }
        }
        for (int i = 0; i < nmemb - 1; i += 2) {
            if (compare(base + i * size, base + (i + 1) * size) > 0) {
                memcpy(tmp, base + i * size, size);
                memcpy(base + i * size, base + (i + 1) * size, size);
                memcpy(base + (i + 1) * size, tmp, size);
                // swap(base + i * size, base + (i + 1) * size, size);
                sorted = 0;
            }
        }
    }
    free(tmp);
}
