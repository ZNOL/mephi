#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "all_func.h"

subscriber *clear_book(subscriber *phone_book, int *length)
{
    if (!phone_book || *length == 0) return phone_book;
    for (int i = 0; i < (*length); ++i) {
        if (phone_book[i].name) free(phone_book[i].name);
    }
    free(phone_book);
    *length = 0;
    return NULL;
}

subscriber *delete(subscriber *phone_book, int *length)
{
    int startIdx = read_int(stdin, "Введите начальный индекс: ");
    int count = read_int(stdin, "Введите количество элементов для удаления: ");

    if (startIdx < 0 || *length <= startIdx || count <= 0) {
        printf("Ошибка с индексами/количеством\n");
        return phone_book;
    }

    int finishIdx = (startIdx + count < *length) ? startIdx + count : *length;
    for (int i = startIdx; i < finishIdx; ++i) {
        if (phone_book[i].name) free(phone_book[i].name);
    }    
  
    if (startIdx + count < *length) {
        memmove(&phone_book[startIdx], &phone_book[startIdx + count], sizeof(subscriber) * (*length - (startIdx + count)));
    }

    *length -= (finishIdx - startIdx);
    phone_book = (subscriber *)realloc(phone_book, sizeof(subscriber) * (*length));

    return phone_book;
}
