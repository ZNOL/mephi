#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "all_func.h"

subscriber *insert(subscriber *phone_book, int *length, int idx, subscriber *tmp)
{
    *length += 1;
    phone_book = (subscriber *)realloc(phone_book, sizeof(subscriber) * (*length));

    if (idx + 1 < *length) {
        memmove(&phone_book[idx + 1], &phone_book[idx], sizeof(subscriber) * (*length - idx - 1));
    }

    phone_book[idx] = *tmp;
    return phone_book;
}

subscriber *idx_insert(subscriber *phone_book, int *length)
{
    int idx = read_int(stdin, "Введите индекс для вставки: ");
    
    if (idx < 0 || *length < idx) {
        printf("Индекс должен находится в отлезке [0; %d]t\n", *length);
        return phone_book;
    }

    subscriber tmp;
    int flag = make_subsriber(stdin, &tmp);

    if (!flag) {
        printf("Неверное задание структуры\n");
        return phone_book;
    }

    phone_book = insert(phone_book, length, idx, &tmp);
    return phone_book;
}

int bin_search(void *tmp, void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *))
{
    int l = -1, r = nmemb, mid;
    while (r - l > 1) {
        mid = (l + r) >> 1;
        
        if (compare(base + mid * size, tmp)) {
            r = mid;
        }
        else {
            l = mid;
        }
    }
    return r;
}

subscriber *bin_insert(subscriber *phone_book, int *length, enum SortMode mode)
{
    subscriber tmp;
    int flag = make_subsriber(stdin, &tmp);

    if (!flag) {
        printf("Неверный ввод структуры\n");
        return phone_book;
    }

    int idx = -1;
    switch (mode)
    {
    case Fio:
        idx = bin_search(&tmp, phone_book, *length, sizeof(subscriber), compare_fio);
        break;
    case FioReversed:
        idx = bin_search(&tmp, phone_book, *length, sizeof(subscriber), compare_fio_reverse);
        break;
    case Number:
        idx = bin_search(&tmp, phone_book, *length, sizeof(subscriber), compare_number);
        break;
    case NumberReversed:
        idx = bin_search(&tmp, phone_book, *length, sizeof(subscriber), compare_number_reverse);
        break;
    case Time:
        idx = bin_search(&tmp, phone_book, *length, sizeof(subscriber), compare_time);
        break;
    case TimeReversed:
        idx = bin_search(&tmp, phone_book, *length, sizeof(subscriber), compare_time_reverse);
        break;    
    default:
        printf("Не производилось запуска сортировок\n");
        free(tmp.name);
        return phone_book;
        break;
    }

    if (idx != -1) {
        phone_book = insert(phone_book, length, idx, &tmp);
    }

    return phone_book;
}
