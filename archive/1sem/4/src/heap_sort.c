#include <stdlib.h>
#include <string.h>
#include "all_func.h"

void shift_down(int idx, void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *), void *tmp)
{
    int mxIdx = idx;
    if (2 * idx + 1 < nmemb && !compare(base + (mxIdx) * size, base + (2 * idx + 1) * size)) {
        mxIdx = 2 * idx + 1;
    }
    if (2 * idx + 2 < nmemb && !compare(base + (mxIdx) * size, base + (2 * idx + 2) * size)) {
        mxIdx = 2 * idx + 2;
    }
    if (idx != mxIdx) {
        memcpy(tmp, base + (idx) * size, size);
        memcpy(base + (idx) * size, base + (mxIdx) * size, size);
        memcpy(base + (mxIdx) * size, tmp, size);
        // swap(base + (idx) * size, base + (mxIdx) * size, size);
        shift_down(mxIdx, base, nmemb, size, compare, tmp);
    }
}

void shift_up(int idx, void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *), void *tmp)
{
    while (idx > 0 && !compare(base + ((idx - 1) / 2) * size, base + (idx) * size)) {
        memcpy(tmp, base + ((idx - 1) / 2) * size, size);
        memcpy(base + ((idx - 1) / 2) * size, base + (idx) * size, size);
        memcpy(base + (idx) * size, tmp, size);
        // swap(base + ((idx - 1) / 2) * size, base + (idx) * size, size);
        idx = (idx - 1) >> 1;
    }
}

void heap_sort(void *base, size_t nmemb, size_t size, int (*compare)(const void *, const void *))
{
    void *tmp = malloc(size);
    for (int i = nmemb - 1; i >= 0; --i) {
        // shift_up(i, base, nmemb, size, compare, tmp);
        shift_down(i, base, nmemb, size, compare, tmp);
    }
    for (int i = nmemb - 1; i > 0; --i) {
        memcpy(tmp, base, size);
        memcpy(base, base + (i) * size, size);
        memcpy(base + (i) * size, tmp, size);
        // swap(base, base + (i) * size, size);
        shift_down(0, base, i, size, compare, tmp);
    }
    free(tmp);
}
