#include <stdio.h>

long double binpow(long double x, long long int n) 
{
    long double res = 1;
	while (n)
	{
		if (n & 1)
			res *= x;
		x *= x;
		n >>= 1;
	}
	return res;
}

long double solve(long double x, long long int n)
{
	long double t, s = 0;
	t = -((long double)2 / binpow(x, n));
	s += t;
	printf("k = %20d | t = %20.10LF | S = %20.10LF\n", 1, t, s);    
	for (int k = 2; k <= n; ++k) {
		t *= -((long double)(k + 1) * x);
		s += t;
		printf("k = %20d | t = %20.10LF | s = %20.10LF\n", k, t, s);
	}
	return s;
}

int main()
{
    long long int n;
    long double x, s;
	
    printf("Введите число X и число N\n");
    scanf("%LF %lld", &x, &n);
	while (n > 0) {		
		s = solve(x, n);	    	
    	printf("Результат суммирования: %LF\n", s);

		printf("\nВведите число Х и число N\n");
		scanf("%LF %lld", &x, &n);
	}
	return 0;
}
