#include <stdio.h>

#define ld long double

int precision(ld eps)
{
    int ans = 0;
    while (eps < 1) {
        ++ans;
        eps *= 10;
    }
    return ans;
}

int main() 
{
    ld x, eps, t, s = 1, k = 2;

    printf("Enter: X EPS\n");
    scanf("%LF %LF", &x, &eps);

    t = (x * x * x) / ((ld)6);
    do
    {
        s += t;
        t = t * x * x / (2 * k * (2 * k + 1));
        ++k;
        printf("%.20LF\n", s);
    } while (t > eps);

    printf("\n(e^x - e^(-x))/2 = %.*LF\n", precision(eps), s);  

    return 0;
}