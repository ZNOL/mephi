#include <stdio.h>


#define X_MAX 1000
#define X_MIN -1000


int solve(int a, int b, int c, int *x1, int *x2)
{
	if  (!a) {
		return 1;
	}

	if ((b % a) || (c % a)) {
		return 2;
	}

	int k1 = -b / a;
	int k2 = c / a;

	for (int i = X_MIN; i < X_MAX; ++i) {
		for (int j = X_MIN; j < X_MAX; ++j) {
			if (((i + j) == k1) && ((i * j) == k2)){
				*x1 = i, *x2 = j;
				return 0;
			}
		}
	}

	return 2;
}


int main()
{
	int a, b, c;
	int x1, x2, status;

	printf("ax^2 + bx + c = 0\n");
	printf("a = ");
	scanf("%d", &a);
	printf("\nb = ");
	scanf("%d", &b);
	printf("\nc = ");
	scanf("%d", &c);
	printf("\n");

	if ((status = solve(a, b, c, &x1, &x2)) != 0){
		switch (status) {
		case 1:
			printf("Not a quadratic equation\n");
			break;
		case 2:
			printf("No roots found\n");
			break;
		default:
			printf("Should never be reached\n");
		}
		return status;
	}

	if (x1 != x2) {
		printf("Two roots found: %d %d\n", x1, x2);
	}
	else {
		printf("Single root found: %d\n", x1);
	}

	return 0;
}
