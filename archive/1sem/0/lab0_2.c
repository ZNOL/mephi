#include <stdio.h>

#define X_MIN -100
#define X_MAX 100

int main()
{
    int a, b, c, x1, x2;

    printf("ax^2 + bx + c = 0\n");
	printf("a = ");
	scanf("%d", &a);
	printf("\nb = ");
	scanf("%d", &b);
	printf("\nc = ");
	scanf("%d", &c);
	printf("\n");

    if (a != 0) {
        for (int i = X_MIN; i < X_MAX; ++i) {
            for (int j = X_MIN; j < X_MAX; ++j) {
                if (((i + j) == (-b / a)) && ((i * j) == (c / a))) {
                    if (i == j) {
                        printf("Single root found: %d\n", i);
                    }
                    else {
                        printf("Two roots found: %d %d\n", i, j);
                    }
                    return 0;
                }
            }
        }       
    }    
    
    printf("Нет решения\n");

    return 0;
}