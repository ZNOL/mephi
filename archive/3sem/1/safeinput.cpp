#include "safeinput.h"
#include <iostream>

void checkInput() {
    if (std::cin.eof()) {
        throw std::ios_base::failure("End of file is discovered!");
    }
    if (!std::cin.good()) {
        throw std::ios_base::failure("Input error!");
    }
}

void getInt(const char *promt, int &x) {
    std::cout << promt << std::endl;
    std::cin >> x;

    checkInput();
}
