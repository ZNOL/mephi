#ifndef LAB1_MATRIX_H_
#define LAB1_MATRIX_H_

#include "array.h"

struct Sparse_Matrix {
    int last_row = -1;
    int n, m;
    Array v, col, row;

    Sparse_Matrix();

    void clear();

    void add(int item_idx, int row_idx, int col_idx, double val);

    void input();
    void output();

    std::pair<int, int> rowInfo(int row_idx);
    std::pair<double, int> rowMin(int row_idx);

    void formMinMatrix(Sparse_Matrix &nw);

    ~Sparse_Matrix() = default;
};

#endif // LAB1_MATRIX_H_
