#ifndef LAB1_ARRAY_H_
#define LAB1_ARRAY_H_

#include <cstdlib>

struct Array {
private:
    size_t _size;
    size_t capacity;
    double *arr;

    void resize();

public:

    Array(int _cap=10);

    double operator[](size_t idx);

    void push_back(double x);

    void clear();

    double back();

    size_t size();

    ~Array();
};

#endif // LAB1_ARRAY_H_
