#include <iostream>
#include <limits>

#include "matrix.h"
#include "safeinput.hpp"

Sparse_Matrix::Sparse_Matrix() : n(0), m(0) {}

void Sparse_Matrix::add(int item_idx, int row_idx, int col_idx, double val) {
    if (row_idx < last_row) {
        throw std::invalid_argument("Wrong rows order!");
    }
    v.push_back(val);
    col.push_back(col_idx);

    if (last_row < row_idx) {
        row.push_back(item_idx - 1);
        ++last_row;
    }
    while (last_row < row_idx) {
        row.push_back(row.back());
        ++last_row;
    }
}

void Sparse_Matrix::clear() {
    n = 0;
    m = 0;
    last_row = 0;
    v.clear();
    col.clear();
    row.clear();
    row.push_back(0);
}

void Sparse_Matrix::input() {
    clear();

    SafeGet(std::cin, "Input number of rows", n);
    if (n <= 0) {
        throw std::length_error("Wrong rows amount!");
    }

    SafeGet(std::cin, "Input number of columns", m);
    if (m <= 0) {
        throw std::length_error("Wrong columns amount!");
    }

    int temp = 0;
    SafeGet(std::cin, "Input number of elements\n For each element input row_idx, col_idx and value", temp);
    for (int i = 1; i <= temp; ++i) {
        int row_idx, col_idx;
        double value;

        SafeGet(std::cin, "", row_idx);
        SafeGet(std::cin, "", col_idx);
        SafeGet(std::cin, "", value);

        add(i, row_idx, col_idx, value);
    }
    row.push_back(temp);
}


std::pair<int, int> Sparse_Matrix::rowInfo(int row_idx) {
    if (row_idx + 1 < row.size()) {
        return {row[row_idx], row[row_idx + 1]};
    }
    else {
        return {0, -1};
    }
}

std::pair<double, int> Sparse_Matrix::rowMin(int row_idx) {
    std::pair<double, int> ans;
    ans.first = std::numeric_limits<double>::infinity(), ans.second = -1;
    auto info = rowInfo(row_idx);
    for (int j = info.first; j < info.second; ++j) {
        if (ans.first > v[j]) {
            ans.first = v[j];
            ans.second = j;
        }
    }
    return ans;
}

void Sparse_Matrix::formMinMatrix(Sparse_Matrix &nw) {
    nw.clear();
    nw.n = n;
    nw.m = m;
    int cnt = 1;
    for (int i = 0; i < n; ++i) {
        auto info = rowInfo(i);
        auto minInfo = rowMin(i);
        for (int j = std::max(minInfo.second, info.first); j < info.second; ++j) {
            nw.add(cnt++, i, col[j], v[j]);
        }
    }
    nw.row.push_back(cnt - 1);
}

void Sparse_Matrix::output() {
    for (int i = 0; i < n; ++i) {
        auto info = rowInfo(i);
        for (int j = info.first; j < info.second; ++j) {
            std::cout << v[j] << " ";
        }
        std::cout << std::endl;
    }
}
