from random import randint

n = 5
m = 5

cnt = 0
matrix = [[0 for j in range(m)] for i in range(n)]
for i in range(n):
    for j in range(m):
        x = randint(0, 100)
        if x > 70:
            cnt += 1
            matrix[i][j] = x

print(n)
print(m)
print(cnt)
for i in range(n):
    for j in range(m):
        if matrix[i][j] != 0:
            print(i, j, matrix[i][j])


print('-' * 30)

for i in matrix:
    print(*i)

