#include <iostream>
#include "array.h"

void Array::resize() {
    capacity *= 2;
    auto *temp_arr = new double[capacity];
    std::copy(arr, arr + _size, temp_arr);
    delete[] arr;
    arr = temp_arr;
}

Array::Array(int _cap) : capacity(_cap) {
    _size = 0;
    arr = new double[capacity];
}

double Array::operator[](size_t idx) {
    if (idx >= _size) {
        throw std::invalid_argument("Wrong array index");
    }
    return arr[idx];
}

void Array::push_back(double x) {
    if (_size >= capacity) {
        resize();
    }
    arr[_size++] = x;
}

void Array::clear() {
    _size = 0;
}

double Array::back() {
    if (_size <= 0) {
        throw std::runtime_error("Back from empty array!");
    }
    else {
        return arr[_size - 1];
    }
}

size_t Array::size() {
    return _size;
}

Array::~Array() {
    delete[] arr;
}
