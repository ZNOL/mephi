#include <iostream>

#include "matrix.h"

int main () {
    Sparse_Matrix matrix, matrix2;
    bool wasCorrectInput = false;

    while (!wasCorrectInput) {
        try {
            matrix.input();
            wasCorrectInput = true;
        }
        catch (const std::exception &exc) {
            std::cout << "I catch error!" << std::endl;
            std::cout << exc.what() << std::endl;
        }
    }

    std::cout << "First matrix\n";
    matrix.output();

    matrix.formMinMatrix(matrix2);

    std::cout << "Min matrix\n";
    matrix2.output();

    return 0;
}