#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>

#include "iterator.hpp"

static std::vector<int> v = {
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10
};

TEST_CASE("constructor") {
    REQUIRE_NOTHROW(Iterator<int>(&v[0], &v[0], &v[10]));
}

TEST_CASE("begin") {
    Iterator it(v);

    REQUIRE(it.begin() == &v[0]);
}

TEST_CASE("end") {
    Iterator it(v);

    REQUIRE(it.end() == &v[10]);
}

TEST_CASE("postfix operator++ and operator*") {
    Iterator it(v);

    size_t idx = 0;
    while (it != it.end()) {
        REQUIRE(*it == v[idx++]);
        it++;
    }
}

TEST_CASE("infix operator++ and operator*") {
    Iterator it(v);

    size_t idx = 0;
    while (it != it.end()) {
        REQUIRE(*it == v[idx++]);
        ++it;
    }
}

TEST_CASE("operator ==") {
    Iterator it1(v);
    Iterator it2(v);

    REQUIRE(it1 == it2);
    (((it1++)++)++);
    (((it2++)++)++);
    REQUIRE(it1 == it2);
}

TEST_CASE("operator !=") {
    Iterator it1(v);
    Iterator it2(v);
    ++it2;

    REQUIRE(it1 != it2);

    ++(++(++it1));
    ++(++(++it2));
    REQUIRE(it1 != it2);

    ++(++(++it1));
    ++(++(++it2));
    REQUIRE(it1 != it2);
}