#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "array.hpp"

TEST_CASE("constructor") {
    REQUIRE_NOTHROW(Array<int>(1));
    REQUIRE_NOTHROW(Array<std::string>(12));
    REQUIRE_NOTHROW(Array<std::vector<std::string>>(123));
}

TEST_CASE("size") {
    Array<int> a;

    REQUIRE(a.size() == 0);
    REQUIRE(a.capacity() == 1);

    a.push_back(123);

    REQUIRE(a.size() == 1);
    REQUIRE(a.capacity() == 1);

    for (int i = 2; i <= 10; ++i) {
        a.push_back(i);
        REQUIRE(a.size() == i);
    }
    REQUIRE(a.size() == 10);
}

TEST_CASE("operator[]") {
    Array<int> a;

    a.push_back(1);
    a.push_back(2);
    a.push_back(3);

    REQUIRE(a[0] == 1);
    REQUIRE(a[1] == 2);
    REQUIRE(a[2] == 3);
    REQUIRE_THROWS_AS(a[123], std::invalid_argument);
}

TEST_CASE("vector constructor") {
    static std::vector<int> v = {
            1, 2, 3, 4, 5, 6
    };

    Array a(v);

    REQUIRE(a.size() == v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        REQUIRE(v[i] == a[i]);
    }
}

TEST_CASE("copy assignment") {
    static std::vector<int> v = {
            1, 2, 3, 4, 5, 6
    };
    Array<int> a(v);
    Array<int> b;

    REQUIRE_NOTHROW(b = a);
    REQUIRE(b.size() == a.size());

    for (size_t i = 0; i < v.size(); ++i) {
        REQUIRE(b[i] == v[i]);
    }
}

TEST_CASE("move assigment") {
    static std::vector<int> v = {
            1, 2, 3, 4, 5, 6
    };
    Array<int> a(v);
    Array<int> b;

    REQUIRE_NOTHROW(b = std::move(a));
    REQUIRE(b.size() == v.size());

    for (size_t i = 0; i < v.size(); ++i) {
        REQUIRE(b[i] == v[i]);
    }
}

TEST_CASE("push") {
    Array<size_t> a;

    for (size_t i = 0; i < 100; ++i) {
        a.push_back(i);
        REQUIRE(a[i] == i);
    }
}