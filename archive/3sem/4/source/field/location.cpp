#include "field/location.h"

Location::Location(int64_t x, int64_t y) : _x(x), _y(y) {}

int64_t &Location::GetX() {
    return _x;
}

int64_t &Location::GetY() {
    return _y;
}

void Location::ChangeLocation(int64_t new_x, int64_t new_y) {
    _x = new_x, _y = new_y;
}

double Location::GetDistance(Location &other) {
    return sqrt(sqr(std::abs(GetX() - other.GetX())) + sqr(std::abs(GetY() - other.GetY())));
}
