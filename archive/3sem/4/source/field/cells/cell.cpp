#include "field/cells/cell.h"

Cell::Cell(int64_t x, int64_t y) : location(x, y), creature(nullptr) {}

Location &Cell::GetLocation() {
    return location;
}

bool Cell::StepOn(std::shared_ptr<BasicPerson> &another_creature) {
    return false;
}

void Cell::StepOut() {
    creature = nullptr;
}

std::shared_ptr<BasicPerson> &Cell::GetCreature() {
    return creature;
}
