#include "field/cells/floor.h"

Floor::Floor(int64_t x, int64_t y) : Cell(x, y) {}

bool Floor::StepOn(std::shared_ptr<BasicPerson> &another_creature) {
    if (creature != nullptr) { return false; }
    creature = another_creature;
    return true;
}
