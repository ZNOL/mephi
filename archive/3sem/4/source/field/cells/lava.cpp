#include "field/cells/lava.h"

Lava::Lava(int64_t x, int64_t y) : Cell(x, y) {}

bool Lava::StepOn(std::shared_ptr<BasicPerson> &another_creature) {
    if (creature != nullptr) { return false; }

    creature = another_creature;
    return false;
}
