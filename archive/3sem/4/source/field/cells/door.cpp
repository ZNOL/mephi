#include "field/cells/door.h"

Door::Door(int64_t x, int64_t y) : Cell(x, y), _open(false) {}

bool Door::StepOn(std::shared_ptr<BasicPerson> &another_creature) {
    if (creature != nullptr || !_open) { return false; }
    creature = another_creature;
    return _open;
}

void Door::ChangeState() {
    if (_open) { _open = false; }
    else { _open = true; }
}
