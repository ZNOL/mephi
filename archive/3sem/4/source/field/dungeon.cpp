#include "field/dungeon.h"

#include <memory>

Dungeon::Dungeon(uint32_t x_size, uint32_t y_size, uint32_t level_count) : current_level(0) {
    levels.reserve(level_count);
    for (size_t i = 0; i < level_count; ++i) {
        levels.emplace_back(x_size, y_size, 3, 15);
    }

    std::shared_ptr<BasicPerson> fake_person(nullptr);

    int32_t max_tries = 10;
    int64_t idx1 = 1 + rand() % (GetH() - 2), idx2 = 1 + rand() % (GetW() - 2);
    while (max_tries-- > 0 && !GetLevel()(idx1, idx2)->StepOn(fake_person)) {
        idx1 = 1 + rand() % (GetH() - 2), idx2 = 1 + rand() % (GetW() - 2);
    }
    if (max_tries <= 0) { idx1 = 1, idx2 = 1; }

    person_ptr = std::shared_ptr<BasicPerson>(
            new Person(
                    GetLevel()(idx1, idx2)->GetLocation(),
                    creatures_parser,
                    "MainPlayer",
                    GetLevel().GetField(),
                    GetLevel().GetCreatures()
            )
    );

    GetLevel()(idx1, idx2)->StepOn(person_ptr);
}

size_t Dungeon::GetH() {
    return GetLevel().GetH();
}

size_t Dungeon::GetW() {
    return GetLevel().GetW();
}

std::shared_ptr<BasicPerson> &Dungeon::GetPerson() {
    return person_ptr;
}

size_t &Dungeon::GetLevelIdx() {
    return current_level;
}

Level &Dungeon::GetLevel() {
    return levels[current_level];
}

void Dungeon::ChangeLevel(uint32_t new_lvl) {
    if (new_lvl > levels.size()) { throw std::logic_error("Wrong new_lvl idx"); }

    current_level = new_lvl;
}

