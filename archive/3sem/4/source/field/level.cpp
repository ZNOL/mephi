#include "field/level.h"

Level::Level(uint32_t h, uint32_t w, uint32_t min_entity, uint32_t max_entity)
        : creatures(),
          field(h) {
    if (h <= 3 || w <= 3 || h > 100 || w > 100) {
        throw std::logic_error("Wrong field size");
    }

    for (size_t i = 0; i < h; ++i) {
        for (size_t j = 0; j < w; ++j) {
            int32_t cell_type = rand() % 100;
            if (i == 0 || i == h - 1 || j == 0 || j == w - 1) {
                field[i].emplace_back(new Wall(i, j));
                continue;
            }

            if (90 <= cell_type && cell_type < 97) {
                field[i].emplace_back(new Lava(i, j));
            } else if (97 <= cell_type && cell_type < 100) {
                field[i].emplace_back(new Wall(i, j));
            } else {
                field[i].emplace_back(new Floor(i, j));
            }
        }
    }


    uint32_t entities = min_entity + rand() % (max_entity - min_entity + 1);
    creatures.reserve(entities);

    std::shared_ptr<BasicPerson> fake_unit(nullptr);

    for (size_t i = 0; i < entities; ++i) {
        uint32_t enemy_type = rand() % 4;
        uint32_t idx1 = 1 + rand() % (h - 2), idx2 = 1 + rand() % (w - 2);

        int32_t max_tries = 10;
        while (max_tries-- > 0 && !field[idx1][idx2]->StepOn(fake_unit)) {
            idx1 = 1 + rand() % (h - 2), idx2 = 1 + rand() % (w - 2);
        }
        if (max_tries <= 0) { break; }

        switch (enemy_type) {
            case 0:  // BasicEnemy
                creatures.emplace_back(
                        new BasicEnemy(
                                field[idx1][idx2]->GetLocation(),
                                creatures_parser,
                                all_basic_enemies_names[rand() % all_basic_enemies_names.size()],
                                &field,
                                &creatures
                        )
                );
                break;
            case 1:  // Golem
                creatures.emplace_back(
                        new Golem(
                                field[idx1][idx2]->GetLocation(),
                                creatures_parser,
                                all_golem_enemies_names[rand() % all_golem_enemies_names.size()],
                                &field,
                                &creatures
                        )
                );
                break;
            case 2:  // Summoner
                creatures.emplace_back(
                        new Summoner(
                                field[idx1][idx2]->GetLocation(),
                                creatures_parser,
                                all_summoner_enemies_names[rand() % all_summoner_enemies_names.size()],
                                &field,
                                &creatures
                        )
                );
                break;
            case 3:  // Undead
                creatures.emplace_back(
                        new Undead(
                                field[idx1][idx2]->GetLocation(),
                                creatures_parser,
                                all_undead_enemies_names[rand() % all_undead_enemies_names.size()],
                                &field,
                                &creatures
                        )
                );
                break;
            default:
                break;
        }
        field[idx1][idx2]->StepOn(creatures.back());
    }
}

size_t Level::GetH() {
    return field.size();
}

size_t Level::GetW() {
    return field[0].size();
}

std::shared_ptr<BasicPerson> &Level::GetCreature(size_t idx) {
    if (idx > GetCreatureCount()) {
        throw std::logic_error("Wrong creature idx");
    }
    return creatures[idx];
}

size_t Level::GetCreatureCount() {
    return creatures.size();
}

std::shared_ptr<Cell> &Level::operator()(int64_t x, int64_t y) {
    if (!(0 <= x && x < GetH() && 0 <= y && y < GetW())) {
        throw std::logic_error("Wrong field idxes");
    }
    return field[x][y];
}

TField *Level::GetField() {
    return &field;
}

TCreaturesList *Level::GetCreatures() {
    return &creatures;
}

