#include "curses.h"

Curse::Curse() : flag(false), damage(0), cur_duration(0), last_time(cur_duration) {}

Curse::Curse(double _damage, TimeType _cur_duration)
        : flag(true), damage(_damage), cur_duration(_cur_duration), last_time(cur_duration) {}

bool Curse::IsCursed() const {
    return flag;
}

double Curse::GetDamage(TimeType cur_time) {
    if (!flag) { return 0; }

    TimeType sec = cur_time - last_time;
    if (sec > cur_duration) { sec = cur_duration; }

    cur_duration -= sec;
    last_time = cur_time;

    if (cur_duration == static_cast<TimeType>(0)) {
        flag = false;
    }

    return damage * static_cast<std::chrono::duration<int64_t>>(sec).count();
}



