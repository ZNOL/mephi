#include "headers/creatures/characteristic.h"

Characteristic::Characteristic(ConfigParser &parser, const std::string &name)
        : level(1),
          current_experience(0),
          next_level_experience(parser.aConfig<uint32_t>(name, "next_level_experience")),
          max_health(parser.aConfig<double>(name, "max_health")),
          current_health(max_health),
          current_either(0),
          damage(parser.aConfig<double>(name, "damage")),
          armor(parser.aConfig<double>(name, "armor")),
          name(name),
          faction(static_cast<Factions>(parser.aConfig<int>(name, "faction"))) {}

void Characteristic::ChangeLevel() {
    if (current_experience < next_level_experience) { return; }

    current_experience -= next_level_experience;
    next_level_experience = static_cast<uint32_t>(next_level_experience * 1.2);

    ++level;

    max_health *= 1.2;
    current_health = max_health;

    damage *= 1.2;
    armor *= 1.1;
}

double Characteristic::GetHealth() const {
    return current_health;
}

double Characteristic::GetMaxHealth() const {
    return max_health;
}

void Characteristic::ChangeHealth(double val) {
    current_health = std::max(0.0, current_health - (1 - armor / 100) * val);
}

void Characteristic::GetHeal(double val) {
    current_health = std::max(max_health, current_health + val);
}

double Characteristic::GetEither() const {
    return current_either;
}

double Characteristic::GetDamage() const {
    return damage;
}

Factions Characteristic::GetFaction() {
    return faction;
}

void Characteristic::ChangeFaction(Factions new_faction) {
    faction = new_faction;
}

void Characteristic::ChangeEither(double val) {
    current_either += val;
}

