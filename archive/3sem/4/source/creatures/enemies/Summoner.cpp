#include "headers/creatures/enemies/Summoner.h"

Summoner::Summoner(Location &location, ConfigParser &parser, const std::string &name,
                   TField *field, TCreaturesList *creatures)
        : BasicPerson(location, parser, name),
          BasicEnemy(location, parser, name, field, creatures),
          Person(location, parser, name, field, creatures) {
    number_of_undead = 0;
    ability_table = CSUMMONER_ABILITY_TABLE;
}

void Summoner::Move() {
    BasicEnemy::Move();
}

void Summoner::UseAbility(size_t idx) {
    if (idx >= ability_table.size()) {
        throw std::logic_error("Wrong summoner ability idx");
    }
    ability_table[idx]->Use((BasicPerson *) this, BasicEnemy::_creatures);
}

void Summoner::Death() {
    // TODO kill all summoned enemies ??
}

void Summoner::MakeDamage() {
    BasicEnemy::MakeDamage();
}
