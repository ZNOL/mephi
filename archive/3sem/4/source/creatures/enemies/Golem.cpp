#include "headers/creatures/enemies/Golem.h"

Golem::Golem(Location &location, ConfigParser &parser, const std::string &name, TField *field,
             TCreaturesList *creatures)
        : BasicPerson(location, parser, name),
          BasicEnemy(location, parser, name, field, creatures),
          type(static_cast<GolemType>(rand() % GolemTypesCount)) {}

void Golem::Death() {
    switch (type) {
        case GolemType::Fire:
            // TODO location cell to lava
            break;
        case GolemType::Stone:
            // TODO location cell to wall
            break;
        case GolemType::Either:
            // TODO give more either to killer
            break;
    }
}