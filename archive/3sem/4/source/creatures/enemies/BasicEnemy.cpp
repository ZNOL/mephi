#include "headers/creatures/enemies/BasicEnemy.h"
#include "field/cells/all_cells.h"

BasicEnemy::BasicEnemy(Location &location, ConfigParser &parser, const std::string &name, TField *field,
                       TCreaturesList *creatures)
        : BasicPerson(location, parser, name), state(EnemyState::Wandering),
          _field(field), _creatures(creatures) {}

void BasicEnemy::ChangeEnemyState(EnemyState new_state) {
    state = new_state;
}

void BasicEnemy::Move() {
    if (GetHealth() == 0) { return; }

    int dx = 0, dy = 0;
    if (state == EnemyState::Wandering) {
        int direction = rand() % 4;
        switch (direction) {
            case 0:  // down
                dx = rand() % 2;
                break;
            case 1:  /* left */
                dy = -(rand() % 2);
                break;
            case 2:  // up
                dx = -(rand() % 2);
                break;
            case 3:  // right
                dy = rand() % 2;
                break;
            default:
                break;
        }
    } else {
        // TODO A* algo
    }

    std::shared_ptr<BasicPerson> fake_unit(nullptr);
    int64_t old_x = _location.GetX(), old_y = _location.GetY();
    int64_t new_x = old_x + dx, new_y = old_y + dy;
    if (0 <= new_x && new_x < (*_field).size() && 0 <= new_y && new_y < (*_field)[0].size()) {
        if ((*_field)[new_x][new_y]->StepOn(fake_unit)) {
            _location.ChangeLocation(new_x, new_y);
            (*_field)[new_x][new_y]->StepOn((*_field)[old_x][old_y]->GetCreature());
            (*_field)[old_x][old_y]->StepOut();
        }
    }
}

void BasicEnemy::Death() {
    // become body
}

void BasicEnemy::MakeDamage() {
    if (GetHealth() == 0) { return; }

    static const std::vector<std::pair<int64_t, int64_t>> directions = {
            {-1, 0},
            {1,  0},
            {0,  1},
            {0,  -1}
    };

    int64_t cur_x = _location.GetX(), cur_y = _location.GetY();
    for (auto &[dx, dy]: directions) {
        int64_t pos_x = cur_x + dx, pos_y = cur_y + dy;
        if (0 <= pos_x && pos_x < (*_field).size() && 0 <= pos_y && pos_y < (*_field)[0].size()) {
            std::shared_ptr<BasicPerson> &entity = (*_field)[pos_x][pos_y]->GetCreature();
            if (entity != nullptr) {
                if (this->GetFaction() != entity->GetFaction()) {
                    entity->ChangeHealth(this->GetDamage());
                }
            }
        }
    }
}
