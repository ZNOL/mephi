#include "headers/creatures/enemies/Undead.h"

Undead::Undead(Location &location, ConfigParser &parser, const std::string &name, TField *field,
               TCreaturesList *creatures)
        : BasicPerson(location, parser, name),
          BasicEnemy(location, parser, name, field, creatures) {}

void Undead::Death() {
    // TODO remove from level
}

void Undead::ComeToLive() { /* must be empty */ }