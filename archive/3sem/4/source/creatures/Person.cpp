#include "creatures/Person.h"

Person::Person(Location &location, ConfigParser &parser, const std::string &name,
               TField *field, TCreaturesList *creatures)
        : BasicPerson(location, parser, name),
          ability_table(CPERSON_ABILITY_TABLE),
          number_of_undead(0),
          _field(field),
          _creatures(creatures) {}

void Person::Move() {
    // TODO add console user input
}

void Person::UseAbility(size_t idx) {
    if (idx >= ability_table.size()) {
        throw std::logic_error("Wrong person ability idx");
    }

    ability_table[idx]->Use((BasicPerson *) this, _creatures);
}

void Person::Death() {
    // TODO game over screen
}

void Person::InputMove(int64_t dx, int64_t dy) {
    static std::shared_ptr<BasicPerson> fake_unit(nullptr);
    int64_t old_x = _location.GetX(), old_y = _location.GetY();
    int64_t new_x = old_x + dx, new_y = old_y + dy;
    if (0 <= new_x && new_x < (*_field).size() && 0 <= new_y && new_y < (*_field)[0].size()) {
        if ((*_field)[new_x][new_y]->GetCreature() == nullptr && (*_field)[new_x][new_y]->StepOn(fake_unit)) {
            _location.ChangeLocation(new_x, new_y);
            (*_field)[new_x][new_y]->StepOn((*_field)[old_x][old_y]->GetCreature());
            (*_field)[old_x][old_y]->StepOut();
        }
    }
}

void Person::MakeDamage() {
    if (GetHealth() == 0) { return; }

    static const std::vector<std::pair<int64_t, int64_t>> directions = {
            {-1, 0},
            {1,  0},
            {0,  1},
            {0,  -1}
    };

    int64_t cur_x = _location.GetX(), cur_y = _location.GetY();
    for (auto &[dx, dy]: directions) {
        int64_t pos_x = cur_x + dx, pos_y = cur_y + dy;
        if (0 <= pos_x && pos_x < (*_field).size() && 0 <= pos_y && pos_y < (*_field)[0].size()) {
            std::shared_ptr<BasicPerson> &entity = (*_field)[pos_x][pos_y]->GetCreature();
            if (entity != nullptr) {
                if (this->GetFaction() != entity->GetFaction()) {
                    entity->ChangeHealth(this->GetDamage());
                }
            }
        }
    }
}