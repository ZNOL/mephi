#include "creatures/BasicPerson.h"


BasicPerson::BasicPerson(Location &location, ConfigParser &parser, const std::string &name)
        : _location(location), _char_field(parser, name), _curse() {}

void BasicPerson::ChangeHealth(double val) {
    _char_field.ChangeHealth(val);
}

double BasicPerson::GetHealth() {
    return _char_field.GetHealth();
}

double BasicPerson::GetDamage() {
    return _char_field.GetDamage();
}

double BasicPerson::GetEither() {
    return _char_field.GetEither();
}

void BasicPerson::GetCursed(double _damage, TimeType _cur_duration) {
    _curse = Curse(_damage, _cur_duration);
}

bool BasicPerson::IsCursed() {
    return _curse.IsCursed();
}

void BasicPerson::GetCurseDamage(TimeType cur_time) {
    if (!IsCursed()) { return; }

    ChangeHealth(_curse.GetDamage(cur_time));
}

void BasicPerson::ComeToLive() {
    _char_field.GetHeal(_char_field.GetMaxHealth());
}

Factions BasicPerson::GetFaction() {
    return _char_field.GetFaction();
}

void BasicPerson::ChangeFaction(Factions new_faction) {
    _char_field.ChangeFaction(new_faction);
}

void BasicPerson::MakeDamage() {
    return;
}

Location &BasicPerson::GetLocation() {
    return _location;
}

double BasicPerson::GetMaxHealt() {
    return _char_field.GetMaxHealth();
}

void BasicPerson::ChangeEither(double val) {
    _char_field.ChangeEither(val);
}

