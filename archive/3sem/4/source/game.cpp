#include "game.h"
#include <unistd.h>

static size_t activeThreads = 0;

Game::Game(uint32_t height, uint32_t weight) : dungeon(height, weight, 6) {}

Dungeon &Game::Get() {
    return dungeon;
}

void Game::DrawDungeonLevel() {
    std::cout << "Health: " << Get().GetPerson()->GetHealth() << "\n";
    std::cout << "Either: " << Get().GetPerson()->GetEither() << "\n";
    for (size_t i = 0; i < Get().GetLevel().GetH(); ++i) {
        for (size_t j = 0; j < Get().GetLevel().GetW(); ++j) {
            std::shared_ptr<Cell> &cell = Get().GetLevel()(i, j);
            if (cell->GetCreature() != nullptr) {
                DrawWithCreature(cell);
            } else {
                DrawEmpty(cell);
            }
        }
        std::cout << "\n";
    }
    std::cout.flush();
}

bool Game::PersonSteps() {
    if (!IsLine()) { return true; }

    std::string next_move = PopLine();
    if (next_move == "w") {          // Up
        std::dynamic_pointer_cast<Person>(Get().GetPerson())->InputMove(-1, 0);
    } else if (next_move == "a") {   // Left
        std::dynamic_pointer_cast<Person>(Get().GetPerson())->InputMove(0, -1);
    } else if (next_move == "s") {   // Down
        std::dynamic_pointer_cast<Person>(Get().GetPerson())->InputMove(1, 0);
    } else if (next_move == "d") {   // Right
        std::dynamic_pointer_cast<Person>(Get().GetPerson())->InputMove(0, 1);
    } else if (next_move == "x") {   // Attack
        Get().GetPerson()->MakeDamage();
    } else if (next_move == "1") {
        std::dynamic_pointer_cast<Person>(Get().GetPerson())->UseAbility(0);
    } else if (next_move == "2") {
        std::dynamic_pointer_cast<Person>(Get().GetPerson())->UseAbility(1);
    } else if (next_move == "3") {
        std::dynamic_pointer_cast<Person>(Get().GetPerson())->UseAbility(2);
    } else if (next_move == "4") {
        std::dynamic_pointer_cast<Person>(Get().GetPerson())->UseAbility(3);
    } else if (next_move == "Q") {   // Quit
        return false;
    }

    return true;
}

void Game::EnemySteps(BasicPerson *creature) {
    if (static_cast<bool>(creature->GetHealth())) {
        creature->Move();
        creature->MakeDamage();
    }
    return;
}

void Game::EnemiesSteps() {
    std::vector<std::jthread> threads;
    for (auto &creature: *Get().GetLevel().GetCreatures()) {
        threads.emplace_back(EnemySteps, creature.get());
    }
    return;
}

void Game::SEnemiesSteps(TCreaturesList *creaturesList) {
    std::vector<std::thread> threads;
    for (auto &creature: *creaturesList) {
        threads.emplace_back(EnemySteps, creature.get());
    }

    // TODO делить врагов поровнy на потоки
    // TODO потоки создавать в начале игры и убивать при смерти челов

    // TODO atomic, mutex, conditional lock

    activeThreads += threads.size();
    for (auto &t : threads) {
        t.join();
    }
    activeThreads -= threads.size();

    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    --activeThreads;
    return;
}

void Game::GameProcess() {
    bool flag = true;

    while (flag) {
        DrawClear();
        DrawDungeonLevel();

        flag = PersonSteps();

        std::thread enemyThread(Game::SEnemiesSteps, Get().GetLevel().GetCreatures());

        ++activeThreads;
        enemyThread.detach();

        std::this_thread::sleep_for(std::chrono::milliseconds(200));
//        EnemiesSteps();
    }

    while (activeThreads > 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}

void Game::PushLine(std::string line) {
    std::unique_lock<std::mutex> lock(rwLock); (void) lock;
    rwQueue.push(std::move(line));
}

bool Game::IsLine() {
    std::unique_lock<std::mutex> lock(rwLock); (void) lock;
    return !rwQueue.empty();
}

std::string Game::PopLine() {
    std::unique_lock<std::mutex> lock(rwLock); (void) lock;
    std::string output(std::move(rwQueue.front()));
    rwQueue.pop();
    return output;
}


void InputThread(Game *game) {
    while (true) {
        if (std::cin.eof()) {
            game->PushLine("Q");
            return;
        }

        std::string line;
        std::getline(std::cin, line);
        if (std::cin.rdbuf()->in_avail()) {
            std::cin.ignore(std::cin.rdbuf()->in_avail());
        }
        std::cin.clear();

        if (!line.empty()) {
            game->PushLine(line);
            if (line == "Q") {
                return;
            }
        }
    }
}
