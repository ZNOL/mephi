#include "headers/draw/draw.h"
#include "headers/draw/colors.h"

void DrawEmpty(std::shared_ptr<Cell> &cell) {
    if (std::dynamic_pointer_cast<Door>(cell)) {           // DOOR
        std::cout << "D ";
    } else if (std::dynamic_pointer_cast<Floor>(cell)) {   // FLOOR
        std::cout << "  ";
    } else if (std::dynamic_pointer_cast<Lava>(cell)) {    // LAVA
        std::cout << Red << "L" << Color_Off << " ";
    } else if (std::dynamic_pointer_cast<Wall>(cell)) {    // WALL
        std::cout << Cyan << "▩" << Color_Off << " ";
    }
}

void DrawWithCreature(std::shared_ptr<Cell> &cell) {
    std::shared_ptr<BasicPerson> entity = cell->GetCreature();
    if (std::dynamic_pointer_cast<BasicEnemy>(entity)) {
        if (std::dynamic_pointer_cast<Golem>(entity)) {              // Golem
            std::cout << "g ";
        } else if (std::dynamic_pointer_cast<Summoner>(entity)) {    // Summoner
            std::cout << "s ";
        } else if (std::dynamic_pointer_cast<Undead>(entity)) {      // Undead
            std::cout << Green << "\uF567" << Color_Off << " ";
        } else {                                                     // BasicEnemy
            std::cout << "\uF17B ";
        }
    } else {
        std::cout << BGreen << "\uF179" << Color_Off << " ";
    }
}

void DrawClear() {
    std::cout << "\x1B[2J\x1B[H";
}