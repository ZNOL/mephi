#include "game.h"

int main() {
    std::cin.tie(nullptr);
    std::cout.tie(nullptr);

    Game game(30, 30);

    std::thread t(InputThread, &game);

    t.detach();
    game.GameProcess();

    return 0;
}
