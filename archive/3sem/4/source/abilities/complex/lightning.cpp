#include "headers/abilities/complex/lightning.h"

Lightning::Lightning(ConfigParser &parser, const std::string &name)
        : Ability(parser, name),
          damage(parser.aConfig<double>(name, "damage")) {}

void Lightning::Use(BasicPerson *unit, TCreaturesList *creatures) {
    for (auto &creature: *creatures) {
        auto distance = unit->GetLocation().GetDistance(creature->GetLocation());
        if (unit->GetFaction() != creature->GetFaction() && 0 < distance && distance < 6) {
            creature->ChangeHealth(damage);
        }
    }
}

void Lightning::changeLevel() {
    Ability::changeLevel();
    if (level == 1) { return; }

    damage *= 1.2;
}