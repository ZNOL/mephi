#include "headers/abilities/complex/necromancy.h"

Necromancy::Necromancy(ConfigParser &parser, const std::string &name)
        : Ability(parser, name),
          undead_factor(parser.aConfig<double>(name, "undead_factor")),
          current_undead(0),
          max_undead(parser.aConfig<uint32_t>(name, "max_undead")) {}

void Necromancy::Use(BasicPerson *unit, TCreaturesList *creatures) {
    for (auto &creature: *creatures) {
        auto distance = unit->GetLocation().GetDistance(creature->GetLocation());
        if (0 < distance && distance < 20) {
            if (creature->GetHealth() == 0 && unit->GetFaction() != creature->GetFaction()) {
                creature->ChangeFaction(unit->GetFaction());
                creature->ComeToLive();
            }
        }
    }
}

void Necromancy::changeLevel() {
    Ability::changeLevel();
    if (level == 1) { return; }

    undead_factor *= 1.1;
    max_undead *= 1.5;
}
