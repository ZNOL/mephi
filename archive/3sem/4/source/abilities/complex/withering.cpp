#include "headers/abilities/complex/withering.h"

Withering::Withering(ConfigParser &parser, const std::string &name)
        : Ability(parser, name),
          factor(parser.aConfig<double>(name, "factor")) {}

void Withering::Use(BasicPerson *unit, TCreaturesList *creatures) {
    for (auto &creature: *creatures) {
        auto distance = unit->GetLocation().GetDistance(creature->GetLocation());
        if (0 < distance && distance < 20) {
            if (creature->GetHealth() == 0) {
                unit->ChangeHealth(factor * creature->GetMaxHealt());
                unit->ChangeEither(factor * creature->GetMaxHealt());
            }
        }
    }
}

void Withering::changeLevel() {
    Ability::changeLevel();
    if (level == 1) { return; }

    factor *= 1.2;
}