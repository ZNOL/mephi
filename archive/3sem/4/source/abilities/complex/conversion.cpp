#include "headers/abilities/complex/conversion.h"

Conversion::Conversion(ConfigParser &parser, const std::string &name)
        : Ability(parser, name),
          factor(parser.aConfig<double>(name, "factor")) {}

void Conversion::Use(BasicPerson *unit, TCreaturesList *creatures) {
    for (auto &creature: *creatures) {
        auto distance = unit->GetLocation().GetDistance(creature->GetLocation());
        if (unit->GetFaction() != creature->GetFaction() && 0 < distance && distance < 15) {
            creature->ChangeHealth(factor * unit->GetDamage());
        }
    }
}

void Conversion::changeLevel() {
    Ability::changeLevel();
    if (level == 1) { return; }
    factor *= 0.9;
}

