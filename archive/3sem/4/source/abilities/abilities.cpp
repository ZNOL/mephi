#include "abilities/abilities.h"

Ability::Ability(ConfigParser &parser, const std::string &name)
        : level(0), _name(name),
          upgrade_cost(parser.aConfig<uint32_t>(name, "upgrade_cost")),
          either_cost(parser.aConfig<double>(name, "either_cost")) {}

uint32_t Ability::getUpgradeCost() {
    return upgrade_cost;
}

void Ability::changeLevel() {
    ++level;
    if (level == 1) { return; }

    upgrade_cost *= 1.2;
    either_cost *= 1.1;
}
