#ifndef LAB4_ABILITIES_H
#define LAB4_ABILITIES_H

#include <cstdint>
#include <string>

#include "configparser.hpp"
#include "field/cells/cell.h"
#include "creatures/BasicPerson.h"

using TField = std::vector<std::vector<std::shared_ptr<Cell>>>;
using TCreaturesList = std::vector<std::shared_ptr<BasicPerson>>;

/*!
 * \brief Абстрактный класс для способностей
 * Класс реализующий интерфейс способности
 */
class Ability {
protected:
    uint32_t level;
    std::string _name;

    uint32_t upgrade_cost;
    double either_cost;
public:
    Ability(ConfigParser &parser, const std::string &name);

    virtual void Use(BasicPerson *unit, TCreaturesList* creatures) = 0;

    virtual void changeLevel();

    virtual uint32_t getUpgradeCost();

    virtual ~Ability() = default;
};


#endif //LAB4_ABILITIES_H
