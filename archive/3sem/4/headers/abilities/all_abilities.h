#ifndef LAB4_ALL_ABILITIES_H
#define LAB4_ALL_ABILITIES_H

#include <memory>

#include "abilities/abilities.h"
#include "abilities/complex/conversion.h"
#include "abilities/complex/lightning.h"
#include "abilities/complex/necromancy.h"
#include "abilities/complex/withering.h"

#ifndef HASPERSONABILITIES
#define HASPERSONABILITIES
static const std::string ability_conf_path = "/home/znol/labs_c/3sem/4/configs/abilities.ini";
static ConfigParser ability_parser = ConfigParser(ability_conf_path);

static const std::vector<std::shared_ptr<Ability>> CPERSON_ABILITY_TABLE = {
        std::shared_ptr<Ability>(new Conversion(ability_parser, "conversion")),
        std::shared_ptr<Ability>(new Lightning(ability_parser, "lightning")),
        std::shared_ptr<Ability>(new Necromancy(ability_parser, "necromancy")),
        std::shared_ptr<Ability>(new Withering(ability_parser, "withering")),
};
#endif  // HASPERSONABILITIES

#endif //LAB4_ALL_ABILITIES_H
