#ifndef LAB4_NECROMANCY_H
#define LAB4_NECROMANCY_H

#include "abilities/abilities.h"

/*!
 * \brief Способность призывает мертвецов
 */
class Necromancy : public Ability {
private:
    double undead_factor;

    uint32_t current_undead;
    uint32_t max_undead;
public:
    Necromancy(ConfigParser &parser, const std::string &name);

    void Use(BasicPerson *unit, TCreaturesList* creatures) override;

    void changeLevel() override;

    virtual ~Necromancy() = default;
};

#endif //LAB4_NECROMANCY_H
