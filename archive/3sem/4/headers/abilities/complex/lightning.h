#ifndef LAB4_LIGHTNING_H
#define LAB4_LIGHTNING_H

#include "abilities/abilities.h"

/*!
 * \brief Способность наносит дистанционый урон врагам
 */
class Lightning : public Ability {
private:
    double damage;
public:
    Lightning(ConfigParser &parser, const std::string &name);

    void Use(BasicPerson *unit, TCreaturesList* creatures) override;

    void changeLevel() override;

    virtual ~Lightning() = default;
};

#endif //LAB4_LIGHTNING_H
