#ifndef LAB4_CONVERSION_H
#define LAB4_CONVERSION_H

#include "abilities/abilities.h"

/*!
 * \brief Способность уменьшаешь характеристики врагов
 */
class Conversion : public Ability {
private:
    double factor;
public:
    Conversion(ConfigParser &parser, const std::string &name);

    void Use(BasicPerson *unit, TCreaturesList* creatures) override;

    void changeLevel() override;

    virtual ~Conversion() = default;
};

#endif //LAB4_CONVERSION_H
