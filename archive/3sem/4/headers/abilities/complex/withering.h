#ifndef LAB4_WITHERING_H
#define LAB4_WITHERING_H

#include "abilities/abilities.h"

/*!
 * \brief Способность преобразовывает мертвые тела в эфир и здоровья
 */
class Withering : public Ability {
private:
    double factor;

public:
    Withering(ConfigParser &parser, const std::string &name);

    void Use(BasicPerson *unit, TCreaturesList* creatures) override;

    void changeLevel() override;

    virtual ~Withering() = default;
};

#endif //LAB4_WITHERING_H
