#ifndef SAFEINPUT_HPP
#define SAFEINPUT_HPP

#include <iostream>
#include <limits>

#include <queue>
#include <string>
#include <mutex>
#include <thread>

/*!
 * Шаблонная функция для защищенного чтения
 * @tparam T - тип переменной
 * @param in - поток ввода
 * @param prompt - сообщение при вводе
 * @param var - ссылка на переменную
 */
template<typename T>
void SafeGet(std::istream &in, const char *prompt, T &var) {
    std::cout << prompt << std::endl;

    in >> var;
    if (in.eof() || in.bad()) {
        throw std::runtime_error("End of file is discovered!");
    }
    if (!in.good() || in.fail()) {
        in.clear();
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw std::logic_error("Input error!");
    }
}

#endif  // SAFEINPUT_HPP