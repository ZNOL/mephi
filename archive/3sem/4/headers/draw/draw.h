#ifndef LAB4_DRAW_H
#define LAB4_DRAW_H

#include <memory>
#include <iostream>

#include "creatures/all_creatures.h"
#include "field/cells/all_cells.h"

void DrawEmpty(std::shared_ptr<Cell> &cell);

void DrawWithCreature(std::shared_ptr<Cell> &cell);

void DrawClear();

#endif //LAB4_DRAW_H
