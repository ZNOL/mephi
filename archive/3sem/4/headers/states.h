#ifndef LAB4_STATES_H
#define LAB4_STATES_H

#include <cstdint>

enum class EnemyState {
    Dead,
    Wandering,
    Aggressive,
};
const uint8_t EnemyStatesCount = 3;

enum class GolemType {
    Fire,
    Stone,
    Either,
};
const uint8_t GolemTypesCount = 3;

#endif //LAB4_STATES_H
