#ifndef LAB4_FACTIONS_H
#define LAB4_FACTIONS_H

enum class Factions {
    Khalai,
    Nerazim,
    Taldarim,
    Purifiers,
    UnitedEarthDirectorate,
    ConfederacyOfMan,
    Zerg,
    Xelnaga,
};

const int CountOfFactions = 8;

#endif //LAB4_FACTIONS_H
