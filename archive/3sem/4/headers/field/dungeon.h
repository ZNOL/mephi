#ifndef LAB4_DUNGEON_H
#define LAB4_DUNGEON_H

#include <cstdint>

#include "field/level.h"
#include "creatures/BasicPerson.h"

#include "headers/draw/draw.h"

class Dungeon {
private:
    std::shared_ptr<BasicPerson> person_ptr;
    size_t current_level;
    std::vector<Level> levels;
public:
    Dungeon(uint32_t x_size, uint32_t y_size, uint32_t level_count);

    size_t GetH();

    size_t GetW();

    std::shared_ptr<BasicPerson> &GetPerson();

    size_t &GetLevelIdx();

    Level &GetLevel();

    void ChangeLevel(uint32_t new_lvl);
};

#endif //LAB4_DUNGEON_H
