#ifndef LAB4_FLOOR_H
#define LAB4_FLOOR_H

#include "field/cells/cell.h"

class Floor : public Cell {
public:
    Floor(int64_t x, int64_t y);

    bool StepOn(std::shared_ptr<BasicPerson> &another_creature) override;
};

#endif //LAB4_FLOOR_H
