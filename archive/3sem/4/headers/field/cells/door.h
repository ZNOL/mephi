#ifndef LAB4_DOOR_H
#define LAB4_DOOR_H

#include "field/cells/cell.h"

class Door : public Cell {
private:
    bool _open;
public:
    Door(int64_t x, int64_t y);

    bool StepOn(std::shared_ptr<BasicPerson> &another_creature) override;

    void ChangeState();
};

#endif //LAB4_DOOR_H
