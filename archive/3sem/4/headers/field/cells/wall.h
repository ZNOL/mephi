#ifndef LAB4_WALL_H
#define LAB4_WALL_H

#include "field/cells/cell.h"

class Wall : public Cell {
public:
    Wall(int64_t x, int64_t y);

    bool StepOn(std::shared_ptr<BasicPerson> &another_creature) override;
};

#endif //LAB4_WALL_H
