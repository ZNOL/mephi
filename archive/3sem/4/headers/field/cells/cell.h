#ifndef LAB4_CELL_H
#define LAB4_CELL_H

#include <cstdint>
#include <memory>
#include <vector>

#include "field/location.h"
#include "creatures/BasicPerson.h"

class Cell {
protected:
    Location location;
    std::shared_ptr<BasicPerson> creature;

public:
    Cell(int64_t x, int64_t y);

    Location &GetLocation();

    std::shared_ptr<BasicPerson> &GetCreature();

    virtual bool StepOn(std::shared_ptr<BasicPerson> &another_creature);

    virtual void StepOut();
};


#endif //LAB4_CELL_H
