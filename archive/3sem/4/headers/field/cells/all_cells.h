#ifndef LAB4_ALL_CELLS_H
#define LAB4_ALL_CELLS_H

#include "field/cells/door.h"
#include "field/cells/floor.h"
#include "field/cells/lava.h"
#include "field/cells/wall.h"
#include "field/cells/cell.h"

using TField = std::vector<std::vector<std::shared_ptr<Cell>>>;
using TCreaturesList = std::vector<std::shared_ptr<BasicPerson>>;

#endif //LAB4_ALL_CELLS_H
