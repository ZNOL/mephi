#ifndef LAB4_LAVA_H
#define LAB4_LAVA_H

#include "field/cells/cell.h"

class Lava : public Cell {
public:
    Lava(int64_t x, int64_t y);

    bool StepOn(std::shared_ptr<BasicPerson> &another_creature) override;
};

#endif //LAB4_LAVA_H
