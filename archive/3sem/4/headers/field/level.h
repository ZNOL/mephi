#ifndef LAB4_LEVEL_H
#define LAB4_LEVEL_H

#include <vector>
#include <memory>

#include "field/cells/all_cells.h"
#include "creatures/all_creatures.h"

using TField = std::vector<std::vector<std::shared_ptr<Cell>>>;
using TCreaturesList = std::vector<std::shared_ptr<BasicPerson>>;

class Level {
private:
    TField field;
    TCreaturesList creatures;

public:
    Level(uint32_t h, uint32_t w, uint32_t min_entity, uint32_t max_entity);

    size_t GetH();

    size_t GetW();

    TField *GetField();

    TCreaturesList *GetCreatures();

    std::shared_ptr<BasicPerson> &GetCreature(size_t idx);

    size_t GetCreatureCount();

    std::shared_ptr<Cell> &operator()(int64_t x, int64_t y);
};

#endif //LAB4_LEVEL_H
