#ifndef LAB4_LOCATION_H
#define LAB4_LOCATION_H

#include <cstdint>
#include <cmath>

#define sqr(x) (x) * (x)

struct Location {
private:
    int64_t _x;
    int64_t _y;

public:
    Location(int64_t x, int64_t y);

    int64_t &GetX();

    int64_t &GetY();

    void ChangeLocation(int64_t new_x, int64_t new_y);

    double GetDistance(Location &another);
};

#endif //LAB4_LOCATION_H