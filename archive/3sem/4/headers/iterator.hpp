#include <iterator>
#include <cstddef>
#include <vector>

template<typename T>
struct Iterator {
    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using value_type = T;
    using pointer = T *;
    using reference = T &;

private:
    pointer m_ptr;
    pointer _begin;
    pointer _end;

public:
    Iterator(pointer ptr)
            : m_ptr(ptr), _begin(ptr), _end(++ptr) {}

    Iterator(pointer ptr, pointer begin, pointer end)
            : m_ptr(ptr), _begin(begin), _end(end) {}

    Iterator(std::vector<T> &v) : m_ptr(&(*v.begin())), _begin(&(*v.begin())), _end(&(*v.end())) {}

    reference operator*() const { return *m_ptr; }

    pointer operator->() { return m_ptr; }

    Iterator &operator++() {
        m_ptr++;
        return *this;
    }

    Iterator operator++(int) {
        Iterator tmp = *this;
        ++(*this);
        return tmp;
    }

    friend bool operator==(const Iterator &a, const Iterator &b) { return a.m_ptr == b.m_ptr; };

    friend bool operator!=(const Iterator &a, const Iterator &b) { return a.m_ptr != b.m_ptr; };

    Iterator begin() { return Iterator(_begin); }

    Iterator end() { return Iterator(_end); }
};

