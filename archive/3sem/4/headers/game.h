#ifndef LAB4_GAME_H
#define LAB4_GAME_H

#include <iostream>

#include "field/dungeon.h"
#include "safeinput.hpp"

class Game {
private:
    Dungeon dungeon;
    std::queue<std::string> rwQueue;
    std::mutex rwLock;

public:
    Game(uint32_t height, uint32_t weight);

    Dungeon &Get();

    bool PersonSteps();

    static void EnemySteps(BasicPerson *creature);

    void EnemiesSteps();

    static void SEnemiesSteps(TCreaturesList *creaturesList);

    void DrawDungeonLevel();

    void GameProcess();

    void PushLine(std::string line);

    bool IsLine();

    std::string PopLine();
};

void InputThread(Game *game);

#endif //LAB4_GAME_H
