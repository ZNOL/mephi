#ifndef LAB4_CURSES_H
#define LAB4_CURSES_H

#include <cstdint>
#include <chrono>

using TimeType = std::chrono::seconds;

/*!
 * \brief Класс проклятия
 * При наложении наносит damage урона каждую cur_duration секунд
 */
class Curse {
private:
    bool flag;
    double damage;

    TimeType cur_duration;
    TimeType last_time;
public:
    Curse();

    Curse(double _damage, TimeType _cur_duration);

    [[nodiscard]] bool IsCursed() const;

    double GetDamage(TimeType cur_time);
};

#endif //LAB4_CURSES_H
