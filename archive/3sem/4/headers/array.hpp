#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <algorithm>
#include <stdexcept>
#include <vector>

/*!
 * Шаблонный класс, повторяющий функционал
 * std::vector<T>
 * @tparam T - тип последовательности
 * @param _size - текущий размер
 * @param _capacity - размер выделенной памяти
 * @param _array - указатель на выделенную память
 */
template<typename T>
class Array {
private:
    std::size_t _size;
    std::size_t _capacity;
    T *_array;

    void resize(std::size_t new_size);
public:
    Array();

    explicit Array(std::size_t size);

    /*!
     * Copy constructor from other Array
     * @param other - ссылка на
     */
    Array(const Array<T> &other);

    Array(Array<T> &&other) noexcept;

    Array(const std::vector<T> &other);

    Array<T> &operator=(const Array<T> &other);

    Array<T> &operator=(Array<T> &&other) noexcept;

    T &operator[](std::size_t idx);

    const T &operator[](std::size_t idx) const;

    [[nodiscard]] std::size_t &size();

    [[nodiscard]] std::size_t capacity() const;

    void push_back(T element);

    ~Array();
};

template<typename T>
Array<T>::Array() : _size(0), _capacity(1), _array(new T[1]) {}

template<typename T>
Array<T>::Array(std::size_t size) : _size(0), _capacity(size), _array(new T[_capacity]) {}

template<typename T>
Array<T>::Array(const Array<T> &other) :
        _size(other._size), _capacity(other._size), _array(new T[other._size]) {
    std::copy(other._array, other._array + _size, _array);
}

template<typename T>
Array<T>::Array(Array<T> &&other) noexcept :
        _size(other._size), _capacity(other._capacity) {
    std::swap(_array, other._array);
}

template<typename T>
Array<T>::Array(const std::vector<T> &other) :
        _size(other.size()), _capacity(other.size()), _array(new T[other.size()]) {
    std::copy(other.begin(), other.end(), _array);
}

template<typename T>
Array<T> &Array<T>::operator=(const Array<T> &other) {
    if (this == &other) {
        return *this;
    }

    delete[] _array;
    _size = other._size;
    _capacity = other._capacity;

    _array = new T[_capacity];
    std::copy(other._array, other._array + other._capacity, _array);

    return *this;
}

template<typename T>
Array<T> &Array<T>::operator=(Array<T> &&other) noexcept {
    if (this == &other) {
        return *this;
    }

    std::swap(_size, other._size);
    std::swap(_capacity, other._capacity);
    std::swap(_array, other._array);

    return *this;
}

template<typename T>
T &Array<T>::operator[](std::size_t idx) {
    if (idx >= _capacity) {
        throw std::invalid_argument("Wrong index");
    }

    return _array[idx];
}

template<typename T>
const T &Array<T>::operator[](std::size_t idx) const {
    if (idx >= _size) {
        throw std::invalid_argument("Wrong index");
    }

    return _array[idx];
}

template<typename T>
std::size_t &Array<T>::size() {
    return _size;
}

template<typename T>
std::size_t Array<T>::capacity() const {
    return _capacity;
}

template<typename T>
void Array<T>::resize(std::size_t new_size) {
    T *new_array = new T[new_size];
    std::copy(_array, _array + std::min(_size, new_size), new_array);

    delete[] _array;

    _array = new_array;
    _capacity = new_size;
}

template<typename T>
void Array<T>::push_back(T element) {
    if (_size == _capacity) resize(_capacity * 2);
    _array[_size++] = element;
}

template<typename T>
Array<T>::~Array() {
    delete[] _array;
    _capacity = 0;
    _size = 0;
}

#endif  // ARRAY_HPP