#ifndef LAB4_PERSON_H
#define LAB4_PERSON_H

#include <cstdint>

#include <vector>
#include <array.hpp>

#include "creatures/BasicPerson.h"
#include "abilities/all_abilities.h"
#include "field/cells/cell.h"

using TField = std::vector<std::vector<std::shared_ptr<Cell>>>;
using TCreaturesList = std::vector<std::shared_ptr<BasicPerson>>;

/*!
 * \brief Класс игрока
 * Основной обработчик действий игрока
*/
class Person : virtual public BasicPerson {
protected:
    TField *_field;
    TCreaturesList *_creatures;

    uint32_t number_of_undead;
    std::vector<std::shared_ptr<Ability>> ability_table;
public:
    Person(Location &location, ConfigParser &parser, const std::string &name,
           TField *field, TCreaturesList *creatures);

    void Move() override;

    void InputMove(int64_t dx, int64_t dy);

    void MakeDamage() override;

    virtual void UseAbility(size_t idx);

    void Death() override;

    virtual ~Person() = default;
};

#endif //LAB4_PERSON_H
