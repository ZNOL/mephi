#ifndef LAB4_UNDEAD_H
#define LAB4_UNDEAD_H

#include "BasicEnemy.h"

using TField = std::vector<std::vector<std::shared_ptr<Cell>>>;
using TCreaturesList = std::vector<std::shared_ptr<BasicPerson>>;

/*!
 * \brief Класс Мертвеца
 * Мертвец не может воскреснуть и после смерти пропадает с поля
 */
class Undead : public BasicEnemy {
public:
    Undead(Location &location, ConfigParser &parser, const std::string &name,
           TField *field, TCreaturesList *creatures);

    void Death() override;

    void ComeToLive() override;

    virtual ~Undead() = default;
};

#endif //LAB4_UNDEAD_H
