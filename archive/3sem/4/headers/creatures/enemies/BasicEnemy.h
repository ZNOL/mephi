#ifndef LAB4_BASICENEMY_H
#define LAB4_BASICENEMY_H

#include <utility>

#include "creatures/BasicPerson.h"
#include "field/cells/cell.h"
#include "states.h"

using TField = std::vector<std::vector<std::shared_ptr<Cell>>>;
using TCreaturesList = std::vector<std::shared_ptr<BasicPerson>>;

/*!
 * \brief Базовый класс для всех противников
 */
class BasicEnemy : virtual public BasicPerson {
protected:
    TField *_field;
    TCreaturesList *_creatures;
    EnemyState state;

public:
    BasicEnemy(Location &location, ConfigParser &parser, const std::string &name,
               TField *field, TCreaturesList *creatures);

    virtual void ChangeEnemyState(EnemyState new_state);

    void MakeDamage() override;

    void Death() override;

    void Move() override;

    virtual ~BasicEnemy() = default;
};

#endif //LAB4_BASICENEMY_H
