#ifndef LAB4_GOLEM_H
#define LAB4_GOLEM_H

#include "states.h"
#include "BasicEnemy.h"

using TField = std::vector<std::vector<std::shared_ptr<Cell>>>;
using TCreaturesList = std::vector<std::shared_ptr<BasicPerson>>;

/*!
 * \brief Класс Голема
 * Голем после смерти превращается в элемент окружения
 */
class Golem : public BasicEnemy {
private:
    GolemType type;
public:
    Golem(Location &location, ConfigParser &parser, const std::string &name,
          TField *field, TCreaturesList *creatures);

    void Death() override;

    virtual ~Golem() = default;
};

#endif //LAB4_GOLEM_H
