#ifndef LAB4_SUMMONER_H
#define LAB4_SUMMONER_H

#include "BasicEnemy.h"
#include "creatures/Person.h"

using TField = std::vector<std::vector<std::shared_ptr<Cell>>>;
using TCreaturesList = std::vector<std::shared_ptr<BasicPerson>>;

/*!
 * \brief Класс Призывателя
 * Призыватель может использовать умение necromancy
 */
class Summoner : public virtual Person, public virtual BasicEnemy {
public:
    Summoner(Location &location, ConfigParser &parser, const std::string &name,
             TField *field, TCreaturesList *creatures);

    void Move() override;

    void MakeDamage() override;

    void UseAbility(size_t idx) override;

    void Death() override;

    virtual ~Summoner() = default;
};

const std::vector<std::shared_ptr<Ability>> CSUMMONER_ABILITY_TABLE = {
        std::shared_ptr<Ability>(new Necromancy(ability_parser, "necromancy")),
};

#endif //LAB4_SUMMONER_H
