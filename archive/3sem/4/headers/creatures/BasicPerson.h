#ifndef LAB4_BASICPERSON_H
#define LAB4_BASICPERSON_H

#include "characteristic.h"

#include "curses.h"
#include "field/location.h"

/*!
 * \brief Абстрактный базовый класс человечков
 */
class BasicPerson {
protected:
    Location _location;
    Characteristic _char_field;
    Curse _curse;

public:
    /*!
     * Основной конструктор
     * @param location - расположение персонажа
     * @param parser - ссылка на класс статической конфигурации
     * @param name - название персонажа для конфигурации
     */
    BasicPerson(Location &location, ConfigParser &parser, const std::string &name);

    /*!
     * Виртуальная функция перемещения
     * Различается у Person и BasicEnemy
     */
    virtual void Move() = 0;

    /*!
     * Функция получения урона (еще не уменьшена броней)
     * @param val - значение урона
     */
    virtual void ChangeHealth(double val);

    /*!
     * @return Количество здоровья персонажа
     */
    virtual double GetHealth();

    virtual double GetMaxHealt();

    /*!
     * @return Показатель урона персонажа
     */
    virtual double GetDamage();

    /*!
     * @return Количество эфира у персонажа
     */
    virtual double GetEither();

    virtual void ChangeEither(double val);

    /*!
     * Виртуальная функция нанесения урона
     */
    virtual void MakeDamage();

    /*!
     * Виртуальная функция смерти
     * Оличается у всех классов
     */
    virtual void Death() = 0;

    /*!
     * Получение проклятия
     * @param _damage - значение урона в секунду
     * @param _cur_duration - длительность проклятия в секундах
     */
    virtual void GetCursed(double _damage, TimeType _cur_duration);

    /*!
     * @return Возвращает проклят ли персонаж
     */
    virtual bool IsCursed();

    /*!
     * Получение урона после хода
     * Зависит от cur_time и Curse::last_time
     * @param cur_time - текущее время в формате timestamp
     */
    virtual void GetCurseDamage(TimeType cur_time);

    /*!
     * Функция для способности necromancy
     * Удалена у Голема и Мертвеца
     */
    virtual void ComeToLive();

    /*!
     * @return Получение расы персонажа
     */
    virtual Factions GetFaction();

    /*!
     * Изменение расы персонажа (требуется при возрождении)
     * @param new_faction - новая раса
     */
    virtual void ChangeFaction(Factions new_faction);

    virtual Location &GetLocation();

    virtual ~BasicPerson() = default;
};

#endif //LAB4_BASICPERSON_H