#ifndef LAB4_ALL_CREATURES_H
#define LAB4_ALL_CREATURES_H

#include "creatures/BasicPerson.h"
#include "creatures/Person.h"

#include "creatures/enemies/BasicEnemy.h"
#include "creatures/enemies/Golem.h"
#include "creatures/enemies/Summoner.h"
#include "creatures/enemies/Undead.h"

#ifndef HASENEMIESNAMES
#define HASENEMIESNAMES
static const std::vector<std::string> all_basic_enemies_names = {
        "Ded", "NeDed", "Babka"
};

static const std::vector<std::string> all_golem_enemies_names = {
        "GolemDed", "GolemNeDed", "GolemBabka"
};

static const std::vector<std::string> all_undead_enemies_names = {
        "UndeadDed", "UndeadNeDed", "UndeadBabka"
};

static const std::vector<std::string> all_summoner_enemies_names = {
        "SummonerDed", "SummonerNeDed", "SummonerBabka"
};
#endif  // HASENEMIESNAMES

#ifndef HASCREATUREPARSER
#define HASCREATUREPARSER
static const std::string creatures_conf_path = "/home/znol/labs_c/3sem/4/configs/characteristic.ini";
static ConfigParser creatures_parser = ConfigParser(creatures_conf_path);
#endif  //  HASCREATUREPARSER

#endif //LAB4_ALL_CREATURES_H
