#ifndef LAB4_CHARACTERISTIC_H
#define LAB4_CHARACTERISTIC_H

#include <cstdint>
#include <string>

#include "factions.h"
#include "configparser.hpp"

/*!
 * \brief Класс для хранения характеристик персонажа
 * Класс поддерживающий получение, изменение и загрузку из конфига
 */
class Characteristic {
private:
    uint32_t level;
    uint32_t current_experience;
    uint32_t next_level_experience;

    double max_health;
    double current_health;
    double current_either;

    double damage;
    double armor;

    std::string name;
    Factions faction;
public:
    /*!
     * Основной конструктор
     * @param parser - ссылка на конфигуратор
     * @param name - название персонажа для получения характеристик
     */
    Characteristic(ConfigParser &parser, const std::string &name);

    void ChangeLevel();

    [[nodiscard]] double GetHealth() const;

    [[nodiscard]] double GetMaxHealth() const;

    void GetHeal(double val);

    void ChangeHealth(double val);

    [[nodiscard]] double GetEither() const;

    void ChangeEither(double val);

    [[nodiscard]] double GetDamage() const;

    Factions GetFaction();

    void ChangeFaction(Factions new_faction);
};


#endif //LAB4_CHARACTERISTIC_H
