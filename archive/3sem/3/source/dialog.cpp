#include <vector>
#include <iostream>

#include "safeinput.hpp"
#include "table.h"

namespace dialog {
    void dialog(const std::vector<std::string> &msgs, int &rc) {
        std::cout << std::endl;
        for (const auto &msg : msgs) {
            std::cout << msg << std::endl;
        }
        std::cout << std::endl;

        do {
            try {
                SafeGet<int>(std::cin, "Select the menu item", rc);
                if (rc < 0 || rc >= msgs.size()) {
                    std::cout << "Wrong menu number!" << std::endl;
                }
            }
            catch (const std::logic_error &ex) {
                std::cout << "Try better!" << std::endl;
            }
        } while (rc < 0 || rc >= msgs.size());
    }

    void D_Input(Table &table) {
        std::cin >> table;
    }

    void D_Output(Table &table) {
        std::cout << table;
    }

    void D_Find(Table &table) {
        int key;
        SafeGet<int>(std::cin, "Ключ", key);
        auto ptr = table.findByKey(key);

        if (ptr != nullptr) {
            std::cout << ptr->_key << " " << ptr->_value << std::endl;
        } else {
            throw std::logic_error("Element doesn't found");
        }
    }

    void D_Insert(Table &table) {
        int key;
        std::string value;
        SafeGet<int>(std::cin, "Ключ", key);
        SafeGet<std::string>(std::cin, "Значение", value);
        table.insert(key, value);
    }

    void D_Delete(Table &table) {
        int64_t key;
        SafeGet<int64_t>(std::cin, "Ключ", key);

        table.deleteByKey(key);
    }

    void D_Garbage(Table &table) {
        std::cout << "Before reorganization = " << table.size() << std::endl;
        table.garbageCollector();
        std::cout << "After reorganization = " << table.size() << std::endl;
    }
}