#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "headers/cardioid.h"

TEST_CASE("Constructor") {
    REQUIRE_NOTHROW(Cardioid());
    REQUIRE_NOTHROW(Cardioid(21));
    REQUIRE_NOTHROW(Cardioid(-12));
    REQUIRE_NOTHROW(Cardioid(228));
}

TEST_CASE("Get a") {
    SECTION("0.228") {
        Cardioid a(0.228);
        CHECK(a.getA() == 0.228);
    }
    SECTION("0") {
        Cardioid a(0);
        CHECK(a.getA() == 0);
    }
    SECTION("100") {
        Cardioid a(100);
        CHECK(a.getA() == 100);
    }
}

TEST_CASE("Copy constructor") {
    SECTION("228") {
        Cardioid a(228);
        Cardioid b(a);
        CHECK(b.getA() == 228);
    }
    SECTION("-228") {
        Cardioid a(-228);
        Cardioid b(a);
        CHECK(b.getA() == -228);
    }
}

TEST_CASE("Set a") {
    Cardioid c;
    SECTION("0.228") {
        c.setA(0.228);
        CHECK(c.getA() == 0.228);
    }
    SECTION("0") {
        c.setA(0);
        CHECK(c.getA() == 0);
    }
    SECTION("100") {
        c.setA(100);
        CHECK(c.getA() == 100);
    }
}

TEST_CASE("Radius") {
    Cardioid c(4);
    SECTION("30") {
        CHECK (c.radius(30) == Approx(6.765).margin(0.001));
    }
    SECTION("3.14") {
        CHECK (c.radius(3.14) == Approx(16).margin(0.001));
    }
    SECTION("6.28") {
        CHECK (c.radius(6.28) == Approx(0).margin(0.001));
    }
}

TEST_CASE("Area") {
    SECTION("12") {
        Cardioid c(12);
        CHECK(c.area() == Approx(2714.34).margin(0.001));
    }
    SECTION("228") {
        Cardioid c(228);
        CHECK(c.area() == Approx(979875).margin(0.001));
    }
    SECTION("1") {
        Cardioid c(1);
        CHECK(c.area() == Approx(18.849).margin(0.001));
    }
}

TEST_CASE("Len") {
    Cardioid c(4);
    SECTION("0") {
        CHECK(c.len(0) == Approx(0).margin(0.001));
    }
    SECTION("PI") {
        CHECK(c.len(M_PI) == Approx(32).margin(0.001));
    }
    SECTION("2PI") {
        CHECK(c.len(2 * M_PI) == Approx(64).margin(0.001));
    }
    SECTION("3PI") {
        CHECK(c.len(3 * M_PI) == Approx(96).margin(0.001));
    }
}

TEST_CASE("Special") {
    SECTION("4") {
        Cardioid c(4);
        auto t = c.getSpecialPoints();
        CHECK(std::get<0>(t) == Approx(0).margin(0.001));
        CHECK(std::get<1>(t) == Approx(4).margin(0.001));
        CHECK(std::get<2>(t) == Approx(16).margin(0.001));
        CHECK(std::get<3>(t) == Approx(4).margin(0.001));
    }
    SECTION("228") {
        Cardioid c(228);
        auto t = c.getSpecialPoints();
        CHECK(std::get<0>(t) == Approx(0).margin(0.001));
        CHECK(std::get<1>(t) == Approx(228).margin(0.001));
        CHECK(std::get<2>(t) == Approx(912).margin(0.001));
        CHECK(std::get<3>(t) == Approx(228).margin(0.001));
    }
}

TEST_CASE("Health") {
    SECTION("2") {
        Cardioid c(2);
        CHECK(c.health() == true);
    }
    SECTION("228") {
        Cardioid c(228);
        CHECK(c.health() == true);
    }
    SECTION("0") {
        Cardioid c(0);
        CHECK(c.health() == false);
    }
    SECTION("-228") {
        Cardioid c(-228);
        CHECK(c.health() == false);
    }
}