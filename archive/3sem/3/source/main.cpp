#include <iostream>

#include "dialog.h"

int main() {
    Table table;

    while (true) {
        try {
            int rc = 1;
            while (rc != 0) {
                dialog::dialog(dialog::menu, rc);
                if (rc != 0) {
                    dialog::Funcs[rc](table);
                }
            }
            if (rc == 0) {
                std::cout << "Good luck" << std::endl;
                break;
            }
        }
        catch (const std::runtime_error &ex) {
            std::cout << "Something important went wrong!" << std::endl;

            std::cout << ex.what() << std::endl;
            break;
        }
        catch (const std::exception &ex) {
            std::cout << "Something went wrong!" << std::endl;
            std::cout << ex.what() << std::endl;
        }
    }

    return 0;
}
