#include "safeinput.hpp"
#include "table.h"

Table::Table() : _table(TABLE_SIZE) {}

Table::Table(size_t table_size) : _table(table_size) {}

size_t Table::size() {
    return _table.size();
}

size_t Table::capacity() const {
    return _table.capacity();
}

Table::Table(Array<int> &keys, Array<std::string> &value) :
    _table(TABLE_SIZE) {
    if (keys.size() > _table.capacity()) {
        throw std::invalid_argument("To many arguments for table");
    }
    if (keys.size() != value.size()) {
        throw std::logic_error("Different sizes of arrays keys and values");
    }

    for (size_t i = 0; i < keys.size(); ++i) {
        insert(keys[i], value[i]);
    }
}

Table::Table(const Table &other) : Table(other.capacity()) {
    _table = other._table;
}

Table::Table(Table &&another) {
    _table = std::move(another._table);
}

Element &Table::operator[](size_t idx) {
    if (idx >= _table.capacity()) {
        throw std::invalid_argument("Wrong table index");
    }
    return _table[idx];
}

const Element &Table::operator[](size_t idx) const {
    if (idx >= _table.capacity()) {
        throw std::invalid_argument("Wrong table index");
    }
    return _table[idx];
}

Table &Table::operator=(const Table &another) {
    if (this == &another) {
        return *this;
    }

    _table = another._table;

    return *this;
}

Table &Table::operator=(Table &&other) noexcept {
    if (this == &other) {
        return *this;
    }

    std::swap(_table, other._table);
    // _table = std::move(other._table);

    return *this;
}

Element *Table::findByKey(int key) {
    if (_table.size() == 0) {
        return nullptr;
    }
    for (size_t i = 0; i < _table.capacity(); ++i) {
        if (_table[i]._busy && _table[i]._key == key) {
            return &_table[i];
        }
    }
    return nullptr;
}

void Table::garbageCollector() {
    size_t p1 = 0, p2 = 0;
    _table.size() = 0;
    while (p1 < _table.capacity()) {
        if (! _table[p1]._busy) {
            ++p1;
        } else {
            if (p1 != p2) {
                _table[p2] = _table[p1];
                _table[p1]._busy = false;
            }
            ++_table.size();
            ++p1, ++p2;
        }
    }
}

void Table::insert(int key, std::string &value) {
    if (findByKey(key) != nullptr) {
        throw std::logic_error("Key duplication");
    }

    if (_table.size() == _table.capacity()) {
        garbageCollector();
        if (_table.size() == _table.capacity()) {
            throw std::logic_error("Table overflow");
        }
    }

    _table[_table.size()++] = Element(key, value);
}

void Table::deleteByKey(int64_t key) {
    auto ptr = findByKey(key);
    if (ptr == nullptr) {
        throw std::invalid_argument("Key is not in table");
    }

    ptr->_busy = false;
}

void Table::show(std::ostream &out) {
    out << "------------------------------------------------------------" << std::endl;
    out << "Size = " << _table.size() << " " << "Capacity = " << _table.capacity() << std::endl;
    for (size_t i = 0; i < _table.capacity() ; ++i) {
        if (_table[i]._busy) {
            out << _table[i]._key << " | " << _table[i]._value << std::endl;
        }
    }
    out << "------------------------------------------------------------" << std::endl;

}

std::istream &operator>>(std::istream &in, Table &t) {
    size_t amount;

    SafeGet<size_t>(std::cin, "Введите кол-во элементов", amount);

    Array<int> keys;
    Array<std::string> values;
    for (size_t i = 0; i < amount; ++i) {
        int key;
        std::string value;
        SafeGet<int>(std::cin, "Ключ", key);
        SafeGet<std::string>(std::cin, "Значение", value);
        keys.push_back(key);
        values.push_back(value);
        t.insert(key, value);
    }
    return in;
}

std::ostream &operator<<(std::ostream &out, Table &t) {
    t.show(out);
    return out;
}
