#ifndef DIALOG_H
#define DIALOG_H

#include <vector>
#include <string>

#include "table.h"

namespace dialog {
    void dialog(const std::vector<std::string> &msgs, int &rc);

    void D_Input(Table &table);
    void D_Output(Table &table);
    void D_Find(Table &table);
    void D_Insert(Table &table);
    void D_Delete(Table &table);
    void D_Garbage(Table &table);

    const std::vector<std::string> menu {
            "0. Завершить программу",
            "1. Ввод таблицы",
            "2. Вывод таблицы",
            "3. Поиск по ключу",
            "4. Добавление элемента",
            "5. Удаление по ключу",
            "6. Реорганизация таблицы",
    };

    void (*Funcs[])(Table &)  = {
            nullptr,                   // 0
            D_Input,                   // 1
            D_Output,                  // 2
            D_Find,                    // 3
            D_Insert,                  // 4
            D_Delete,                  // 5
            D_Garbage,                 // 6
    };
}
#endif  // DIALOG_H
