#ifndef TABLE_H
#define TABLE_H

#include <utility>
#include <vector>
#include <string>
#include <iostream>

#include "array.hpp"

#define TABLE_SIZE 10

struct Element {
    bool _busy{};
    int _key{};
    std::string _value;

    Element() : _busy(false), _key(0) {}
    Element(int key, std::string value) : _busy(true), _key(key), _value(std::move(value)) {}
    // Element(Element &other) : _busy(other._busy), _key(other._key), _value(other._value) {}
    Element(Element &other) = default;
    Element(Element &&other) noexcept {
        std::swap(_busy, other._busy);
        std::swap(_key, other._key);
        std::swap(_value, other._value);
    }

    Element &operator=(Element other) {
        if (this == &other) {
            return *this;
        }

        std::swap(_busy, other._busy);
        std::swap(_key, other._key);
        std::swap(_value, other._value);

        return *this;
    }
};

class Table {
private:
    Array<Element> _table;

public:
    Table();
    Table(size_t table_size);
    Table(Array<int> &keys, Array<std::string> &values);

    Table(const Table &another);
    Table(Table &&other);

    Element &operator[](size_t idx);
    const Element &operator[](size_t idx) const;

    friend std::istream &operator>>(std::istream &in, Table &t);
    friend std::ostream &operator<<(std::ostream &out, Table &t);

    Table &operator=(const Table &another);
    Table &operator=(Table &&other) noexcept;

    size_t size();
    [[nodiscard]] size_t capacity() const;

    Element *findByKey(int key);

    void garbageCollector();

    void insert(int key, std::string &value);

    void deleteByKey(int64_t key);

    void show(std::ostream &out);

    ~Table() = default;
};

#endif  // TABLE_H