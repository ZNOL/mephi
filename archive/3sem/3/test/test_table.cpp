#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "table.h"

TEST_CASE("Constructor") {
    REQUIRE_NOTHROW(Table());
    REQUIRE_NOTHROW(Table(123));
}

TEST_CASE("Copy constructor") {
    Table t(123);
    REQUIRE_NOTHROW(Table(t));

    Table copy_t(t);
    REQUIRE(t.size() == copy_t.size());
    REQUIRE(t.capacity() == copy_t.capacity());
}

TEST_CASE("Move constructor") {
    Table t(123);
    REQUIRE_NOTHROW(Table(std::move(t)));
}

TEST_CASE("capacity") {
    Table t(123);

    REQUIRE(t.capacity() == 123);
}

TEST_CASE("find") {
    Table t(123);
    REQUIRE(t.findByKey(123) == nullptr);
}

TEST_CASE("Insert") {
    Table t(123); std::string s = "ababa";
    REQUIRE_NOTHROW(t.insert(123, s));

    REQUIRE(t.findByKey(123)->_value == s);
}

TEST_CASE("size") {
    Table t(123);

    REQUIRE(t.size() == 0);

    std::string s = "aababbaba";
    for (size_t i = 0; i < 10; ++i) {
        t.insert(i, s);
        REQUIRE(t.size() == i + 1);
    }
    REQUIRE(t.size() == 10);
}

TEST_CASE("delete") {
    Table t(123); std::string s = "ababa";

    REQUIRE_THROWS_AS(t.deleteByKey(123), std::invalid_argument);

    t.insert(123, s);
    REQUIRE(t.findByKey(123)->_value == s);

    REQUIRE_NOTHROW(t.deleteByKey(123));
}

TEST_CASE("garbage collector") {
    Table t(123);
    static std::vector<std::string> ss{
        "ababa", "ababababab", "bababab", "ababab"
    };

    for (size_t i = 0; i < ss.size(); ++i) {
        t.insert(123 * i + 123, ss[i]);
    }

    REQUIRE_NOTHROW(t.deleteByKey(123 * 1 + 123));
    REQUIRE_NOTHROW(t.deleteByKey(123 * 3 + 123));

    REQUIRE(t.size() == 4);
    REQUIRE_NOTHROW(t.garbageCollector());
    REQUIRE(t.size() == 2);
}
