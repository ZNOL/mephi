#include "headers/cardioid.h"

Cardioid::Cardioid(double a) : _a(a) {}

Cardioid::Cardioid(const Cardioid &other) : _a(other.getA()) {}

double Cardioid::getA() const {
    return _a;
}

void Cardioid::setA(double _new_a) {
    _a = _new_a;
}

double Cardioid::radius(double phi) const {
    return 2 * _a * (1 - cos(phi));
}

double Cardioid::area() const {
    return 6 * M_PI * _a * _a;
}

double Cardioid::len(double phi) const {
    auto turns = static_cast<long long>(phi / (2 * M_PI));
    double ans = 16 * _a * static_cast<double>(turns);
    phi -= static_cast<double>(turns) * (2 * M_PI);
    ans += 8 * _a * (1 - cos(phi / 2));
    return ans;
}

std::tuple<double, double, double, double> Cardioid::getSpecialPoints() const {
    return std::make_tuple(
            radius(0),
            radius(M_PI / 3),
            radius(M_PI),
            radius(5 * M_PI / 3)
    );
}

bool Cardioid::health() {   // TODO
    return (_a > 0);
    /*if (_a <= 0) {
        return
        throw std::invalid_argument("Wrong value of parameter 'a'!");
    }*/
}
