#include <iostream>

#include "headers/cardioid.h"
#include "headers/dialog.h"

/*
Вариант 7. Кардиоида
Разработать класс, определяющий кривую – кардиоиду.
1) Определить состояние класса.
2) Разработать необходимые конструкторы и методы получения и изменения параметров,
определяющих кривую.

--3) Вернуть расстояние до центра в полярной системе координат в зависимости от угла для
точки принадлежащей кардиоиде.
4) Вернуть координаты наиболее удаленных от оси кардиоиды точек.
--5) Вернуть радиуса кривизны в характерных точках кардиоиды. 0 pi/3 pi 5pi/3
--6) Вернуть площадь описываемую кардиоидой.
--7) Вернуть длину дуги кардиоиды в зависимости от угла полярного радиуса
*/

int main() {
    Cardioid c(-1);

    while (true) {
        try {
            int rc = 1;
            while (rc != 0) {
                dialog::dialog(dialog::menu, rc);
                if (rc != 0) {
                    dialog::Funcs[rc](c);
                }
            }
            if (rc == 0) {
                std::cout << "Good luck" << std::endl;
                break;
            }
        }
        catch (const std::runtime_error &ex) {
            std::cout << "Something important went wrong!" << std::endl;
            std::cout << ex.what() << std::endl;
            break;
        }
        catch (const std::exception &ex) {
            std::cout << "Something went wrong!" << std::endl;
            std::cout << ex.what() << std::endl;
        }
    }

    return 0;
}