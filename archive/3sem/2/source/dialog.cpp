#include <vector>

#include "headers/safeinput.hpp"
#include "headers/cardioid.h"
// #include "headers/dialog.h"

namespace dialog {
    void dialog(const std::vector<std::string> &msgs, int &rc) {
        std::cout << std::endl;
        for (const auto &msg : msgs) {
            std::cout << msg << std::endl;
        }
        std::cout << std::endl;

        do {
            try {
                SafeGet<int>(std::cin, "Select the menu item", rc);
                if (rc < 0 || rc >= msgs.size()) {
                    std::cout << "Wrong menu number!" << std::endl;
                }
            }
            catch (const std::logic_error &ex) {
                std::cout << "Try better!" << std::endl;
            }
        } while (rc < 0 || rc >= msgs.size());
    }

    void D_health(Cardioid &c) {
        if (!c.health()) {
            throw std::invalid_argument("Wrong value of parameter 'a'!");
        }
    }

    void D_Radius(Cardioid &c) {
        D_health(c);

        double phi = -1;
        SafeGet<double>(std::cin, "Введите угол в радианах", phi);

        std::cout << c.radius(phi) << std::endl;
    }


    void D_Distant(Cardioid &c) {
        D_health(c);

        const std::vector<double> angels = {
                0,
                M_PI / 3,
                M_PI,
                5 * M_PI / 3,
        };

        for (const auto &phi : angels) {
            std::cout << "(" << phi << ", " << c.radius(phi) << ")" << std::endl;
        }
    }

    void D_Special(Cardioid &c) {
        D_health(c);

        auto t = c.getSpecialPoints();
        if (std::tuple_size<decltype(t)>::value != 4) {
            throw std::invalid_argument("Expected 4 element tuple!");
        }

        std::cout << std::get<0>(t) << std::endl;
        std::cout << std::get<1>(t) << std::endl;
        std::cout << std::get<2>(t) << std::endl;
        std::cout << std::get<3>(t) << std::endl;
    }

    void D_Area(Cardioid &c) {
        D_health(c);

        std::cout << c.area() << std::endl;
    }

    void D_Len(Cardioid &c) {
        D_health(c);

        double phi = 0;
        SafeGet<double>(std::cin, "Введите угол в радианах", phi);

        std::cout << c.len(phi) << std::endl;
    }

    void D_SetA(Cardioid &c) {
        double new_a = 0;
        SafeGet<double>(std::cin, "Задайте новое значение переменной 'a'", new_a);
        c.setA(new_a);
    }

    void D_GetA(Cardioid &c) {
        std::cout << c.getA() << std::endl;
    }

    void D_CircleOutput(Cardioid &c) {
        for (double phi = 0; phi < 2 * 3.14; phi += 2 * 3.14 / 100) {
            std::cout << c.radius(phi) << std::endl;
        }
    }
}