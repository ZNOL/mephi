#ifndef LAB2_DIALOG_H
#define LAB2_DIALOG_H

#include <vector>

#include "headers/cardioid.h"
#include "headers/safeinput.hpp"

namespace dialog {
    void dialog(const std::vector<std::string> &msgs, int &rc);

    void D_health(Cardioid &c);

    void D_Radius(Cardioid &c);

    void D_Distant(Cardioid &c);

    void D_Special(Cardioid &c);

    void D_Area(Cardioid &c);

    void D_Len(Cardioid &c);

    void D_SetA(Cardioid &c);

    void D_GetA(Cardioid &c);

    void D_CircleOutput(Cardioid &c);

    const std::vector<std::string> menu {
            "0. Завершить программу",
            "1. Вернить расстояние до центра в полярных координатах",
            "2. Вернуть координаты наиболее удаленных точек",
            "3. Вернуть характные точки",
            "4. Вернуть площадь кардиоды",
            "5. Вернуть длины дуги",
            "6. Задать кардиоиду",
            "7. Получить параметр кардиодиды",
            "8. Круговой вывод"
    };

    void (*Funcs[])(Cardioid &)  = {
            nullptr,                   // 0
            dialog::D_Radius,          // 1
            dialog::D_Distant,         // 2
            dialog::D_Special,         // 3
            dialog::D_Area,            // 4
            dialog::D_Len,             // 5
            dialog::D_SetA,            // 6
            dialog::D_GetA,            // 7
            dialog::D_CircleOutput,    // 8
    };
}
#endif //LAB2_DIALOG_H
