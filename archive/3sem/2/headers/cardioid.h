#ifndef LAB2_CARDIOID_H
#define LAB2_CARDIOID_H

#include <cmath>
#include <tuple>
#include <stdexcept>

class Cardioid {
private:
    double _a;

public:
    Cardioid() = default;

    explicit Cardioid(double a);

    double getA() const;

    Cardioid(const Cardioid& other);

    void setA(double _new_a);

    double radius(double phi) const;

    double area() const;

    double len(double phi) const;

    std::tuple<double, double, double, double> getSpecialPoints() const;

    bool health();

    ~Cardioid() = default;
};

#endif //LAB2_CARDIOID_H
