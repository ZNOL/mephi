bits	64
; Task #3
; res = ((d + b)(a − c) + (e − b)(e + b)) / b^2
; Знаковые 32, 16, 32, 16, 32
section .data
res:
	dq	0
a:
	dd 412; 4 byte 
b:
	dw 95   ; 2 byte 
c:
	dd 1111
d:
	dw 32
e:
	dd 54
tmp:
	dd 0
section .text
global _start
_start:
	mov ax, [d] 
	mov cx, [b]
	add ax, cx    	; ax = d + b
	jo overflow

	mov ecx, [a]
	mov edx, [c]
	sub ecx, edx  	; ecx = a - c
	jo overflow

	movsx eax, ax
	imul ecx      	; edx:eax = (d+b)(a-c)

	shl rdx, 32 
	or rdx, rax  	; rdx = edx:eax = (d+b)(a-c)

	; ---

	movsx ecx, word[b]
	mov eax, [e]
	sub eax, ecx  	; eax = e - b
	jo overflow
	; push eax     	; store in stack
	mov [tmp], eax

	mov eax, [e]
	add eax, ecx  	; eax = e + b
	mov ecx, [tmp]
	; pop ecx       ; ecx = e - b

	mov [res], rdx  ; res = (d+b)(a-c)

	imul ecx      	; edx:eax = (e-b)(e+b)

	mov esi, edx 		; movzx rsi, edx
	shl rsi, 32
	or rsi, rax    ; rsi = edx:eax = (e-b)(e+b)

	; ---
	
	mov rdx, [res]
	add rdx, rsi    ; rdx = (d+b)(a-c) + (e-b)(e+b)
	jo overflow

	mov [res], rdx  ; res = (d+b)(a-c) + (e-b)(e+b)
	; ---

	xor eax, eax 
	mov ax, [b]
	imul word[b]    ; dx:ax = b*b

	movzx esi, dx
	shl esi, 16
	add eax, esi    ; eax = dx:ax = b*b
	jo overflow

	cmp eax, 0
	je div_0

	mov [tmp], eax  ; tmp = b*b

	; ---

	mov rdx, [res]

	mov rax, rdx
	cqo
	
	mov ecx, [tmp]
	idiv rcx        ; rax - частное, rdx - остаток

	mov [res], rax

	cmp rdx, 0
	jge good_exit

	dec rax
	add rdx, rcx

	; ---

good_exit:
	mov eax, 60
	mov edi, 0
	syscall	

div_0:
	mov eax, 60
	mov edi, 1
	syscall

overflow:
	mov eax, 60
	mov edi, 1
	syscall
