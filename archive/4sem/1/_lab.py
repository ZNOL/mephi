a, b, c, d, e = map(int, input().split())

f = (d + b) * (a - c) + (e - b) * (e + b)
s = b ** 2

print(divmod(f, s))
