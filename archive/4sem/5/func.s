bits	64

section .text

global func
func:
	push rbp
	push rbx
	mov rbp, rsp
	sub rsp, 40

	mov [rsp + 0], rdi ; rdi - width
	mov [rsp + 8], rsi ; rsi - height
	mov [rsp + 16], rdx ; rdx - channels
	mov [rsp + 24], rcx ; rcx - im
	mov [rsp + 32], r8 ; r8  - nw

	mov rax, [rsp + 0]
	mov rcx, qword[rsp + 8]
	dec rcx
	mul rcx
	mul qword[rsp + 16]

	mov r8, [rsp + 32]
	add r8, rax
	mov [rsp + 32], r8

	; rdi - i
	; rcx - j
	; rsi - pos

	xor rdi, rdi; i
	.for_i_start:
		cmp rdi, [rsp + 8] 
		jge .for_i_finish
		
		xor r8, r8 

		xor rcx, rcx ; j
		.for_j_start:
			cmp rcx, [rsp + 0]
			jge .for_j_finish
			
			xor rsi, rsi ; byte_pos
			.for_pos_start:
				cmp rsi, [rsp + 16]
				jge .for_pos_finish

				mov rax, [rsp + 24]
				add rax, r8
				mov r10b, byte[rax]  ; cur_byte

				mov r11, [rsp + 32]
				add r11, r8
				mov byte[r11], r10b

				inc r8	
				inc rsi
				jmp .for_pos_start
			.for_pos_finish:

			inc rcx
			jmp .for_j_start
		.for_j_finish:

		mov rax, [rsp + 24]
		add rax, r8
		mov [rsp + 24], rax

		mov rax, [rsp + 32]
		sub rax, r8
		mov [rsp + 32], rax

		inc rdi
		jmp .for_i_start
	.for_i_finish:

	xor eax, eax

	add rsp, 40
	pop rbx
	pop rbp
	ret 
