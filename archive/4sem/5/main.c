#include <stdio.h>
#include <time.h>

#define STB_IMAGE_IMPLEMENTATION
#include "thirdparty/stb/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "thirdparty/stb/stb_image_write.h"

#ifdef C
#include "func.h"
#else
extern void func(int w, int h, int ch, unsigned char *im, unsigned char *nw);
#endif

void beauty_print(struct timespec *dur) {
  static char nanos[10];

  printf("%ld.", dur->tv_sec);
  nanos[9] = '\0';
  int nsec = dur->tv_nsec;
  for (int pos = 8; pos >= 0; --pos) {
    char numb = '0' + nsec % 10;
    nsec /= 10;
    nanos[pos] = numb;
  }
  printf("%s   ", nanos);
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    printf("Use %s <input>.png <output>.png\n", argv[0]);
    return 1;
  }

  int width = INT_MIN, height = INT_MIN, channels = INT_MIN;
  unsigned char *img = stbi_load(argv[1], &width, &height, &channels, 0);
  if (img == NULL) {
    printf("Error loading image\n");
    return 0;
  }
  // printf("Width = %d | height = %d | channels = %d\n", width, height,
  // channels);

  unsigned char *res = malloc(width * height * channels);

  struct timespec beg, end, dur;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &beg);

  func(width, height, channels, img, res);

  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &end);

  dur.tv_sec = end.tv_sec - beg.tv_sec;
  if (end.tv_nsec < beg.tv_nsec) {
    dur.tv_sec--;
    dur.tv_nsec = 1000000000 + end.tv_nsec - beg.tv_nsec;
  } else {
    dur.tv_nsec = end.tv_nsec - beg.tv_nsec;
  }

  beauty_print(&dur);

  stbi_write_png(argv[2], width, height, channels, res, width * channels);
  stbi_image_free(res);
}
