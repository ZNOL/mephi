#include <stdio.h>

void func(int w, int h, int ch, unsigned char *im, unsigned char *nw) {

  int in_one_row = ch * w;
  for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
      for (int byte_pos = 0; byte_pos < ch; ++byte_pos) {

        int cur_pos = in_one_row * i + ch * j + byte_pos;
        int mirrored_pos = in_one_row * (h - 1 - i) + ch * j + byte_pos;

        // printf("%d %d %d || ", i, j, byte_pos);
        // printf("%d | %d\n", cur_pos, mirrored_pos);

        nw[cur_pos] = im[mirrored_pos];
        nw[mirrored_pos] = im[cur_pos];
      }
    }
  }
}
