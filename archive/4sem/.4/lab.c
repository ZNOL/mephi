#include <math.h>
#include <stdint.h>
#include <stdio.h>

void calc(double x) {
  double res = 0;

  int idx = 1;

  double f1 = x * x * x;
  f1 *= 3.0;
  f1 /= 4.0;

  double f2 = 3 * 3 - 1;

  f1 *= f2;
  f1 /= 6.0;

  double tmp = 0;
  do {
    tmp = f1;

    if (idx & 1) {
      res += tmp;
    } else {
      res -= tmp;
    }

    f1 *= x * x;

    f1 /= f2;
    f2 += 1;
    f2 = f2 * 3 * 3 - 1;
    f1 *= f2;

    double f6 = 2 * idx + 2;
    f1 /= f6;
    f1 /= f6;

    ++idx;

  } while (tmp > 0.001);

  printf("%lf\n", res);
}

int32_t main() {
  double x;

  scanf("%lf", &x);

  calc(x);

  double tmp = sin(x);
  printf("math = %lf", tmp * tmp * tmp);

  return 0;
}
