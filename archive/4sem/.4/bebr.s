bits	64
section	.data
msg1:
	db	"Input x, a, n", 10, 0
format:
	db	"%f %f %d", 0
msg3:
	db	"ln[1-2*%f*cos(%f)+(%f)^2]=%f", 10, 0
msg4:
	db	"my(%f)=%f", 10, 0
file_name:
	times 16 db	0
mode_w:
	db	"w", 0
file_format:
	db	"%d: %f", 10, 0
mtwo	dd	-2.0
file_d	db	0
file_error:
	db	"Error in open file for write!", 10, 0
section	.text
one	dd	1.0
two	dd	2.0
n1	equ	8
nf	equ	n1+8
xn	equ	nf+8
a1	equ	xn+8
x1	equ	a1+8
ni	equ	x1+8
last	equ	ni+8
myexp:
	push	rbx
	push	rbp
	mov	rbp, rsp
	sub	rsp, last
	mov	rbx, rdi
	movss	dword [rbp-x1], xmm0
	movss	dword [rbp-a1], xmm1
	movss	dword [rbp-xn], xmm0
	movss	xmm0, dword [one]
	movss	dword [rbp-n1], xmm0
	movss	dword [rbp-nf], xmm0
	xorps	xmm0, xmm0
	movss	dword [rbp-last], xmm0
	; Открываем файл на запись
	mov	rdi, [file_name]
	mov	rsi, mode_w
	call fopen
	; Проверяем, успешно ли открылся файл
	cmp rax, 0
	je .open_error ; rax <= 0, ошибка открытия файла
	; Файл открыт, дескриптор в rax
	mov	[file_d], rax
	xor	rcx, rcx
.m0:
	inc	rcx
	mov	[rbp-ni], rcx
	movss	xmm0, dword [rbp-n1]
	movss	xmm1, dword [rbp-a1]
	mulss	xmm1, xmm0
	cvtss2sd xmm0, xmm1
	call	cos
	cvtsd2ss xmm1, xmm0
	movss	xmm0, dword [rbp-xn]
	movss	xmm2, dword [rbp-x1]
	mulss	xmm1, xmm0
	mulss	xmm0, xmm2
	movss	dword [rbp-xn], xmm0
	movss	xmm0, dword [rbp-nf]
	divss	xmm1, xmm0
	movss	xmm2, dword [rbp-n1]
	movss	xmm3, dword [one]
	addss	xmm2, xmm3
	mulss	xmm0, xmm2
	movss	dword [rbp-n1], xmm2
	movss	dword [rbp-nf], xmm0
	movss	xmm0, dword [rbp-last]
	movss	xmm2, dword [mtwo]
	mulss	xmm1, xmm2
	addss	xmm1, xmm0
	movss	dword [rbp-last], xmm1
	cvtss2sd    xmm0, xmm1
	mov	rdi, [file_d]
	mov	rsi, file_format
	mov	rdx, [rbp-ni]
	mov	eax, 2
	call	fprintf
	mov	rcx, [rbp-ni]
	cmp	rcx, rbx	; условие цикла
	jne	.m0
	mov	rdi, [file_d] ; передаем дескриптор файла в качестве аргумента
	call	fclose
	jmp	.exit
.open_error:	
	mov	edi, file_error
	xor	eax, eax
	call	printf
.exit:
	movss	xmm0, dword [rbp-last] 
	add	rsp, last
	pop	rbp
	pop	rbx
	ret
x	equ	8
a	equ	x+8
n	equ	a+8
y	equ	n+8
extern	printf
extern	scanf
extern	exp
extern	fopen
extern	fclose
extern	fprintf
extern	cos
extern	log
global	main
main:
	push	rbp
	mov	rbp, rsp
	mov	rdi, [rsi+8]
	sub	rsp, y
	mov	[file_name], rdi
	mov	edi, msg1
	xor	eax, eax
	call	printf
	mov	edi, format
	lea	rsi, [rbp-x]
	lea	rdx, [rbp-a]
	lea	rcx, [rbp-n]
	xor	eax, eax
	call	scanf
	movss	xmm1, dword [rbp-a]

	cvtss2sd xmm0, xmm1
	call	cos
	cvtsd2ss xmm1, xmm0

	movss	xmm5, dword [rbp-x]
	movss	xmm2, xmm5
	mulss	xmm2, xmm1
	movss	xmm3, [two]
	mulss	xmm2, xmm3	; получили 2*x*cos(a)
	movss	xmm1, xmm5
	mulss	xmm1, xmm5	; получили x**2	
	movss	xmm3, [one]
	addss	xmm1, xmm3
	subss	xmm1, xmm2	; получили многочлен
	cvtss2sd xmm0, xmm1
	call	log		; посчитали логорифм
	cvtsd2ss xmm1, xmm0
	movss	dword [rbp-y], xmm1
	mov	rdi, msg3
	movss	xmm5, dword [rbp-x]
	cvtss2sd xmm0, xmm5
	movss	xmm5, dword [rbp-a]
	cvtss2sd xmm1, xmm5
	movsd	xmm2, xmm0
	movss	xmm5, dword [rbp-y]
	cvtss2sd xmm3, xmm5
	mov	eax, 4
	call	printf
	movss	xmm0, dword [rbp-x]
	movss	xmm1, dword [rbp-a]
	mov	rdi, [rbp-n]
	call	myexp
	movss	[rbp-y], xmm0
	mov	edi, msg4
	movss	xmm5, dword [rbp-x]
	cvtss2sd xmm0, xmm5
	movss	xmm5, dword [rbp-y]
	cvtss2sd xmm1, xmm5
	mov	eax, 2
	call	printf
	leave
	xor	eax, eax
	ret
