""bits 64

section .data

msg1:
    db "Input x", 10, 0
msg2:
    db "Input e", 10, 0
msg3:
    db  "%f", 0
msg4:
    db  "  func(%f)=%.10f", 10, 0
msg5:
    db  "myfunc(%f)=%.10f", 10, 0
series_members:
    db  "%d - %f    %f", 10, 0

format:
    db  "w", 0

section .text
zero dd 0.0
one dd  1.0
none dd -1.0
eps dd  0.001

s_xmm0  equ 16
func:

    push    rbp
    mov     rbp, rsp
    sub rsp, s_xmm0
    movss   [rsp-s_xmm0], xmm0
    mulss   xmm0, [none]
    call expf
    movss   xmm1, [rsp-s_xmm0]
    addss   xmm1, [one]
    mulss   xmm0, xmm1
    leave
    ret


n   equ 4
f   equ n+8
ss_xmm0  equ f+8
s_eps    equ ss_xmm0+8
ss_xmm2  equ s_eps+8
ss_xmm3  equ ss_xmm2+8
ss_xmm4  equ ss_xmm3+8
ss_xmm5  equ ss_xmm4+8
ss_xmm6  equ ss_xmm5+8
ss_xmm7  equ ss_xmm6+8
ss_xmm8  equ ss_xmm7+8

myfunc:
    push rbp
    mov  rbp, rsp
    sub  rsp, ss_xmm8
    sub  rsp, 12
    mov  [rbp-f], rdi
    movss [rbp-s_eps], xmm1

    movss   xmm2, xmm0
    mulss   xmm2, xmm0; x (x^2)
    movss   xmm4, [one]; n-1 (1)
    movss   xmm5, [one]
    movss   xmm6, [one];
    addss   xmm6, [one]; n! (2)
    movss   xmm7, xmm2
    divss   xmm7, xmm6
    mulss   xmm7, [none]
    movss   xmm8, xmm7 ; rez
    addss   xmm8, [one]
.l:
    inc     dword[rbp-n]
    mulss   xmm7, [none]  ;* -1
    mulss   xmm7, xmm0  ;* x
    addss   xmm6, [one] ;n++
    divss   xmm7, xmm6  ;/n
    divss   xmm7, xmm4  ;/n-2
    addss   xmm4, [one] ;(n-2)+1
    mulss   xmm7, xmm4  ;*n-1
    addss   xmm8, xmm7  ;rez + mem


    movss   [rbp-ss_xmm0], xmm0
    movss   [rbp-ss_xmm2], xmm2
    movss   [rbp-ss_xmm3], xmm3
    movss   [rbp-ss_xmm4], xmm4
    movss   [rbp-ss_xmm5], xmm5
    movss   [rbp-ss_xmm6], xmm6
    movss   [rbp-ss_xmm7], xmm7
    movss   [rbp-ss_xmm8], xmm8

    cvtss2sd   xmm0, xmm8
    cvtss2sd   xmm1, xmm7
    mov     rdi, [rbp-f]
    mov     rsi, series_members
    mov     edx, [rbp-n]
    call    fprintf



    movss   xmm0, [rbp-ss_xmm0]
    movss   xmm2, [rbp-ss_xmm2]
    movss   xmm3, [rbp-ss_xmm3]
    movss   xmm4, [rbp-ss_xmm4]
    movss   xmm5, [rbp-ss_xmm5]
    movss   xmm6, [rbp-ss_xmm6]
    movss   xmm7, [rbp-ss_xmm7]
    movss   xmm8, [rbp-ss_xmm8]

    movss   xmm0, [rbp-ss_xmm0]
    movss   xmm9, xmm7
    ucomiss xmm9, [zero]
    jae     .comp
    mulss   xmm9, [none]
.comp:
    ucomiss xmm9, [rbp-s_eps] ;xmm1
    jae .l
    movss   xmm0, xmm8
    leave
    ret


extern  fopen
extern  fclose
extern  printf
extern  fprintf
extern  scanf
extern  expf
global  main

fw      equ 8
x       equ fw+4
e       equ x+4
res     equ e+8
main:

    push    rbp
    mov     rbp, rsp
    sub     rsp, res
    sub     rsp, 8
    cmp rdi, 2
    jne .exit_nofile
    mov rdi, [rsi+8]
    mov rsi, format
    call fopen
    or  rax, rax
    jle .exit_err_fopen
    mov [rbp-fw], rax


    mov rdi, msg1
    xor rax, rax
    call printf

    mov rdi, msg3
    lea rsi, [rbp-x]
    xor rax, rax
    call scanf
    or eax, eax
    jle .exit_err_scanf

    mov rdi, msg2
    xor rax, rax
    call printf

    mov rdi, msg3
    lea rsi, [rbp-e]
    call scanf
    or eax, eax
    jle  .exit_err_scanf

    movss   xmm0, [rbp-x]
    call    func
    movss   [rbp-res], xmm0
    mov     rdi, msg4
    cvtss2sd  xmm1, xmm0
    cvtss2sd  xmm0, [rbp-x]
    call printf

    movss   xmm0, [rbp-x]
    movss   xmm1, [rbp-e]
    ucomiss xmm1, [zero]
    jbe     .exit_zero
    mov     rdi, [rbp-fw]
    call    myfunc
    movss   [rbp-res], xmm0

    mov rdi, msg5
    cvtss2sd  xmm1, xmm0
    cvtss2sd  xmm0, [rbp-x]
    call printf

.exit_zero:

.exit_nofile:
.exit_err_scanf:
.exit_err_fopen:
    mov rdi, [rbp-fw]
    call fclose
    leave
    xor rax, rax
    ret
