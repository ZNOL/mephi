bits 64
section .data
; ---
input_message:
	db	"Input x, eps: ", 10, 0
; ---
input_format:
	db "%lf %lf", 0
; ---
output_format:
	db "sin(%lf)^3 = %lf", 10, 0
output_file_format:
	db "%d. %lf", 10, 0
; ---
clear_format:
	db "%*c", 0
; ---
file_format:
	db "w", 0
; ---
fdw:
	dd -1
; ---
invalid_args:
	db "Wrong amount of arguments", 0
invalid_input:
	db "Invalid input", 10, "Try again", 10, 10, 0 
file_error:
	db "Can't open file in write mod", 10, 0
; ---
zero:
	dq 0
one:
	dq 1.0
none:
	dq -1.0
two:
	dq 2.0
three:
	dq 3.0
four:
	dq 4.0
six:
	dq 6.0
eps:
	dq 0.0001 
; ---
section .text

extern printf
extern scanf
extern fopen
extern fclose
extern fprintf
extern sin 

fw  equ 8
x   equ fw+8

ss_xmm0 equ 8
ss_xmm1 equ ss_xmm0 + 8
ss_xmm2 equ ss_xmm1 + 8
ss_xmm4 equ ss_xmm2 + 8
ss_xmm5 equ ss_xmm4 + 8
ss_r8   equ ss_xmm5 + 8
ss_end  equ ss_xmm5 + 8

; xmm0 - input
lib_calculation:
	push rbp
	mov rbp, rsp
	
	call sin
	movsd xmm1, xmm0
	mulsd xmm0, xmm0
	mulsd xmm0, xmm1

	mov eax, 2
	mov rdi, output_format
	movsd xmm1, xmm0
	movsd xmm0, xmm15  
	call printf

	leave
	ret

; xmm0 - input
taylor_calculation:
	push rbp	
	mov rbp, rsp
	sub rsp, ss_end

	xor r8, r8        ; line counter
	inc r8            ; r8 = 1

	movsd xmm1, xmm0
	mulsd xmm1, xmm1
	mulsd xmm1, xmm0    ; xmm1 = x^3 - для степени x

	mulsd xmm1, [three]
	divsd xmm1, [four]

	movsd xmm2, [three]
	mulsd xmm2, xmm2    
	subsd xmm2, [one]   ; xmm2 = 3^2-1

	mulsd xmm1, xmm2    ; xmm1 = * 3^2-1 * x^3

	divsd xmm1, [six];  ; xmm1 = степень * иксы / факториал
	
	movsd xmm4, [zero]  ; tmp
	movsd xmm5, [zero]  ; res 

	jmp .while_start
	.while_start:
		movsd xmm4, xmm1
		
		test r8, 1	
		jz .ws_sub
		.ws_add:
			addsd xmm5, xmm4
			jmp .print
		.ws_sub:
			subsd xmm5, xmm4

		.print:
			jmp .xm_save
		.print_start:
			mov rdi, [fdw]
			mov rsi, output_file_format
			mov eax, 2
			mov rdx, r8
			movsd xmm0, xmm5
			call fprintf
			jmp .xm_load
		.print_finish:


		mulsd xmm1, xmm15
		mulsd xmm1, xmm15

		divsd xmm1, xmm2
		addsd xmm2, [one]
		mulsd xmm2, [three]
		mulsd xmm2, [three]
		subsd xmm2, [one]
		mulsd xmm1, xmm2

		cvtsi2sd xmm6, r8
		mulsd xmm6, [two]
		addsd xmm6, [two]
		divsd xmm1, xmm6
		addsd xmm6, [one]
		divsd xmm1, xmm6

		inc r8

		ucomisd xmm4, [eps]
		jae .while_start
	.while_end:

	mov eax, 2
	mov rdi, output_format
	movsd xmm1, xmm5
	movsd xmm0, xmm15  
	call printf

	add rsp, ss_end
	leave
	ret
	.xm_save:
		movsd [rbp - ss_xmm0], xmm0
		movsd [rbp - ss_xmm1], xmm1
		movsd [rbp - ss_xmm2], xmm2
		movsd [rbp - ss_xmm4], xmm4
		movsd [rbp - ss_xmm5], xmm5
		mov [rbp - ss_r8], r8
		jmp .print_start
	.xm_load:
		movsd xmm0, [rbp - ss_xmm0]
		movsd xmm1, [rbp - ss_xmm1]
		movsd xmm2, [rbp - ss_xmm2]
		movsd xmm4, [rbp - ss_xmm4]
		movsd xmm5, [rbp - ss_xmm5]
		mov r8, [rbp - ss_r8]
		jmp .print_finish


global main 
main:
	push rbp
	mov rbp, rsp
	sub rsp, x
	cmp rdi, 2
	jne .wrong_args

	mov rdi, [rsi + 8]
	mov rsi, file_format
	call fopen
	or rax, rax
	jle .wrong_file
	mov [rbp - fw], rax
	mov [fdw], rax

	.read_x:
		mov edi, input_message
		xor eax, eax
		call printf

		mov edi, input_format
		lea rsi, [rbp - x]
		lea rdx, [eps]
		xor eax, eax
		call scanf

		cmp eax, 2
		je .start
		cmp eax, 0
		jl .exit0

		mov rdi, invalid_input
		xor rax, rax
		call printf

		mov rdi, clear_format
		xor rax, rax
		call scanf
		jmp .read_x

		.start:
			movsd xmm0, [rbp - x]
			movsd xmm15, [rbp - x]
			
			call lib_calculation

			movsd xmm0, [rbp - x]   
			movsd xmm15, [rbp - x]
			call taylor_calculation

			jmp .exit0

	.wrong_file:
		mov edi, file_error 
		xor eax, eax
		call printf
		jmp .exit
	.wrong_args:
		mov edi, invalid_args
		xor eax, eax
		call printf
		jmp .exit
	.exit0:
		mov rdi, [rbp - fw]
		call fclose
	.exit:
		leave
		xor eax, eax
		ret

