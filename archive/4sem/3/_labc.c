#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *readline_(FILE *stream, const char *prompt) {
  if (stream == stdin) {
    printf("%s", prompt);
  }

  char buf[BUFSIZ] = {0};
  char *res = NULL;
  int len = 0;
  int n = 0;

  do {
    n = fscanf(stream, "%8192[^\n]", buf);
    if (n < 0) {
      if (!res) {
        return NULL;
      }
    } else if (n > 0) {
      int chunk_len = strlen(buf);
      int str_len = len + chunk_len;
      res = (char *)realloc(res, str_len + 1);
      memcpy(res + len, buf, chunk_len);
      len = str_len;
    } else {
      fscanf(stream, "%*c");
    }
  } while (n > 0);

  if (len > 0) {
    res[len] = '\0';
  } else {
    res = (char *)calloc(1, sizeof(char));
  }

  return res;
}

char *solve(char *input) {
  int idx_word_begin = 0;
  int idx_word_end = 0;
  int idx_write = 0;

  int need_space = 0;

  int n = strlen(input);
  while (idx_word_end < n) {
    while (input[idx_word_begin] == ' ') {
      ++idx_word_begin;
    }

    idx_word_end = idx_word_begin;
    while (idx_word_end + 1 < n && input[idx_word_end + 1] != ' ') {
      ++idx_word_end;
    }

    if (input[idx_word_begin] != input[idx_word_end]) {
      idx_word_begin = idx_word_end + 1;
      continue;
    }

    if (need_space) {
      input[idx_write++] = ' ';
    }

    while (idx_word_begin <= idx_word_end) {
      input[idx_write++] = input[idx_word_begin++];
    }
    need_space = 1;
  }
  input[idx_write] = '\0';

  return input;
}

char *solve2(char *input) {
  int cur_idx = 0;
  int n = strlen(input);

  while (cur_idx < n) {
  hyi:
    while (cur_idx < n && input[cur_idx] == ' ') {
      ++cur_idx;
    }

    int idx = 0;
    while (cur_idx < n) {
      input[idx++] = input[cur_idx++];
    }

    cur_idx = 0;

    while (cur_idx + 1 < n && input[cur_idx + 1] != ' ') {
      ++cur_idx;
    }

    if (cur_idx + 1 < n && input[cur_idx + 1] == ' ') {
      if (input[0] == input[cur_idx]) {
        char tmp = input[cur_idx + 2];
        input[cur_idx + 2] = '\0';
        printf("%s", input);
        input[cur_idx + 2] = tmp;
      }
      ++cur_idx;
      goto hyi;
    } else {
      break;
    }
  }
  return input;
}

int32_t main() {
  char *input = readline_(stdin, "Введите строку: ");

  char *output = solve(input);

  printf("%s\n", output);

  free(input);

  return 0;
}
