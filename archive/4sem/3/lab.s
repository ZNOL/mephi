bits 64
; Task # 51
; Удалить из строки слова, первый и последний символ которых различны.
; Ввод данных — стандартный поток ввода («с клавиатуры»).
; Вывод данных — файл.
; Способ передачи параметров: Через диалоговое взаимодействие с пользователем
section .data
; ---
inpsize	equ	5         ; define moment buffer_size
namelen	equ	1024
anslen	equ	100
; ---
msg1:
	db	"Enter string: "
msg1len	equ	$-msg1
; ---
inpline_additional0:
	db 32, 32, 32, 32, 32
inpline:
	times	inpsize	db	0
inpline_additional:
	db 0, 0, 0, 0, 0
outputline:
	times 1000 db 0
; ---
msg2:
	db	"File exists. Rewrite? (Y/N)", 10
msg2len	equ	$-msg2
; ---
ans:
	times	anslen	db	0
; ---
namefile:
	times	namelen	db	0
; ---
fdw:
	dd -1
; ---
flag:
	dq 0
tmp:                  ; previous first letter
	db 0

err2:
	db	"No such file or directory",	10	
err13:
	db	"Permission denied", 	10
err17:
	db	"File exists", 	10
err21:
	db	"Is a directory", 	10
err22:
	db 	"Invalid arguments",	10
err36:
	db	"File name is too long",	10
err255:
	db 	"Unknown error", 	10
errlist:
	times	2	dd err255     ; times ассемблирует несколько раз
	dd 	err2
	times	10	dd	err255
	dd	err13
	times	3	dd	err255
	dd	err17
	times	3	dd	err255
	dd	err21
	dd	err22
	times 13	dd	err255
	dd 	err36
	times 219	dd	err255
; ---
section .text

%define SPACE_CODE 32
%define TAB_CODE 9
%define ENTER_CODE 10

; param1 - rdi; result - rdi
is_separating_char:
	cmp rdi, SPACE_CODE
	je .true
	cmp rdi, TAB_CODE
	je .true
	cmp rdi, 10
	je .true

	jmp .false
	.true:
		mov rdi, 1
		ret
	.false:
		mov rdi, 0
		ret

; first_idx - rdi, last_idx - rdx; result - rdi
is_good_word:
	cmp byte[inpline + 1 * rdx], ENTER_CODE
	jne .else
	dec rdx
	.else:

	mv cl, byte[inpline + 1 * rdx]
	cmp cl, byte[inpline + 1 * rdi]
	jne .false
	.true:
		mov rdi, 1
		ret
	.false:
		mov rdi, 0
		ret

; r8 - word_begin; result - rdi
get_word_len:
	xor rdi, rdi ; len

	.while:
		cmp byte[inpline + rdi + r8], 0
		je .end_while

		movzx rdi, byte[inpline + rdi + r8]
		call is_separating_char
		cmp rdi, 1
		je .end_while

		inc rdi
		
		jmp .while
	.end_while:
	ret

; param1 - [rbp - 16]; result - rax
string_normalizer:
	push rbp
	mov rbp, rsp
	; sub rsp, 123123  ; под локальные переменные
	
	xor rdi, rdi         ; tmp for funcs
	xor r8, r8    			 ; idx_word_begin
	xor r9, r9    			 ; idx_word_end
	xor r10, r10  		   ; idx_write
	xor r11, r11         ; need_space
	.start_while:
		cmp byte[inpline + r8], 0
		je .end_while

		; find word start
		.start_begin_while:
			cmp byte[rax + 1 * r8], 0
			je .finish_while

			movzx rdi, byte[rax + 1 * r8]
			call is_separating_char
			cmp rdi, 0
			je .end_begin_while

			inc r8
			jmp .start_begin_while
		.end_begin_while:
		
		; r8 - word_start_idx
		call get_word_len
		; rdi - word_len

		add rdi, r8
		mov rdx, rdi
		mov rdi, r8
		dec rdx
		; rdi - start, rdx - finish

		call is_good_word
		cmp rdi, 1
		jne .else_bad
		.if_good:
			; move word to output buff
		.else_bad:




	.end_while:

	mov rsp, rbp
	pop rbp
	ret


global _start
_start:
; ---
	cmp	dword[rsp], 2	    ; кол-во аргументов
	je 	m0
	mov 	ebx, 22
	jmp 	exit0
m0:
	mov 	rdi, [rsp+16]
m01:
	mov	bx, [rdi+rdx]
	mov 	[namefile+rdx], bx
	inc 	edx
	cmp 	byte[rdi+rdx], 0
	jne	m01
	xor	edx, edx
	mov	eax, 2
	mov edi, namefile
	mov	esi, 0c1h
	mov	edx, 600o
	syscall
	or	eax, eax
	jge 	m5
	cmp	eax, -17
	je	m1
	mov	ebx, eax
	neg	ebx
	jmp	exit0
m1:
	mov	eax, 1
	mov	edi, 1
	mov	esi, msg2
	mov	edx, msg2len
	syscall
	mov	eax, 0
	mov 	edi, 0
	mov	esi, ans
	mov	edx, anslen
	syscall
	or	eax, eax
	jle	m2
	cmp	eax, ans
	jl	m3
m2:
	mov	ebx, 17
	jmp	exit0
m3:
	cmp	byte[ans], 'y'
	je	m4
	cmp	byte[ans], 'Y'
	je	m4
	mov	ebx, 17
	jmp	exit0
m4:
	mov	eax, 2
	mov	edi, namefile
	mov	esi, 201h
	syscall
	or	eax, eax
	jge	m5
	mov	ebx, eax
	neg	ebx
	jmp	exit0
m5:
	mov 	[fdw], eax
	jmp 	setup
exit0:
	or	ebx, ebx
	je	exit1
	mov	eax, 1
	mov	edi, 1
	mov	esi, [errlist+rbx*4]
	xor	edx, edx
perr:
	inc	edx
	cmp	byte[rsi+rdx-1], 10
	jne	perr
	syscall
exit1:
	cmp	dword[fdw], -1
	je	exit2
	mov	eax, 3
	mov	edi, [fdw]
	syscall
exit2:
	mov	edi, ebx
	mov	eax, 60
	syscall
setup:
work:
	mov	r15, 0
	mov	eax, 1
	mov	edi, 1
	mov	esi, msg1
	mov	edx, msg1len
	syscall	
work1:
	xor	eax, eax
	xor	edi, edi
	mov	rsi, inpline
	mov	rdx, inpsize
	syscall
	or	eax, eax
	jl 	ex_un
	je	ex_ok
; ---
	lea rax, inpline 
	push rax
	call string_normalizer
	add rsp, 8

make_print:
	mov	rsi, rax   ; string pointer
	mov	rdx, r10   ; string len
	mov	rax, 1
	mov	edi, [fdw]
	syscall

	jmp work
; ---
ex_ok:
	xor	edi, edi
	mov	rbx, 0
	jmp	exit0
ex_un:
	mov	rbx, 255
	jmp	exit0
