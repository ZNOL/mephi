bits	64
;	Revers symbols in words
section	.data
size	equ	8    ;1024
msg1:
	db	"Enter string: "
msg1len	equ	$-msg1
str:
	times size	db	0
msg2:
	db	"Result: '"
newstr:
	times size	db	0
section	.text
global	_start
_start:
	mov	eax, 1
	mov	edi, 1
	mov	esi, msg1
	mov	edx, msg1len
	syscall
	xor	eax, eax
	xor	edi, edi
	mov	esi, str
	mov	edx, size
	syscall
	or	eax, eax
	jl	m6 ;если ощибка
	je	m5 ;если EOF
	cmp	eax, size ;если пустая строка
	je	m6
	mov	esi, str ; указатель на строку в esi
	mov	edi, newstr ;указатель на новую строку в edi
	cmp	byte [rsi+rax-1], 10 ;в конце строки \n?
	jne	m6 ;если нет то ошибка
	xor	ecx, ecx    
m0: 
    ;вычисление длинны cлова 
	;+ в esi указатель на конец слова
    ;+ в ecx количество символов
    mov	al, [rsi]
	inc	esi
	cmp	al, 10 ;\n
	je	m1
	cmp	al, ' '
	je	m1
	cmp	al, 9 ;\t
	je	m1
	inc	ecx
	jmp	m0
m1:
	jecxz	m4 ;eсли длина строки 0
	cmp	edi, newstr ; проверка на начало первого слова,
	je	m2 ;            если это не первое слово, то 
	mov	byte [rdi], ' ' ;поставить пробел после предыдущего слова
	inc	edi             ;и переставить указатель на след символ
m2:
	mov	edx, esi ;
	dec	edx ;количство символов в слове
m3:
    ; /переписываем слово задом наперёд в newstr
	dec	edx
	mov	bl, [rdx]
	mov	[rdi], bl
	inc	edi
	loop	m3
m4:
	cmp	al, 10
	jne	m0; если не \n то в m0
	mov	word [rdi], 2560+"'"  ; ' + \n
	inc	edi
	inc	edi
	mov	eax, 1
	mov	esi, msg2
	mov	edx, edi
	sub	edx, msg2  ;вывод 
	mov	edi, 1
	syscall
	jmp	_start
m5:
	xor	edi, edi
	jmp	m7
m6:
	mov	edi, 1
m7:
	mov	eax, 60
	syscall
