bits	64
;	duplicate wowel letters in each word
section	.data
inpsize	equ	10
outsize equ	20
namelen	equ	1024
anslen	equ	3
msg1:
	db	"Enter string: "
msg1len	equ	$-msg1
inpline:
	times	inpsize	db	0
msg2:
	db	"File exists. Rewrite? (Y/N)", 10
msg2len	equ	$-msg2
outline:
	times	outsize	db	0
ans:
	times	anslen	db	0
err2:
	db	"No such file or directory",	10	
err13:
	db	"Permission denied", 	10
err17:
	db	"File exists", 	10
err21:
	db	"Is a directory", 	10
err22:
	db 	"Invalid arguments",	10
err36:
	db	"File name is too long",	10
err255:
	db 	"Unknown error", 	10
errlist:
	times	2	dd	err255
	dd 	err2
	times	10	dd	err255
	dd	err13
	times	3	dd	err255
	dd	err17
	times	3	dd	err255
	dd	err21
	dd	err22
	times	13	dd	err255
	dd 	err36
	times 	219	dd	err255
namefile:
	times	namelen	db	0
fdw:
	dd -1
section .text
global _start
_start:
	cmp	dword[rsp], 2	
	je 	m0
	mov 	ebx, 22
	jmp 	exit0
m0:
	mov 	rdi, [rsp+16]
m01:
	mov	bx, [rdi+rdx]
	mov 	[namefile+rdx], bx
	inc 	edx
	cmp 	byte[rdi+rdx], 0
	jne	m01
	xor	edx, edx
	mov	eax, 2
	mov 	edi, namefile
	mov	esi, 0c1h
	mov	edx, 600o
	syscall
	or	eax, eax
	jge 	m5
	cmp	eax, -17
	je	m1
	mov	ebx, eax
	neg	ebx
	jmp	exit0
m1:
	mov	eax, 1
	mov	edi, 1
	mov	esi, msg2
	mov	edx, msg2len
	syscall
	mov	eax, 0
	mov 	edi, 0
	mov	esi, ans
	mov	edx, anslen
	syscall
	or	eax, eax
	jle	m2
	cmp	eax, ans
	jl	m3
m2:
	mov	ebx, 17
	jmp	exit0
m3:
	cmp	byte[ans], 'y'
	je	m4
	cmp	byte[ans], 'Y'
	je	m4
	mov	ebx, 17
	jmp	exit0
m4:
	mov	eax, 2
	mov	edi, namefile
	mov	esi, 201h
	syscall
	or	eax, eax
	jge	m5
	mov	ebx, eax
	neg	ebx
	jmp	exit0
m5:
	mov 	[fdw], eax
	jmp 	setup
exit0:
	or	ebx, ebx
	je	exit1
	mov	eax, 1
	mov	edi, 1
	mov	esi, [errlist+rbx*4]
	xor	edx, edx
perr:
	inc	edx
	cmp	byte[rsi+rdx-1], 10
	jne	perr
	syscall
exit1:
	cmp	dword[fdw], -1
	je	exit2
	mov	eax, 3
	mov	edi, [fdw]
	syscall
exit2:
	mov	edi, ebx
	mov	eax, 60
	syscall
setup:
work:
	mov	r15, 0
	mov	eax, 1
	mov	edi, 1
	mov	esi, msg1
	mov	edx, msg1len
	syscall	
work1:
	xor	eax, eax
	xor	edi, edi
	mov	esi, inpline
	mov	edx, inpsize
	syscall
	or	eax, eax
	jl 	ex_un
	je	ex_ok
	mov	r14, 0 ; counter in inpline
	mov	r12, 0 ; counter in outline
	mov	r13, rax	
work2:
	jmp	write_ans ; holds logic of cnahge string; removes spaces, duplicate wowel letters
write_ans_back:
	inc	r14
	cmp 	r14, r13
	jl	work2
	cmp	r12, 1
	jle	make_print
	dec	r12
	cmp	byte[r10+r12 - 1], ' '
	je	remove_last_space
	jmp	repair_r12
remove_last_space:
	mov	r9b, byte[r10+r12]
	mov	byte[r10+r12-1], r9b
	jmp check_last_space
repair_r12:
	inc r12
	jmp check_last_space

check_last_space:
	cmp	byte[r10+r12-1], ' '
	jne	make_print
	dec	r12
make_print:
	
	mov	rax, 1
	mov	edi, [fdw]
	mov	rsi, outline
	mov	rdx, r12
	syscall
	cmp	rax, r12
	jne	ex_un
	cmp	r15, 2
	je	work
	jmp work1
			

write_ans:
	mov	r11, inpline
	mov	r10, outline
	cmp	byte[r11+r14], 10
	je endl
	cmp	r15, 0
	je	not_need_space
	jmp	need_space
not_need_space:
	cmp	byte[r11+r14], ' '
	je	write_end
	cmp	byte[r11+r14], 9
	je 	write_end
	jmp	write_letter
need_space:
	cmp	byte[r11+r14], ' '
	je	write_space
	cmp	byte[r11+r14], 9
	je	write_space
	jmp	write_letter
write_space:
	mov	byte[r10+r12], ' '
	inc	r12
	mov	r15, 0
	jmp	write_end
write_letter:
	cmp	byte[r11+r14], 66
	je	write_twice
	cmp	byte[r11+r14], 69
	je	write_twice
	cmp	byte[r11+r14], 73
	je	write_twice
	cmp	byte[r11+r14], 79
	je	write_twice
	cmp	byte[r11+r14], 85
	je	write_twice
	cmp	byte[r11+r14], 89
	je	write_twice
	cmp	byte[r11+r14], 97
	je	write_twice
	cmp	byte[r11+r14], 101
	je	write_twice
	cmp	byte[r11+r14], 105
	je	write_twice
	cmp	byte[r11+r14], 111
	je	write_twice
	cmp	byte[r11+r14], 117
	je	write_twice
	cmp	byte[r11+r14], 121
	je	write_twice
	jmp write_once
write_once:
	mov	r9b, byte[r11+r14]
	mov	byte[r10+r12], r9b
	inc	r12
	mov	r15, 1
	jmp 	write_end		
write_twice:
	mov 	r9b, byte[r11+r14]
	mov	byte[r10+r12], r9b
	inc	r12
	mov	byte[r10+r12], r9b
	inc	r12
	mov	r15, 1
	jmp 	write_end		
endl:
	mov	byte[r10+r12], 10
	inc	r12
	mov	r15, 2
	jmp	write_end
write_end:	
	jmp 	write_ans_back
		
ex_ok:
	xor	edi, edi
	mov	rbx, 0
	jmp	exit0
ex_un:
	mov	rbx, 255
	jmp	exit0
		
