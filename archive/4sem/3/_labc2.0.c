#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 6

char inputbuffer[BUFFER_SIZE];
char outputbuffer[500];

void addWord(int start_idx, int finish_idx, char *source, int dest_start,
             char *dest) {
  for (size_t i = start_idx; i < finish_idx; ++i) {
    dest[dest_start++] = source[i];
  }
}

void output(char *input) { 
  int n = strlen(input);


  int curInputIdx = 0;
  int curOutputIdx = 0;
}

int32_t main() {
  while (true) {
    scanf("%5s", &inputbuffer);
  }

  output(&inputbuffer);

  printf("%s\n", output);
  free(input);
  free(output);

  return 0;
}
