bits 64
; Task # 51
; Удалить из строки слова, первый и последний символ которых различны.
; Ввод данных — стандартный поток ввода («с клавиатуры»).
; Вывод данных — файл.
; Способ передачи параметров: Через диалоговое взаимодействие с пользователем
section .data
input_string:
	;db "asdasd asd as a dasd   aaa   			aaaa asd a   "
	db "  aaaa asd a   ", 0
; ---
inpsize	equ	5; define moment
namelen	equ	1024
anslen	equ	100
; ---
msg1:
	db	"Enter string: "
msg1len	equ	$-msg1
; ---
inpline_additional0:
	db 32, 32, 32, 32, 32
inpline:
	times	inpsize	db	0
inpline_additional:
	db 0, 0, 0, 0, 0
; ---
msg2:
	db	"File exists. Rewrite? (Y/N)", 10
msg2len	equ	$-msg2
; ---
ans:
	times	anslen	db	0
; ---
namefile:
	times	namelen	db	0
; ---
fdw:
	dd -1
; ---
flag:
	dq 0
diff:
 	dq 0
tmp:
	db 0

err2:
	db	"No such file or directory",	10	
err13:
	db	"Permission denied", 	10
err17:
	db	"File exists", 	10
err21:
	db	"Is a directory", 	10
err22:
	db 	"Invalid arguments",	10
err36:
	db	"File name is too long",	10
err255:
	db 	"Unknown error", 	10
errlist:
	times	2	dd err255     ; times ассемблирует несколько раз
	dd 	err2
	times	10	dd	err255
	dd	err13
	times	3	dd	err255
	dd	err17
	times	3	dd	err255
	dd	err21
	dd	err22
	times 13	dd	err255
	dd 	err36
	times 219	dd	err255
; ---
section .text

%define SPACE_CODE 32
%define TAB_CODE 9

; param1 - rdi; result - rdi
is_separating_char:
	cmp rdi, SPACE_CODE
	je .true
	cmp rdi, TAB_CODE
	je .true
	cmp rdi, 10
	je .true

	jmp .false
	.true:
		mov rdi, 1
		ret
	.false:
		mov rdi, 0
		ret

; param1 - [rbp - 16]; result - rax
string_normalizer:
	push rbp
	mov rbp, rsp
	; sub rsp, 123123  ; под локальные переменные
	
	mov rax, [rbp + 16]  ; string base pointer
	xor r8, r8    			 ; idx_word_begin
	xor r9, r9    			 ; idx_word_end
	xor r10, r10  		   ; idx_write
	xor r11, r11         ; need_space
	.start_while:
		cmp byte[rax + 1 * r8], 0
		je .finish_while
		.start_begin_while:
			cmp byte[rax + 1 * r8], 0
			je .finish_while

			cmp byte[rax + 1 * r8], 10
			jne .sbw_not_end
			mov rdi, 2
			jmp .finish_while
			
			.sbw_not_end:
			movzx rdi, byte[rax + 1 * r8]
			call is_separating_char
			cmp rdi, 0
			je .end_begin_while

			inc r8
			jmp .start_begin_while
		.end_begin_while:

		mov r9, r8
		.start_end_while:
			cmp byte[rax + 1 * (r9 + 1)], 0
			je .end_end_while
			cmp byte[rax + 1 * (r9 + 1)], 10
			je .end_end_while

			.sew_else:
				movzx rdi, byte[rax + 1 * (r9 + 1)]
				call is_separating_char
				cmp rdi, 0
				jne .end_end_while

				inc r9
				jmp .start_end_while
		.end_end_while:
		
		mov cl, byte[rax + 1 * r8]
		cmp cl, byte[rax + 1 * r9]
		jne .sw_if
		jmp .sw_else
		.sw_if:
			mov r8, r9
			inc r8
			jmp .start_while
		.sw_else:
			cmp r11, 1
			je .swe_if
			jmp .swe_else
			.swe_if:
				mov byte[rax + 1 * r10], SPACE_CODE
				inc r10
			.swe_else:
				.start_swee_while:
					cmp r8, r9
					jg .end_swee_while
					
					mov cl, byte[rax + 1 * r8]
					mov byte[rax + 1 * r10], cl

					inc r8
					inc r10
					jmp .start_swee_while
				.end_swee_while:
				mov r11, 1
				mov qword[flag], 1
		jmp .start_while
	.finish_while:
	cmp rdi, 2
	jne .skip_enter

	mov byte[rax + 1 * r10], 10
	inc r10
	mov qword[flag], 0
	mov qword[diff], 0
	jmp .add_string_end

	.skip_enter:

	mov r11, r10
	dec r10
	.find_word_begin:
		movzx rdi, byte[rax + 1 * r10]
		call is_separating_char
		cmp rdi, 0
		je .find_word_eng
		dec r10
		jmp .find_word_begin
	.find_word_eng:

	inc r10
	mov r8, r10
	xor rcx, rcx
	.put_word_begin:
		cmp r8, r11
		jge .put_word_end

		mov dil, byte[rax + 1 * r8]
		mov byte[rax + 1 * rcx], dil

		inc r8
		inc rcx
	.put_word_end:
	mov qword[diff], rcx

	cmp qword[flag], 1
	; cmp rdi, 1
	jne .add_string_end
	mov byte[rax + 1 * r10], SPACE_CODE
	inc r10
	mov qword[flag], 0

	.add_string_end:
	mov byte[rax + 1 * r10], 0

	mov rsp, rbp
	pop rbp

	ret

; param rax, r8
push_buffer:
	push rbp
	mov rbp, rsp

	mov rsi, rax
	mov rdx, r8
	mov rax, 1
	mov edi, [fdw]
	syscall

	mov rsp, rbp
	pop rbp
	ret
	
; param1 - [rbp - 16], result - rax
string_normalizer2:
	push rbp
	mov rbp, rsp
	
	mov rsp, rbp
	pop rbp
	ret


global _start
_start:
; ---
	cmp	dword[rsp], 2	    ; кол-во аргументов
	je 	m0
	mov 	ebx, 22
	jmp 	exit0
m0:
	mov 	rdi, [rsp+16]
m01:
	mov	bx, [rdi+rdx]
	mov 	[namefile+rdx], bx
	inc 	edx
	cmp 	byte[rdi+rdx], 0
	jne	m01
	xor	edx, edx
	mov	eax, 2
	mov edi, namefile
	mov	esi, 0c1h
	mov	edx, 600o
	syscall
	or	eax, eax
	jge 	m5
	cmp	eax, -17
	je	m1
	mov	ebx, eax
	neg	ebx
	jmp	exit0
m1:
	mov	eax, 1
	mov	edi, 1
	mov	esi, msg2
	mov	edx, msg2len
	syscall
	mov	eax, 0
	mov 	edi, 0
	mov	esi, ans
	mov	edx, anslen
	syscall
	or	eax, eax
	jle	m2
	cmp	eax, ans
	jl	m3
m2:
	mov	ebx, 17
	jmp	exit0
m3:
	cmp	byte[ans], 'y'
	je	m4
	cmp	byte[ans], 'Y'
	je	m4
	mov	ebx, 17
	jmp	exit0
m4:
	mov	eax, 2
	mov	edi, namefile
	mov	esi, 201h
	syscall
	or	eax, eax
	jge	m5
	mov	ebx, eax
	neg	ebx
	jmp	exit0
m5:
	mov 	[fdw], eax
	jmp 	setup
exit0:
	or	ebx, ebx
	je	exit1
	mov	eax, 1
	mov	edi, 1
	mov	esi, [errlist+rbx*4]
	xor	edx, edx
perr:
	inc	edx
	cmp	byte[rsi+rdx-1], 10
	jne	perr
	syscall
exit1:
	cmp	dword[fdw], -1
	je	exit2
	mov	eax, 3
	mov	edi, [fdw]
	syscall
exit2:
	mov	edi, ebx
	mov	eax, 60
	syscall
setup:
work:
	mov	r15, 0
	mov	eax, 1
	mov	edi, 1
	mov	esi, msg1
	mov	edx, msg1len
	syscall	
work1:
	xor	eax, eax
	xor	edi, edi
	mov	rsi, inpline
	add rsi, qword[diff]
	mov	rdx, inpsize
	sub rdx, qword[diff]
	syscall
	or	eax, eax
	jl 	ex_un
	je	ex_ok
; ---
	mov byte[inpline + 1 * rax], 0
	lea rax, inpline 
	push rax
	call string_normalizer
	add rsp, 8

make_print:
	mov	rsi, rax   ; string pointer
	mov	rdx, r10   ; string len
	mov	rax, 1
	mov	edi, [fdw]
	syscall

	jmp work
; ---
ex_ok:
	xor	edi, edi
	mov	rbx, 0
	jmp	exit0
ex_un:
	mov	rbx, 255
	jmp	exit0

