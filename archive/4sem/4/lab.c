#include <math.h>
#include <stdint.h>
#include <stdio.h>

void calc(double x) {

  double res = 1;
  double tmp = x * x;

  int idx = 2;

  tmp = tmp / idx;
  do {
    res += tmp;

    tmp *= x;
    ++idx;
    tmp /= idx;
    tmp *= x;
    ++idx;
    tmp /= idx;

  } while (tmp > 0.001);

  printf("%lf\n", res);
}

int32_t main() {
  double x;

  scanf("%lf", &x);

  calc(x);

  double tmp = cosh(x);
  printf("math = %lf\n\n", tmp);

  return 0;
}
