bits	64
; Task # 289
; Insertion sort
; line sort by max value
section .data
; ---
n:
	db 5
m:
	db 5
tmp:
	db 0
matrix:
	db 11,12,13,14,15
	db 21,22,23,24,25
	db 6,7,8,9,10
	db 16,17,18,19,20
	db 1,2,3,4,5
matrix2:  ; TODO перенос в новую матрицу
	db -1,-1,-1,-1,-1
	db -1,-1,-1,-1,-1
	db -1,-1,-1,-1,-1
	db -1,-1,-1,-1,-1
	db -1,-1,-1,-1,-1
line_max_value:
	db -128, -128, -128, -128, -128 
line_max_idx:
	dq 0, 0, 0, 0, 0
; ---
section .text
global _start
_start:
; ---
find_max_values:
	movsx rcx, byte[n]      ; rcx = n
	movsx r8, byte[m]

	xor rax, rax  					; rax = 0 - line shift 
	xor rdx, rdx            ; rdx = 0 - line number & line_max idx
	.for_i:
		push rcx
		movsx rcx, byte[m]    ; rcx = m

		.for_j:
			mov r10b, byte[matrix + rax + 1 * (rcx - 1)]
			cmp r10b, byte[line_max_value + 1 * rdx]
			jg update_max_line_val	

			.end_max_val_cmp:
			loop .for_j          ; --rcx, if rcx != 0 jump for_j

		add rax, r8           ; rax += m
		inc rdx               ; rdx += 1

		pop rcx
		loop .for_i
; ---
set_initial_state:
	xor rax, rax            ; rax = 0
	xor rdx, rdx            ; rdx = 0
	movsx rcx, byte[n]
	movsx r8, byte[m]
	.for_i:
		lea r10, [matrix + rax]
		mov [line_max_idx + 8 * rdx], r10
		movsx r10, byte[line_max_value + 1 * rdx]   ; TODO: remove
		mov r10, [line_max_idx + 8 * rdx]           ; TODO: remove
		add rax, r8           ; rax += m
		inc rdx
		loop .for_i
; ---
insertion_sort:
	xor rax, rax
	xor r10, r10
	mov rax, 1
	.for_i:
		cmp al, byte[n]
		jge .end_for_i
			
		mov r10b, byte[line_max_value + 1 * rax]
		mov r11, [line_max_idx + 8 * rax]

		mov rcx, rax
		dec cl
		.for_j:
			cmp byte[line_max_value + 1 * rcx], r10b
%ifdef ASC
			jle .end_for_j_first_state
%else
			jge .end_for_j_first_state
%endif

			.swap:
				mov r8b, byte[line_max_value + 1 * rcx]
				mov byte[line_max_value + 1 * (rcx + 1)], r8b

				mov r8, [line_max_idx + 8 * rcx]
				mov [line_max_idx + 8 * (rcx + 1)], r8
				
			cmp cl, 0
			je .end_for_j_second_state
			dec cl
			jmp .for_j

		.end_for_j_first_state:
		mov byte[line_max_value + 1 * (rcx + 1)], r10b
		mov [line_max_idx + 8 * (rcx + 1)], r11
		inc al
		jmp .for_i

		.end_for_j_second_state:
		mov byte[line_max_value + 1 * rcx], r10b
		mov [line_max_idx + 8 * rcx], r11
		inc al
		jmp .for_i

	.end_for_i:
; ---
print_matrix:
	xor r10, r10
	xor rax, rax
	xor r9, r9

	movsx r8, byte[m]
	.for_i:
		cmp al, byte[n]
		jge .end_for_i

		xor rdx, rdx
		.for_j:
			cmp dl, byte[m]
			jge .end_for_j

			mov rcx, [line_max_idx + 8 * rax]
			mov r10b, [rcx + 1 * rdx]             ; TODO: remove
			mov [matrix2 + r9 + 1 * rdx], r10b

			inc dl
			jmp .for_j	
		.end_for_j:

		add r9, r8   ; r9 += m

		inc al
		jmp .for_i
	.end_for_i:
; ---
good_exit:
	mov eax, 60
	mov edi, 0
	syscall	

update_max_line_val:
	mov [line_max_value + rdx * 1], r10b
	jmp find_max_values.end_max_val_cmp
