function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESENT(X, y, theta, alpha, num_iters) updates theta by
%   taking num_iters gradient steps with learning rate alpha

    % Инициализация некоторых полезных значений
    m = length(y); % количество обучающих примеров
    J_history = zeros(num_iters, 1);

    for iter = 1:num_iters
        % Вычисление предсказаний и ошибок
        predictions = X * theta;
        errors = predictions - y;

        % Обновление параметров с использованием градиента
        theta = theta - (alpha/m) * (X' * errors);

        % Сохранение значения функции стоимости J на каждой итерации
        J_history(iter) = computeCost(X, y, theta);
    end

end
