# Add your solution here
from numba import cuda

@cuda.jit
def cuda_histogram(x, xmin, xmax, histogram_out):
    '''Increment bin counts in histogram_out, given histogram range [xmin, xmax).'''
    # Note that we don't have to pass in nbins explicitly, because the size of histogram_out determines it
    nbins = histogram_out.shape[0]
    bin_width = (xmax - xmin) / nbins
    
    start = cuda.grid(1)
    stride = cuda.gridsize(1)
    
    # This is a very slow way to do this with NumPy, but looks similar to what you will do on the GPU
    for idx in range(start, x.shape[0], stride):
        bin_number = np.int32((x[idx] - xmin)/bin_width)
        if bin_number >= 0 and bin_number < histogram_out.shape[0]:
            cuda.atomic.add(histogram_out, bin_number, 1)
            #pass
        
