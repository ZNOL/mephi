---
title: "MelnikovLR"
output: html_document
---

```{r}
# https://hyper.mephi.ru/courses/course-v1:MEPhIx+CS712DS+2023Fall/courseware/4509c745418e4fc39d637d629f6c04cb/0dea38c6caf24d6e951f6d40aa25b7d2/?child=first

df = read.csv("zeta.csv")
df = subset(df, df$sex == 'F')
df = subset(df, select = -c(zcta, sex))
df = subset(df, 8 < meaneducation & meaneducation < 18)
df = subset(df, 10000 < meanhouseholdincome & meanhouseholdincome < 200000)
df = subset(df, 0 < meanemployment & meanemployment < 3)
df = subset(df, 20 < meanage & meanage < 60)
df$log_income = log10(df$meanhouseholdincome)
names(df) <- c('X', 'age', 'education', 'employment', 'income', 'log_income')
```

```{r}
library(ggplot2)

ggplot(df,aes(x= age, y=log_income)) +geom_point(alpha=0.2) +labs(x="age",y="income",title="log_income(age)")

```

```{r}
linearModel <- lm(log_income ~ age, df)
print(linearModel) # intercept - (пересечение с oy) b0
summary(linearModel)
# t-value - shows difference beetween coefficients and 0 | higher t-value - better!
# R-squared - (0;1) 0 - bad 1 - good | 0.01184 - 1.2% of variance in log_income can be explained by age - so our model is not good
# F-statistic - much larger then 1 indicates good linear regression model
#An F-statistic is calculated as the ratio of residual variance to predicted variance.

```

```{r}
ggplot(df,aes(x= education, y=log_income)) +geom_point(alpha=0.2) +labs(x="education",y="income",title="log_income(age)")
```

```{r}
linearModel_2 <- lm(log_income ~ education, df)
print(linearModel_2)
summary(linearModel_2)
# R-squared = 0.5354, much better then in previous model
# hex-bin plot
```

```{r}
#Is this model a good fit? (Подходит ли эта модель)
linearModel_3 <- lm(log_income ~ education + age + employment, df)
print(linearModel_3)
summary(linearModel_3)
```

```{r}
ggplot() + geom_point(aes(x= df$log_income, y=fitted(linearModel_3)), alpha=0.5) + geom_line(aes(x=df$log_income, y= df$log_income), col = 'red') + labs(x="income", y="Predicted income")
```
