\babel@toc {russian}{}\relax 
\contentsline {section}{\numberline {0.1}Цель работы}{2}{section.0.1}%
\contentsline {subsection}{\numberline {0.1.1}Подготовка к ЛР}{2}{subsection.0.1.1}%
\contentsline {subsection}{\numberline {0.1.2}Порядок выполнения}{2}{subsection.0.1.2}%
\contentsline {section}{\numberline {0.2}Комбинационная схема, построенная по функции, заданной в варианте}{3}{section.0.2}%
\contentsline {section}{\numberline {0.3}Этап определения состязаний в комбинационной схеме}{5}{section.0.3}%
\contentsline {subsection}{\numberline {0.3.1}Статические состязания}{5}{subsection.0.3.1}%
\contentsline {subsection}{\numberline {0.3.2}Логические состязания}{5}{subsection.0.3.2}%
\contentsline {subsection}{\numberline {0.3.3}Функциональные состязания}{6}{subsection.0.3.3}%
\contentsline {section}{\numberline {0.4}Синтез комбинационной схемы, свободной от статический и логических состязаний}{7}{section.0.4}%
