\babel@toc {russian}{}\relax 
\contentsline {section}{\numberline {0.1}Цель работы}{2}{section.0.1}%
\contentsline {subsection}{\numberline {0.1.1}Подготовка к ЛР}{2}{subsection.0.1.1}%
\contentsline {subsection}{\numberline {0.1.2}Порядок выполнения}{2}{subsection.0.1.2}%
\contentsline {section}{\numberline {0.2}Кодирование микроопераций регистра}{3}{section.0.2}%
\contentsline {section}{\numberline {0.3}Функция возбуждения входа D триггера i-ого разряда}{3}{section.0.3}%
\contentsline {section}{\numberline {0.4}Схема многофункционального регистра}{4}{section.0.4}%
\contentsline {section}{\numberline {0.5}Описание регистра на VHDL}{5}{section.0.5}%
\contentsline {subsection}{\numberline {0.5.1}Модель на VHDL}{5}{subsection.0.5.1}%
\contentsline {section}{\numberline {0.6}Функциональное моделирование}{6}{section.0.6}%
\contentsline {section}{\numberline {0.7}Динамические параметры регистров}{6}{section.0.7}%
