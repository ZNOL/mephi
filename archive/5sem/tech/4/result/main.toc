\babel@toc {russian}{}\relax 
\contentsline {section}{\numberline {0.1}Цель работы}{2}{section.0.1}%
\contentsline {subsection}{\numberline {0.1.1}Подготовка к ЛР}{2}{subsection.0.1.1}%
\contentsline {subsection}{\numberline {0.1.2}Порядок выполнения}{2}{subsection.0.1.2}%
\contentsline {section}{\numberline {0.2}Таблица переходов и матрицы переходов триггеров}{4}{section.0.2}%
\contentsline {subsection}{\numberline {0.2.1}Таблицы истинности}{4}{subsection.0.2.1}%
\contentsline {subsection}{\numberline {0.2.2}Матрицы переходов}{4}{subsection.0.2.2}%
\contentsline {section}{\numberline {0.3}Проектирование двухразрядного двоично-десятичного счетчика на DV-триггере}{5}{section.0.3}%
\contentsline {subsubsection}{Минимализация}{6}{table.caption.6}%
\contentsline {section}{\numberline {0.4}Разработать делитель частоты N = 7, S = 2}{9}{section.0.4}%
\contentsline {subsection}{\numberline {0.4.1}На Т-триггерах}{9}{subsection.0.4.1}%
\contentsline {subsubsection}{Минимизация}{9}{table.caption.17}%
\contentsline {subsection}{\numberline {0.4.2}НА CD4}{10}{subsection.0.4.2}%
\contentsline {subsection}{\numberline {0.4.3}Модель на VHDL}{11}{subsection.0.4.3}%
\contentsline {section}{\numberline {0.5}Схема для исследования счетчиков}{12}{section.0.5}%
\contentsline {subsection}{\numberline {0.5.1}Группы работают параллельно}{12}{subsection.0.5.1}%
\contentsline {subsection}{\numberline {0.5.2}Группы соединены последовательно}{12}{subsection.0.5.2}%
\contentsline {subsection}{\numberline {0.5.3}Записать в счетчик заданное число за 10 тактов}{12}{subsection.0.5.3}%
