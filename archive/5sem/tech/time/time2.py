from typing import List
from queue import PriorityQueue


class State():  # (time, state)
    def __init__(self, time, state):
        self.time: int = time
        self.state: int = state


class Base():   # (name, func, params)
    def __init__(self, name, func, params):
        self.name: str = name
        self.func = func
        self.params: List[str] = params

    def calc(self) -> int:
        if self.params is None or self.func is None:
            raise ValueError

        return self.func(self.params)

# TODO: INPUT & OUTPUT classes


class INV(Base):  # (name, params)
    @staticmethod
    def func_inv(params: List[str]):
        global global_state

        if len(params) == 0:
            raise ValueError

        cur_value = global_state[params[0]][-1].state
        new_value = (cur_value + 1) % 2

        return new_value

    def __init__(self, name, params):
        super(INV, self).__init__(name, self.func_inv, params)


class NAND(Base):  # (name, params)
    @staticmethod
    def func_nand(params: List[str]):
        global global_state

        if len(params) < 2:
            raise ValueError

        zeroes = 0
        for element in params:
            cur_value = global_state[element][-1].state
            if cur_value == 0:
                return 1
        return 0

    def __init__(self, name, params):
        super(NAND, self).__init__(name, self.func_nand, params)


class AND(Base):
    @staticmethod
    def func_and(params: List[str]):
        global global_state

        if len(params) < 2:
            raise ValueError

        for element in params:
            cur_value = global_state[element][-1].state
            if cur_value == 0:
                return 0
        return 1

    def __init__(self, name, params):
        super(AND, self).__init__(name, self.func_and, params)


class OR(Base):
    @staticmethod
    def func_or(params: List[str]):
        global global_state

        if len(params) < 2:
            raise ValueError

        for element in params:
            cur_value = global_state[element][-1].state
            if cur_value == 1:
                return 1
        return 0

    def __init__(self, name, params):
        super(OR, self).__init__(name, self.func_or, params)


class FDC(Base):
    @staticmethod
    def func(params):
        C = global_state[params["C"]][-1].state
        D = global_state[params["D"]][-1].state
        CLR = global_state[params["CLR"]][-1].state
        cur = global_state[params["name"]][-1].state

        if CLR == 1:
            return 0
        if C == 1 and D == 1:
            return (cur + 1) % 2
        return cur

    def __init__(self, name, params):
        super(FDC, self).__init__(name, self.func, params)


global_elements = dict()             # name: class
global_state = dict()                # name: [State(time, state)]
global_operations = PriorityQueue()  # (time, name, new_value)
global_graph = dict()                # name: [name]
global_wave = dict()                 # name: [str]


def init_all():
    global global_elements
    global global_state
    global global_operations
    global global_graph

    # init elements
    global_elements["A"] = Base("A", None, None)
    global_elements["R"] = Base("R", None, None)
    global_elements["C"] = Base("C", None, None)

    global_elements["I1"] = INV("I1", ["A"])
    global_elements["I2"] = INV("I2", ["A"])

    global_elements["D1"] = AND("D1", ["Q4", "I1"])
    global_elements["D2"] = AND("D2", ["Q1", "A"])
    global_elements["D3"] = OR("D3", ["D1", "D2"])

    global_elements["Q0"] = FDC(
        "Q0", {"name": "Q0", "C": "C", "D": "D3", "CLR": "R"})

    global_elements["D4"] = AND("D4", ["A", "Q0"])
    global_elements["D5"] = OR("D5", ["D4", "Q0"])
    global_elements["Q1"] = FDC(
        "Q1", {"name": "Q1", "C": "C", "D": "D5", "CLR": "R"})

    global_elements["D6"] = AND("D6", ["I2", "Q1"])
    global_elements["Q2"] = FDC(
        "Q2", {"name": "Q2", "C": "C", "D": "D6", "CLR": "R"})

    global_elements["Q3"] = FDC(
        "Q3", {"name": "Q3", "C": "C", "D": "Q2", "CLR": "R"})
    global_elements["Q4"] = FDC(
        "Q4", {"name": "Q4", "C": "C", "D": "Q3", "CLR": "R"})

    # init graph
    for name, element in global_elements.items():
        if element.params is not None:
            for param in element.params:
                if param not in global_graph:
                    global_graph[param] = []
                global_graph[param].append(name)

    print("Result graph:", global_graph)

    # init operations
    for i in range(0, 20):
        global_operations.put((i * 12, "C", i % 2))

    global_operations.put((0, "R", 1))
    global_operations.put((6, "R", 0))

    # init start_state
    global_state["C"] = [State(0, 0)]
    global_state["E1"] = [State(0, 0)]
    global_state["E2"] = [State(0, 0)]
    global_state["NS"] = [State(0, 0)]
    global_state["NR"] = [State(0, 1)]

    global_state["I2"] = [State(0, 1)]
    global_state["I3"] = [State(0, 1)]

    global_state["D1"] = [State(0, 1)]
    global_state["D2"] = [State(0, 1)]
    global_state["D3"] = [State(0, 1)]
    global_state["D4"] = [State(0, 0)]
    global_state["D5"] = [State(0, 0)]
    global_state["D6"] = [State(0, 1)]
    global_state["D7"] = [State(0, 1)]
    global_state["D8"] = [State(0, 0)]


def add_new_state(time: int, name: str, new_value: int):
    global global_state

    if name not in global_state:
        global_state[name] = []

    global_state[name].append(State(time, new_value))


def add_new_wave(parent: str, child: str, time, change: str):
    global global_wave

    if child not in global_wave:
        global_wave[child] = []

    result = ''
    if parent in global_wave:
        result = global_wave[parent][-1]

    result += f", {parent}({time})->{child}=[{change}]"
    global_wave[child].append(result)


# TODO: check
def export_state(states: List[State]) -> str:
    result = ''
    for i in range(0, len(states) - 1):
        cur_state = states[i]
        next_state = states[i + 1]
        amount = next_state.time - cur_state.time
        result += str(cur_state.state) * amount
        # print(f"{state.time}-{state.state}", end=' ')
    return result


def main():
    global global_operations
    global global_state
    global global_graph
    global global_wave

    init_all()

    last_time = -1
    while not global_operations.empty():
        time, name, new_value = global_operations.get()
        last_time = max(last_time, time)

        print(time, name, new_value)
        add_new_state(time, name, new_value)

        # check if something changed
        for child in global_graph[name]:
            cur_state = global_state[child][-1].state
            new_state = global_elements[child].calc()
            if cur_state == 0 and new_state == 1:
                global_operations.put((time + 3, child, new_state))
                add_new_wave(name, child, time, "0->1")

            elif cur_state == 1 and new_state == 0:
                global_operations.put((time + 2, child, new_state))
                add_new_wave(name, child, time, "1->0")

    for name in global_state.keys():
        add_new_state(last_time + 1, name, global_state[name][-1].state)

    print('Сынок!')

    for name, states in global_state.items():
        if len(name) < 2:
            name += ' '
        result = export_state(states)
        result = ','.join(i for i in result)
        print(f"{name}: {result}")

    print()
    for name in ["D5", "D6", "D7", "D8"]:
        wave = global_wave[name]

        print(f"{name}: ", " | ".join(i for i in wave))
        print()

    return


if __name__ == "__main__":
    main()
