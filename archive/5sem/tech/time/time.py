from typing import List
from queue import PriorityQueue


class State():  # (time, state)
    def __init__(self, time, state):
        self.time: int = time
        self.state: int = state


class Base():   # (name, func, params)
    def __init__(self, name, func, params):
        self.name: str = name
        self.func = func
        self.params: List[str] = params

    def calc(self) -> int:
        if self.params is None or self.func is None:
            raise ValueError

        return self.func(self.params)

# TODO: INPUT & OUTPUT classes


class INV(Base):  # (name, params)
    @staticmethod
    def func_inv(params: List[str]):
        global global_state

        if len(params) == 0:
            raise ValueError

        cur_value = global_state[params[0]][-1].state
        new_value = (cur_value + 1) % 2

        return new_value

    def __init__(self, name, params):
        super(INV, self).__init__(name, self.func_inv, params)


class NAND(Base):  # (name, params)
    @staticmethod
    def func_nand(params: List[str]):
        global global_state

        if len(params) < 2:
            raise ValueError

        zeroes = 0
        for element in params:
            cur_value = global_state[element][-1].state
            if cur_value == 0:
                return 1
        return 0

    def __init__(self, name, params):
        super(NAND, self).__init__(name, self.func_nand, params)


global_elements = dict()             # name: class
global_state = dict()                # name: [State(time, state)]
global_operations = PriorityQueue()  # (time, name, new_value)
global_graph = dict()                # name: [name]
global_wave = dict()                 # name: [str]


def init_all():
    global global_elements
    global global_state
    global global_operations
    global global_graph

    # init elements
    global_elements["NS"] = Base("NS", None, None)
    global_elements["E1"] = Base("E1", None, None)
    global_elements["C"] = Base("C", None, None)
    global_elements["E2"] = Base("E2", None, None)
    global_elements["NR"] = Base("NR", None, None)

    global_elements["I2"] = INV("I2", ["E2"])
    global_elements["I3"] = INV("I3", ["C"])

    global_elements["D1"] = NAND("D1", ["NR", "C", "E1", "E2"])
    global_elements["D2"] = NAND("D2", ["NS", "C", "E1", "I2"])

    global_elements["D3"] = NAND("D3", ["NS", "D1", "D4"])
    global_elements["D4"] = NAND("D4", ["NR", "D2", "D3"])

    global_elements["D5"] = NAND("D5", ["NR", "I3", "D3"])
    global_elements["D6"] = NAND("D6", ["NS", "I3", "D4"])

    global_elements["D7"] = NAND("D7", ["NS", "D5", "D8"])
    global_elements["D8"] = NAND("D8", ["NR", "D6", "D7"])

    # init graph
    for name, element in global_elements.items():
        if element.params is not None:
            for param in element.params:
                if param not in global_graph:
                    global_graph[param] = []
                global_graph[param].append(name)

    print("Result graph:", global_graph)

    # init operations
    for i in range(0, 10):
        global_operations.put((i * 12, "C", i % 2))

    global_operations.put((0, "NS", 0))
    global_operations.put((6, "NS", 1))

    global_operations.put((0, "NR", 1))
    global_operations.put((6, "NR", 0))
    global_operations.put((12, "NR", 1))

    global_operations.put((0, "E1", 0))
    global_operations.put((48, "E1", 1))
    global_operations.put((96, "E1", 0))

    global_operations.put((0, "E2", 0))
    global_operations.put((24, "E2", 1))
    global_operations.put((48, "E2", 0))
    global_operations.put((72, "E2", 1))
    global_operations.put((96, "E2", 0))

    # init start_state
    global_state["C"] = [State(0, 0)]
    global_state["E1"] = [State(0, 0)]
    global_state["E2"] = [State(0, 0)]
    global_state["NS"] = [State(0, 0)]
    global_state["NR"] = [State(0, 1)]

    global_state["I2"] = [State(0, 1)]
    global_state["I3"] = [State(0, 1)]

    global_state["D1"] = [State(0, 1)]
    global_state["D2"] = [State(0, 1)]
    global_state["D3"] = [State(0, 1)]
    global_state["D4"] = [State(0, 0)]
    global_state["D5"] = [State(0, 0)]
    global_state["D6"] = [State(0, 1)]
    global_state["D7"] = [State(0, 1)]
    global_state["D8"] = [State(0, 0)]


def add_new_state(time: int, name: str, new_value: int):
    global global_state

    if name not in global_state:
        global_state[name] = []

    global_state[name].append(State(time, new_value))


def add_new_wave(parent: str, child: str, time, change: str):
    global global_wave

    if child not in global_wave:
        global_wave[child] = []

    result = ''
    if parent in global_wave:
        result = global_wave[parent][-1]

    result += f", {parent}({time})->{child}=[{change}]"
    global_wave[child].append(result)


# TODO: check
def export_state(states: List[State]) -> str:
    result = ''
    for i in range(0, len(states) - 1):
        cur_state = states[i]
        next_state = states[i + 1]
        amount = next_state.time - cur_state.time
        result += str(cur_state.state) * amount
        # print(f"{state.time}-{state.state}", end=' ')
    return result


def main():
    global global_operations
    global global_state
    global global_graph
    global global_wave

    init_all()

    last_time = -1
    while not global_operations.empty():
        time, name, new_value = global_operations.get()
        last_time = max(last_time, time)

        print(time, name, new_value)
        add_new_state(time, name, new_value)

        # check if something changed
        for child in global_graph[name]:
            cur_state = global_state[child][-1].state
            new_state = global_elements[child].calc()
            if cur_state == 0 and new_state == 1:
                global_operations.put((time + 3, child, new_state))
                add_new_wave(name, child, time, "0->1")

            elif cur_state == 1 and new_state == 0:
                global_operations.put((time + 2, child, new_state))
                add_new_wave(name, child, time, "1->0")

    for name in global_state.keys():
        add_new_state(last_time + 1, name, global_state[name][-1].state)

    print('Сынок!')

    for name, states in global_state.items():
        if len(name) < 2:
            name += ' '
        result = export_state(states)
        result = ','.join(i for i in result)
        print(f"{name}: {result}")

    print()
    for name in ["D5", "D6", "D7", "D8"]:
        wave = global_wave[name]

        print(f"{name}: ", " | ".join(i for i in wave))
        print()

    return


if __name__ == "__main__":
    main()
