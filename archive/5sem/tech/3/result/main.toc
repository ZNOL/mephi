\babel@toc {russian}{}\relax 
\contentsline {section}{\numberline {0.1}Цель работы}{2}{section.0.1}%
\contentsline {subsection}{\numberline {0.1.1}Подготовка к ЛР}{2}{subsection.0.1.1}%
\contentsline {subsection}{\numberline {0.1.2}Порядок выполнения}{2}{subsection.0.1.2}%
\contentsline {section}{\numberline {0.2}Таблица переходов и матрицы переходов триггеров}{3}{section.0.2}%
\contentsline {subsection}{\numberline {0.2.1}Таблицы истинности}{3}{subsection.0.2.1}%
\contentsline {subsection}{\numberline {0.2.2}Матрицы переходов}{4}{subsection.0.2.2}%
\contentsline {section}{\numberline {0.3}Синтез триггера с заданной таблицей перехода}{5}{section.0.3}%
\contentsline {subsection}{\numberline {0.3.1}Двухступенчатый триггер}{6}{subsection.0.3.1}%
\contentsline {subsubsection}{Минимализация}{6}{table.caption.12}%
\contentsline {subsubsection}{Функции возбуждений}{7}{table.caption.15}%
\contentsline {subsection}{\numberline {0.3.2}ТТ с инвертором, И-НЕ и асинронными входами NR, NS}{7}{subsection.0.3.2}%
\contentsline {subsection}{\numberline {0.3.3}На основе JK-триггера}{8}{subsection.0.3.3}%
\contentsline {subsubsection}{Минимизация}{8}{table.caption.16}%
\contentsline {subsubsection}{Результат}{9}{table.caption.19}%
\contentsline {subsection}{\numberline {0.3.4}На основе DV-триггера}{9}{subsection.0.3.4}%
\contentsline {subsubsection}{Минимизация}{9}{table.caption.20}%
\contentsline {subsubsection}{Результат}{10}{table.caption.23}%
\contentsline {section}{\numberline {0.4}Модель на VHDL}{10}{section.0.4}%
\contentsline {subsection}{\numberline {0.4.1}Таблица истинности}{10}{subsection.0.4.1}%
\contentsline {subsubsection}{Минимализация}{10}{table.caption.24}%
\contentsline {subsubsection}{Результат}{11}{table.caption.26}%
\contentsline {subsubsection}{Код}{11}{table.caption.26}%
\contentsline {section}{\numberline {0.5}Выражения для задержек}{12}{section.0.5}%
\contentsline {section}{\numberline {0.6}Временные диаграммы}{13}{section.0.6}%
