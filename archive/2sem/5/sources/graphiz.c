#include "../headers/graph.h"
#include <stdio.h>
#include <stdlib.h>

void dfs2(Graph *gr, FILE *file)
{
    for (int i = 0; i < gr->size; ++i) {
        if (gr->nodes[i]) {
            for (listItem *ptr = gr->nodes[i]->head; ptr != NULL; ptr = ptr->next) {
                fprintf(file, "%s -> %s [label=%lld]\n", gr->nodes[i]->node, ptr->v, ptr->w);
            }
        }
    }
}

int printGraphViz(Graph *gr)
{
    if (gr->amount == 0) {
        return 0;
    }

    FILE *file = fopen("graph.gv","w");
    fprintf(file, "digraph G {\n");

    dfs2(gr, file);
    fprintf(file, "}");
    fclose(file);

    system("dot -Tpng graph.gv -o file.png");
    return 0;
}