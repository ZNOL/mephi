#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ull unsigned long long int

void streamClear(FILE *stream)
{
    fscanf(stream, "%*[^\n]");
    fscanf(stream, "%*c");
}

char *readline_(FILE *stream, const char *prompt)
{
    if (stream == stdin) printf("%s", prompt);

    char buf[BUFSIZ] = {0};
    char *res = NULL;
    int len = 0;
    int n = 0;

    do {
        n = fscanf(stream, "%8192[^\n]", buf);
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        }
        else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            res = (char *)realloc(res, str_len + 1);
            memcpy(res + len, buf, chunk_len);
            len = str_len;
        }
        else {
            fscanf(stream, "%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    }
    else {
        res = (char *)calloc(1, sizeof(char));
    }

    return res;
}

long long int getInt(FILE *stream, long long int *ans, const char *prompt)
{
    if (stream == stdin) printf("%s", prompt);

    int flag = 0;
    while ((flag = fscanf(stream, "%lld", ans)) != 1) {
        if (flag == -1) {
            *ans = 0;
            return 0;
        }
        if (stream == stdin) {
            printf("Ошибка при считывании числа. Повторите ввод\n");
            scanf("%*[^\n]");
        }
        else {
            return 0;
        }
    }
    streamClear(stream);
    return 1;
}
