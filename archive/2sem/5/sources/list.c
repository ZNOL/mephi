#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/list.h"

List *createList(char *node)
{
    List *list = calloc(1, sizeof(List));
    list->node = node;

    return list;
}

void clearListItem(listItem *item, int withChar)
{
    if (withChar) free(item->v);
    free(item);
}

void clearList(List *list, int withChar)
{
    listItem *ptr = list->head;
    while (ptr) {
        listItem *tmp = ptr;
        ptr = ptr->next;
        clearListItem(tmp, withChar);
    }
    if (withChar) free(list->node);
    free(list);
}

int pushList(List *list, char *v, long long int w)
{
    if (!list || !v) {
        return 1;
    }

    listItem *newIt = (listItem *)calloc(1, sizeof(listItem));
    if (!newIt) {
        return 1;
    }

    newIt->v = v;
    newIt->w = w;
    newIt->next = list->head;
    list->head = newIt;

    return 0;
}

int deleteFromList(List *list, char *v, int withChar)
{
    if (!list || !v || !list->head) {
        return 1;
    }

    if (strcmp(list->head->v, v) == 0) {
        listItem *tmp = list->head;
        list->head = list->head->next;
        clearListItem(tmp, withChar);
        return 0;
    }
    else {
        listItem *prev = list->head;
        listItem *ptr = list->head->next;
        while (ptr) {
            if (strcmp(ptr->v, v) == 0) {
                prev->next = ptr->next;
                clearListItem(ptr, withChar);
                return 0;
            }
            prev = ptr;
            ptr = ptr->next;
        }
        return 1;
    }
}

void printList(List *list)
{
    if (!list) {
        return;
    }

    printf("%s : ", list->node);
    for (listItem *ptr = list->head; ptr != NULL; ptr = ptr->next) {
        printf("%s ", ptr->v);
    }
    printf("\n");
}
