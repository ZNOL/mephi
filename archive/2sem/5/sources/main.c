#include <stdio.h>
#include "../headers/dialog.h"
#include "../headers/graphviz.h"


#define MAX_GRAPH_SIZE 200


int main()
{
    Graph *gr = initGraph(MAX_GRAPH_SIZE);

    const char *msgs[] = {
        "0. Выход из программы",
        "1. Добавление вершины",
        "2. Добавление ребра",
        "3. Удаление вершины",
        "4. Удаление ребра",
        "5. Вывод графа",
        "6. Поиск пути между двумя вершинами",  // поиск в глубину
        "7. Поиск кратчайшего пути между двумя вершинами",  // Беллман-Форд
        "8. Поиск минимального остовного дерева",  // Алгоритм Прима
        "9. Graphviz",
    };
    const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

    int (*fptr[])(Graph *) = {
        NULL,
        D_AddNode,
        D_AddEdge,
        D_DeleteNode,
        D_DeleteEdge,
        D_PrintGraph,
        D_FindPath,
        D_FindShortesPath,
        D_FindMinTree,
        printGraphViz,
    };

    int rc;
    while ((rc = dialog(msgs, NMsgs))) {
        if (fptr[rc](gr)) {
            break;
        }
    }

    clearGraph(gr, 1);

    return 0;
}