#include "../headers/graph.h"
#include "../headers/edges.h"
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

long long int hash(char *str, int size)
{
    long long int hash = LONG_MAX;
    for (int i = 0; str[i]; ++i) {
        hash = 37 * hash + str[i];
    }
    return abs(hash) % size;
}

Graph *initGraph(int size)
{
    Graph *gr = calloc(1, sizeof(Graph));
    gr->amount = 0;
    gr->size = size;
    gr->nodes = (List **)calloc(size, sizeof(List *));
    return gr;
}

void clearGraph(Graph *gr, int withChar)
{
    for (int i = 0; i < gr->size; ++i) {
        if (gr->nodes[i]) {
            clearList(gr->nodes[i], withChar);
        }
    }
    free(gr->nodes);
    free(gr);
}

List *addNodeGraph(Graph *gr, char* u)
{
    long long int idx = hash(u, gr->size);

    for (int i = 0; i < gr->size; ++i) {
        long long int idx_temp = abs(idx + 17 * i) % gr->size;
        if (gr->nodes[idx_temp] != NULL && strcmp(gr->nodes[idx_temp]->node, u) == 0) {
            return NULL;  // вершина существует
        }
        if (gr->nodes[idx_temp] == NULL) {
            gr->nodes[idx_temp] = createList(u);
            gr->amount++;
            return gr->nodes[idx_temp];
        }
    }

    return NULL;  // переполнение
}

List *findNodeGraph(Graph *gr, char *u)
{
    int idx = hash(u, gr->size);

    for (int i = 0; i < gr->size; ++i) {
        long long int idx_temp = abs(idx + 17 * i) % gr->size;
        if (gr->nodes[idx_temp] != NULL && strcmp(gr->nodes[idx_temp]->node, u) == 0) {
            return gr->nodes[idx_temp];
        }
        else if (gr->nodes[idx_temp] == NULL) {
            break;
        }
    }

    return NULL;
}

int addEdgeGraph(Graph *gr, char *u, char *v, long long int w)
{
    int flag = 0;

    List *list = findNodeGraph(gr, u);
    if (!list) {
        list = addNodeGraph(gr, u);
        flag = 1;  // создана новая вершина
        if (!list) {
            flag = -1;  // переполнение
        }
    }
    else {
        flag = 2;  // добавлено в существующую
    }

    List *list2 = findNodeGraph(gr, v);
    if (!list2) {
        char *new_v = (char *)calloc(strlen(v) + 1, sizeof(char));
        memcpy(new_v, v, sizeof(char) * strlen(v));
        addNodeGraph(gr, new_v);
    }

    pushList(list, v, w);

    return flag;
}

int deleteNodeGraph(Graph *gr, char *u, int withChar)
{
    int idx = hash(u, gr->size);

    for (int i = 0; i < gr->size; ++i) {
        long long int idx_temp = abs(idx + 17 * i) % gr->size;

        if (gr->nodes[idx_temp] != NULL && strcmp(gr->nodes[idx_temp]->node, u) == 0) {

            clearList(gr->nodes[idx_temp], withChar);
            gr->amount--;
            gr->nodes[idx_temp] = NULL;

            return 0;
        }
        else if (gr->nodes[idx_temp] == NULL) {
            break;
        }
    }

    return 1;  // вершина не найдена
}

int deleteEdgeGraph(Graph *gr, char *u, char *v)
{
    List *list = findNodeGraph(gr, u);
    if (!list) {
        return 1;  // вершина не найдена
    }

    return deleteFromList(list, v, 1);
}

void printGraph(Graph *gr)
{
    for (int i = 0; i < gr->size; ++i) {
        if (gr->nodes[i] != NULL) {
            printList(gr->nodes[i]);
        }
    }
}

void dfs(Graph *gr, char *cur, char *finish, List *result, Graph *used, int *flag)
{
    if (*flag == 1) return;

    addNodeGraph(used, cur);

    if (strcmp(cur, finish) == 0) {
        *flag = 1;
        return;
    }

    List *reachable = findNodeGraph(gr, cur);
    if (!reachable) {
        return;
    }
    for (listItem *v = reachable->head; v != NULL; v = v->next) {
        if (findNodeGraph(used, v->v) == NULL) {
            if (*flag == 0) {
                pushList(result, v->v, v->w);
            }
            dfs(gr, v->v, finish, result, used, flag);
            deleteNodeGraph(used, v->v, 0);
            if (*flag == 0) {
                deleteFromList(result, v->v, 0);
            }
        }
    }
}

List *findPath(Graph *gr, char *u, char *v)
{
    int flag = 0;
    List *list = createList(u);
    pushList(list, u, 0);
    Graph *used = initGraph(gr->amount);

    dfs(gr, u, v, list, used, &flag);

    clearGraph(used, 0);
    if (flag) {
        return list;
    }
    else {
        clearList(list, 0);
        return NULL;
    }
}

long long int getIndex(Graph *gr, char *u)
{
    int idx = hash(u, gr->size);

    for (int i = 0; i < gr->size; ++i) {
        long long int idx_temp = abs(idx + 17 * i) % gr->size;
        if (gr->nodes[idx_temp] != NULL && strcmp(gr->nodes[idx_temp]->node, u) == 0) {
            return idx_temp;
        }
        else if (gr->nodes[idx_temp] == NULL) {
            return idx_temp;
        }
    }

    return -1;
}

List *findShortesPath(Graph *gr, char *u, char *v)
{
    Edges *e = getEdges(gr);

    char **p = (char **)calloc(gr->size, sizeof(char *));

    long long int *d = (long long int *)calloc(gr->size, sizeof(long long int));
    for (int i = 0; i < gr->size; ++i) {
        d[i] = INT_MAX;
    }
    d[getIndex(gr, u)] = 0;

    for (int _ = 0; _ < gr->amount; ++_) {
        int flag = 0;
        for (Edge *ptr = e->head; ptr != NULL; ptr = ptr->next) {
            int u_idx = -1, v_idx = -1;
            if ((u_idx = getIndex(gr, ptr->u)) != -1 && d[u_idx] < INT_MAX) {
                if ((v_idx = getIndex(gr, ptr->v)) != -1 && d[v_idx] > d[u_idx] + ptr->w) {
                    d[v_idx] = d[u_idx] + ptr->w;
                    p[v_idx] = ptr->u;
                    flag = 1;
                }
            }
        }
        if (!flag) {
            break;
        }
    }

    for (Edge *ptr = e->head; ptr != NULL; ptr = ptr->next) {
        int u_idx = -1, v_idx = -1;
        if ((u_idx = getIndex(gr, ptr->u)) != -1 && d[u_idx] < INT_MAX &&
            (v_idx = getIndex(gr, ptr->v)) != -1 && d[v_idx] > d[u_idx] + ptr->w) {
            free(p);
            free(d);
            clearEdges(e);
            return NULL;  // найден цикл с отрицательным весом
        }
    }

    List *list = NULL;
    if (d[getIndex(gr, v)] != INT_MAX) {
        list = createList(u);
        char *cur = v;
        while (cur) {
            pushList(list, cur, 0);
            cur = p[getIndex(gr, cur)];
        }
    }

    free(p);
    free(d);
    clearEdges(e);

    return list;
}



Graph *findMinTree(Graph *gr)
{
    Graph *new_gr = initGraph(gr->size);

    Graph *used = initGraph(gr->amount);

    int *min_e = (int *)calloc(gr->size, sizeof(int));
    for (int i = 0; i < gr->size; ++i) {
        min_e[i] = INT_MAX;
    }
    for (int i = 0; i < gr->size; ++i) {
        if (gr->nodes[i]) {
            min_e[i] = 0;
            break;
        }
    }

    char **sel_e = (char **)calloc(gr->size, sizeof(char *));

    for (int i = 0; i < gr->amount; ++i) {
        char *v = NULL;
        for (int j = 0; j < gr->size; ++j) {
            if (gr->nodes[j] && findNodeGraph(used, gr->nodes[j]->node) == NULL &&
                (v == NULL || min_e[getIndex(gr, gr->nodes[j]->node)] < min_e[getIndex(gr, v)])
            ) {
                v = gr->nodes[j]->node;
            }
        }

        if (!v || min_e[getIndex(gr, v)] == INT_MAX) {
            clearGraph(new_gr, 0);
            clearGraph(used, 0);
            free(min_e);
            free(sel_e);
            return NULL;  // Минимальное остовное дерево не существует
        }

        addNodeGraph(used, v);
        if (sel_e[getIndex(gr, v)] != NULL) {
            addEdgeGraph(new_gr, sel_e[getIndex(gr, v)], v, min_e[getIndex(gr, v)]);
        }

        List *reachable = findNodeGraph(gr, v);
        for (listItem *ptr = reachable->head; ptr != NULL; ptr = ptr->next) {
            if (ptr->w < min_e[getIndex(gr, ptr->v)]) {
                min_e[getIndex(gr, ptr->v)] = ptr->w;
                sel_e[getIndex(gr, ptr->v)] = v;
            }
        }
    }

    free(min_e);
    free(sel_e);
    clearGraph(used, 0);

    return new_gr;
}
