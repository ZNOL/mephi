#include <stdlib.h>
#include "../headers/io.h"
#include "../headers/graph.h"

// должны вернуть 0, если всё хорошо


int dialog(const char *msgs[], int N)
{
    puts("");
    for (size_t i = 0; i < N; ++i) {
        puts(msgs[i]);
    }
    puts("");

    char *errmsg = "";
    long long int rc;
    do {
        puts(errmsg);
        errmsg = "Неверный номер пункта меню. Повторите ввод";
        if (!getInt(stdin, &rc, "")) {
            rc = 0;
        }
    } while (rc < 0 || rc >= N);

    return rc;
}

int D_AddNode(Graph *gr)
{
    char *u = readline_(stdin, "Введите вершину: ");

    addNodeGraph(gr, u);

    return 0;
}

int D_AddEdge(Graph *gr)
{
    char *u = readline_(stdin, "Введите 1ую вершину: ");
    char *v = readline_(stdin, "Введите 2ую вершину: ");

    long long int w = 0;
    getInt(stdin, &w, "Введите вес ребра: ");

    int flag = addEdgeGraph(gr, u, v, w);
    if (flag == -1) {
        free(u);
        free(v);
        printf("В графе недостаточно места\n");
        return 1;  // переполнение
    }
    else if (flag == 2) {
        free(u);
    }

    return 0;
}

int D_DeleteNode(Graph *gr)
{
    char *u = readline_(stdin, "Введите вершину: ");

    if (deleteNodeGraph(gr, u, 1)) {
        printf("Вершина не найдена");
    }

    free(u);

    return 0;
}

int D_DeleteEdge(Graph *gr)
{
    char *u = readline_(stdin, "Введите 1ую вершину: ");
    char *v = readline_(stdin, "Введите 2ую вершину: ");

    if (deleteEdgeGraph(gr, u, v)) {
        printf("Ребро не найдено!\n");
    }

    free(u);
    free(v);

    return 0;
}

int D_PrintGraph(Graph *gr)
{
    printGraph(gr);

    return 0;
}

int D_FindPath(Graph *gr)  // поиск в глубину
{
    char *u = readline_(stdin, "Введите начальную вершину: ");
    char *v = readline_(stdin, "Введите конечную вершину: ");
    List *result = findPath(gr, u, v);

    if (!result) {
        printf("Путь не найден\n");
    }
    else {
        printList(result);
        clearList(result, 0);
    }

    free(u);
    free(v);

    return 0;
}

int D_FindShortesPath(Graph *gr)  // Беллман-Форд
{
    char *u = readline_(stdin, "Введите начальную вершину: ");
    char *v = readline_(stdin, "Введите конечную вершину: ");
    List *list = findShortesPath(gr, u, v);

    if (!list) {
        printf("Путь не найден\n");
    }
    else {
        printList(list);
        clearList(list, 0);
    }

    free(u);
    free(v);

    return 0;
}

int D_FindMinTree(Graph *gr)  // Алгоритм Прима
{
    Graph *graph = findMinTree(gr);

    if (!graph) {
        printf("Минимального остовного дерева не существует\n");
    }
    else {
        printGraph(graph);
        clearGraph(graph, 0);
    }

    return 0;
}
