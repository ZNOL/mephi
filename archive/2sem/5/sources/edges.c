#include <stdlib.h>
#include "../headers/edges.h"
#include "../headers/graph.h"


Edges *createEdges()
{
    return (Edges *)calloc(1, sizeof(Edges));
}

void clearEdges(Edges *seq)
{
    for (Edge *ptr = seq->head; ptr != NULL;) {
        Edge *tmp = ptr;
        ptr = ptr->next;
        free(tmp);
    }
    free(seq);
}

int addEdge(Edges *seq, char *u, char *v, long long int w)
{
    if (!seq) {
        return 1;
    }
    Edge *newIt = (Edge *)calloc(1, sizeof(Edge));
    if (!newIt) {
        return 1;
    }

    newIt->u = u;
    newIt->v = v;
    newIt->w = w;
    newIt->next = seq->head;
    seq->head = newIt;

    return 0;
}

Edges* getEdges(Graph *gr)
{
    Edges *newEdges = createEdges();
    for (int i = 0; i < gr->size; ++i) {
        if (gr->nodes[i] != NULL) {
            List *cur = gr->nodes[i];
            for (listItem *ptr = cur->head; ptr != NULL; ptr = ptr->next) {
                addEdge(newEdges, cur->node, ptr->v, ptr->w);
            }
        }
    }
    return newEdges;
}