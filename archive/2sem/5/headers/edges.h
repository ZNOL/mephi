#pragma once
typedef struct Edge {
    char *u;
    char *v;
    long long int w;

    struct Edge *next;
} Edge;

typedef struct Edges
{
    Edge *head;
} Edges;

Edges *createEdges();

void clearEdges(Edges *seq);

int addEdge(Edges *seq, char *u, char *v, long long int w);
