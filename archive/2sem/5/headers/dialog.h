#include "graph.h"
#pragma once

// должны вернуть 0, если всё хорошо

int dialog(const char *msgs[], int N);

int D_AddNode(Graph *gr);

int D_AddEdge(Graph *gr);

int D_DeleteNode(Graph *gr);

int D_DeleteEdge(Graph *gr);

int D_PrintGraph(Graph *gr);

int D_FindPath(Graph *gr);

int D_FindShortesPath(Graph *gr);

int D_FindMinTree(Graph *gr);
