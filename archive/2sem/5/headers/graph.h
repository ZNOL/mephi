#include "list.h"
#include "edges.h"
#pragma once

typedef struct Graph {
    int size;
    int amount;
    List **nodes;
} Graph;

long long int hash(char *str, int size);

Graph *initGraph(int size);

void clearGraph(Graph *gr, int withChar);

List *addNodeGraph(Graph *gr, char* u);

List *findNodeGraph(Graph *gr, char *u);

int addEdgeGraph(Graph *gr, char *u, char *v, long long int w);

int deleteNodeGraph(Graph *gr, char *u, int withChar);

int deleteEdgeGraph(Graph *gr, char *u, char *v);

void printGraph(Graph *gr);

List *findPath(Graph *gr, char *u, char *v);

long long int getIndex(Graph *gr, char *u);

List *findShortesPath(Graph *gr, char *u, char *v);

Edges* getEdges(Graph *gr);

Graph *findMinTree(Graph *gr);