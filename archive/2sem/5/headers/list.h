#pragma once

typedef struct listItem {
    char* v;
    long long int w;

    struct listItem *next;
} listItem;

typedef struct List {
    char *node;
    listItem *head;
} List;

List *createList(char *node);
void clearListItem(listItem *item, int withChar);
void clearList(List *list, int withChar);
int pushList(List *list, char *v, long long int w);
int deleteFromList(List *list, char *v, int withChar);
void printList(List *list);
