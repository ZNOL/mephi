#include <stdio.h>
#include <stdlib.h>
#include "../headers/io.h" 
#include "../headers/func.h"

int main()
{
    char *str = readline_(stdin, "Введите инфиксную формулу: ");
    while (str)
    {
        char *tmp = solve(str);
        free(str);

        printf("\n%s\n\n", tmp);
        free(tmp);

        str = readline_(stdin, "Введите инфиксную формулу: ");
    }   

    return 0;
}
