#include <stdlib.h>
#include "../../headers/list.h"
#include "../../headers/stack.h"

Stack *create_stack()
{
    Stack *stack = (Stack *)calloc(1, sizeof(Stack));
    stack->size = 0;
    stack->basic = create_list();
    return stack;
}

void clear_stack(Stack *stack)
{
    clear_list((List *)stack->basic);
    free(stack);
}

char top(Stack *stack)
{
    return top_list((List *)stack->basic);
}

int push(Stack *stack, char data)
{
    if (push_list((List *)stack->basic, data)) {
        return 1;
    }
    else {
        ++stack->size;
        return 0;
    }
}

int pop(Stack *stack)
{
    if (pop_list((List *)stack->basic)) {
        return 1;
    }
    else {
        --stack->size;
        return 0;
    }
}
