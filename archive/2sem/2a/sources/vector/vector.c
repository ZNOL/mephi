#include <stdlib.h>
#include "../../headers/vector.h"
#include "../../headers/io.h"

Vector *create_vector()
{
    Vector *new = (Vector *)calloc(1, sizeof(Vector));
    new->top = -1;
    new->maxsize = MAXSIZE;
    // new->maxsize = get_digit("Введите максимальный размер стека: ");
    // stdin_clear();    
    // printf("%d\n", MAXSIZE);
    new->seq = (char *)malloc(sizeof(char) * new->maxsize);
    return new;
}

void clear_vector(Vector *vector)
{
    free(vector->seq);
    free(vector);
}

int push_vector(Vector *vector, char data)
{
    if (data && vector->top + 1 < vector->maxsize) {
        vector->seq[++vector->top] = data;
        return 0;
    }
    else {
        return 1;
    }
}

int pop_vector(Vector *vector)
{
    if (vector->top != -1) {
        --vector->top;
        return 0;
    }
    else {
        return 1;
    }
}

char top_vector(Vector *vector)
{
    if (vector->top != -1) {
        return vector->seq[vector->top];
    }
    else {
        return '\0';
    }
}
