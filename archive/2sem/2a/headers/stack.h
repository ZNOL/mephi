#pragma once

typedef struct Stack
{
    int size;
    void *basic;
} Stack;

Stack *create_stack();
void clear_stack(Stack *stack);
char top(Stack *stack);
int push(Stack *stack, char data);
int pop(Stack *stack);
