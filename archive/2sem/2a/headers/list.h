#pragma once

typedef struct Item {
    char data;
    struct Item *next;
} Item;

typedef struct List {
    Item *head;
} List;

List *create_list();
void clear_list(List *list);
int push_list(List *list, char data);
int pop_list(List *list);
char top_list(List *list);

