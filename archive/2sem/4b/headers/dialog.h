#pragma once
#include "btree.h"

int dialog(const char *msgs[], int NMsgs);

int D_Add(BTree *tree);
int D_Delete(BTree *tree);
int D_Show(BTree *tree);
int D_Round(BTree *tree);
int D_Find(BTree *tree);
int D_MinFind(BTree *tree);
int D_FileInput(BTree *tree);
