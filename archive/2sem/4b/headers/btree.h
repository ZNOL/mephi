#pragma once

#define T 2

typedef struct Node {
    int n;
    struct Node *parent;

    char *key[2 * T - 1];

    char *info[2 * T - 1];

    struct Node *ptr[2 * T];
} Node;


typedef struct BTree {

    Node *root;

} BTree;


void clearNode(Node *node);
void clearTree(Node *ptr);
int findIdx(char *key, Node *ptr);

int addNode(BTree *tree, char *key, char *info);
int deleteKey(BTree *tree, char *key);

Node *findKey(BTree *tree, char *key);
Node *findMin(BTree *tree);

void dfs(Node *ptr);
int roundTree(BTree *tree, char *key);

void printTree(Node *ptr, int lvl);
int showTree(BTree *tree, char *key);
