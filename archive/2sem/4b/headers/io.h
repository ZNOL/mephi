#pragma once
#include <stdio.h>

void streamClear(FILE *stream);
char *readline_(FILE *stream, const char *prompt);
long long int getInt(FILE *stream, long long int *ans);
