#include "../headers/btree.h"
#include <stdio.h>
#include <stdlib.h>

void dfs2(Node *ptr, FILE *file)
{
    if (!ptr) {
        return;
    }

    fprintf(file, "struct%p [label=\"", ptr);
    if(ptr->key[0]) {
        fprintf(file, "<f0> %s|", ptr->key[0]);
    }
    else {
        fprintf(file, "<f0> null|");
    }
    if (ptr->key[1]) {
        fprintf(file, "<f1> %s|", ptr->key[1]);
    }
    else {
        fprintf(file, "<f1> null|");
    }
    if (ptr->key[1]) {
        fprintf(file, "<f2> %s\"];\n", ptr->key[2]);
    }
    else {
        fprintf(file, "<f2> null\"];\n");
    }

    if (ptr->ptr[0]) {
        fprintf(file, "struct%p:f0 -- struct%p:f1;\n", ptr, ptr->ptr[0]);
        dfs2(ptr->ptr[0], file);
    }
    if (ptr->ptr[1]) {
        fprintf(file, "struct%p:f1 -- struct%p:f1;\n", ptr, ptr->ptr[1]);
        dfs2(ptr->ptr[1], file);
    }
    if (ptr->ptr[2]) {
        fprintf(file, "struct%p:f1 -- struct%p:f1;\n", ptr, ptr->ptr[2]);
        dfs2(ptr->ptr[2], file);
    }
    if (ptr->ptr[3]) {
        fprintf(file, "struct%p:f2 -- struct%p:f1;\n", ptr, ptr->ptr[3]);
        dfs2(ptr->ptr[3], file);
    }
}

int printGraphViz(BTree *tree)
{
    if (tree->root == NULL) {
        return 1;
    }

    FILE *file = fopen("graph.gv","w");
    fprintf(file, "graph G {\n");
    fprintf(file, "node [shape=record];\n");

    dfs2(tree->root, file);

    fprintf(file, "}");
    fclose(file);

    system("dot -Tpng graph.gv -o file.png");
    return 0;
}