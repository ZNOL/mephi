#include <stdlib.h>
#include <string.h>
#include "../headers/io.h"
#include "../headers/btree.h"

// должны вернуть 0, если всё хорошо

int dialog(const char *msgs[], int N)
{
    puts("");
    for (size_t i = 0; i < N; ++i) {
        puts(msgs[i]);
    }
    puts("");

    char *errmsg = "";
    long long int rc;
    do {
        puts(errmsg);
        errmsg = "Неверный номер пункта меню. Повторите ввод";
        if (!getInt(stdin, &rc)) {
            rc = 0;
        }
    } while (rc < 0 || rc >= N);

    return rc;
}

int D_Add(BTree *tree)
{
    long long int amount = 1;
    printf("Введите количество вершин для добавления: ");
    getInt(stdin, &amount);

    while (amount-- > 0) {
        char *key = readline_(stdin, "Ключ: ");
        char *info = readline_(stdin, "Поле info: ");

        addNode(tree, key, info);
    }
    return 0;
}

int D_Delete(BTree *tree)
{
    char *key = readline_(stdin, "Введите ключ для удаления: ");

    deleteKey(tree, key);

    free(key);
    return 0;
}

int D_Show(BTree *tree)
{
    char *key = readline_(stdin, "Введите начальный ключ для вывода дерева или -1 для вывода c корня: ");

    showTree(tree, key);

    free(key);
    return 0;
}

int D_Round(BTree *tree)
{
    char *key = readline_(stdin, "Введите начальный ключ для обхода дерева или -1 для вывода c корня: ");

    roundTree(tree, key);

    free(key);
    return 0;
}

int D_Find(BTree *tree)
{
    char *key = readline_(stdin, "Введите ключ для поиска: ");

    Node *ptr = findKey(tree, key);
    if (ptr) {
        for (int i = 0; i < 2 * T - 1; ++i) {
            if (ptr->key[i] && strcmp(key, ptr->key[i]) == 0) {
                printf("key = %s | info = %s\n", ptr->key[i], ptr->info[i]);
                break;
            }
        }
    }
    else {
        printf("Ключ не найден\n");
    }

    free(key);
    return 0;
}

int D_MinFind(BTree *tree)
{
    Node *ptr = findMin(tree);
    if (ptr) {
        for (int i = 0; i < 3; ++i) {
            if (ptr->key[i]) {
                printf("key = %s | info = %s\n", ptr->key[i], ptr->info[i]);
                break;
            }
        }
    }
    else {
        printf("Дерево пустое\n");
    }

    return 0;
}

int D_FileInput(BTree *tree)
{
    char *filename = readline_(stdin, "Введите название файла: ");

    FILE *file = fopen(filename, "r");
    if (!file) {
        printf("Неверное название файла\n");
        free(filename);
        return 0;
    }

    long long amount = -1;
    getInt(file, &amount);
    while (amount-- > 0) {
        char *key = readline_(file, "");
        char *info = readline_(file, "");

        addNode(tree, key, info);
    }

    free(filename);
    fclose(file);

    return 0;
}
