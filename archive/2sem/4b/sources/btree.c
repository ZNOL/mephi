#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/btree.h"

void clearNode(Node *node)
{
    for (int i = 0; i < 2 * T - 1; ++i) {
        if (node->key[i]) {
            free(node->key[i]);
        }
        if (node->info[i]) {
            free(node->info[i]);
        }
    }
    free(node);
}

void clearTree(Node *ptr)
{
    if (!ptr) {
        return;
    }

    for (int i = 0; i < 2 * T; ++i) {
        if (ptr->ptr[i]) {
            clearTree(ptr->ptr[i]);
        }
    }

    clearNode(ptr);
}

Node *createNode()
{
    Node *newIt = (Node *)calloc(1, sizeof(Node));
    return newIt;
}

void simpleDeleteKey(Node *x, int idx)
{
    if (idx == 0) {
        free(x->key[0]);
        free(x->info[0]);
        x->key[0] = x->key[1];
        x->info[0] = x->info[1];
        x->key[1] = x->key[2];
        x->info[1] = x->info[2];
        x->key[2] = NULL;
        x->info[2] = NULL;
    }
    else if (idx == 1) {
        free(x->key[1]);
        free(x->info[1]);
        x->key[1] = x->key[2];
        x->info[1] = x->info[2];
        x->key[2] = NULL;
        x->info[2] = NULL;
    }
    else if (idx == 2) {
        free(x->key[2]);
        free(x->info[2]);
        x->key[2] = NULL;
        x->info[2] = NULL;
    }
    x->n--;
}

void fixNode(Node *ptr)
{
    Node *parent = ptr->parent;
    int idx;
    for (idx = 0; idx < 2 * T; ++idx) {
        if (parent->ptr[idx] == ptr) {
            break;
        }
    }

}

int deleteKey(BTree *tree, char *key)
{
    Node *x = tree->root;
    while (x->ptr[0] != NULL) {
        int idx;
        for (idx = 0; idx < x->n && x->key[idx] && strcmp(key, x->key[idx]) > 0; ++idx);

        if (x->key[idx] != NULL && strcmp(key, x->key[idx]) == 0) {
            simpleDeleteKey(x, idx);
            if (x->n == 0) {
                fixNode(x);
            }
        }

        Node *s = x->ptr[idx];
        if (x->key[idx] != NULL && strcmp(key, x->key[idx]) > 0) {
            s = x->ptr[idx + 1];
        }
        x = s;
    }

    int idx;
    for (idx = 0; idx < x->n && x->key[idx] && strcmp(key, x->key[idx]) > 0; ++idx);

    if (x->key[idx] != NULL && strcmp(key, x->key[idx]) == 0) {
        simpleDeleteKey(x, idx);
        if (x->n == 0) {
            fixNode(x);
        }
    }

    return 0;
}

int findInsertPosition(Node *ptr, char *key)
{
    int i = -1;
    for (i = 0; i < ptr->n && ptr->key[i] && strcmp(key, ptr->key[i]) > 0; ++i);
    return i;
}

/*
void splitNode(Node *x, int splitIdx)
{
    if (x->n == 2 * T - 1) {
        return;  // неверный сплит
    }

    for (int i = x->n - 1; i >= splitIdx; --i) {
        x->key[i + 1] = x->key[i];
        x->info[i + 1] = x->info[i];
        x->ptr[i + 2] = x->ptr[i + 1];
    }

    int t = (2 * T - 1) / 2;


    int new_yn = y->n;
    for (int i = t + 1; i < y->n; ++i) {
        if (y->key[i]) {
            z->n++;
            new_yn--;
        }

        z->key[i - (t + 1)] = y->key[i];
        y->key[i] = NULL;

        z->info[i - (t + 1)] = y->info[i];
        y->info[i] = NULL;

        z->ptr[i - t] = y->ptr[i + 1];
        y->ptr[i + 1] = NULL;
    }
    y->n = new_yn - 1;

    y->parent = x;
    z->parent = x;
    x->ptr[splitIdx + 1] = z;


    x->key[splitIdx] = y->key[t];;
    x->info[splitIdx] = y->info[t];

    y->key[t] = NULL;
    y->info[t] = NULL;

    x->n++;

}
*/

void splitNode(Node *x, int splitIdx)
{
    if (x->n == 2 * T - 1) {
        return;  // неверный сплит
    }

    Node *z = createNode();
    Node *y = x->ptr[splitIdx];

    for (int i = x->n - 1; i >= splitIdx; --i) {
        x->key[i + 1] = x->key[i];
        x->info[i + 1] = x->info[i];
        x->ptr[i + 2] = x->ptr[i + 1];
    }

    z->n = 1;
    y->n = 1;

    z->key[0] = y->key[2];
    z->info[0] = y->info[2];
    z->ptr[0] = y->ptr[2];
    z->ptr[1] = y->ptr[3];


    y->parent = x;
    z->parent = x;
    x->ptr[splitIdx + 1] = z;

    x->key[splitIdx] = y->key[1];
    x->info[splitIdx] = y->info[1];
    x->n++;

    y->key[1] = NULL;
    y->key[2] = NULL;
    y->info[1] = NULL;
    y->info[2] = NULL;

    y->ptr[2] = NULL;
    y->ptr[3] = NULL;
}


int addNode(BTree *tree, char *key, char *info)
{
    if (tree->root == NULL) {
        tree->root = createNode();
        tree->root->n = 1;
        tree->root->key[0] = key;
        tree->root->info[0] = info;
        return 0;
    }

    if (tree->root->n == 2 * T - 1) {
        Node *s = createNode();
        s->ptr[0] = tree->root;
        tree->root = s;
        splitNode(s, 0);
    }

    Node *x = tree->root;
    while (x->ptr[0] != NULL) {
        int idx;
        for (idx = 0; idx < x->n && x->key[idx] && strcmp(key, x->key[idx]) > 0; ++idx);

        if (x->key[idx] != NULL && strcmp(key, x->key[idx]) == 0) {
            return 1;  // дублирующийся ключ
        }

        Node *s = x->ptr[idx];
        if (s->n == 2 * T - 1) {
            splitNode(x, idx);
            if (x->key[idx] != NULL && strcmp(key, x->key[idx]) > 0) {
                s = x->ptr[idx + 1];
            }
        }
        x = s;
    }

    int idx;
    for (idx = 0; idx < x->n && x->key[idx] && strcmp(key, x->key[idx]) > 0; ++idx);

    if (x->key[idx] != NULL && strcmp(key, x->key[idx]) == 0) {
        return 1;  // дублирование ключа
    }

    for (int i = x->n - 1; i >= idx; --i) {
        x->key[i + 1] = x->key[i];
        x->info[i + 1] = x->info[i];
    }

    x->key[idx] = key;
    x->info[idx] = info;
    x->n++;

    return 0;
}

Node *findKey(BTree *tree, char *key)
{
    Node *ptr = tree->root;
    while (ptr) {
        int i = -1;
        for (i = 0; i < 2 * T - 1 && ptr->key[i] && strcmp(key, ptr->key[i]) > 0; ++i);

        if (0 <= i && i < 2 * T - 1) {
            int flag = -1;
            if (ptr->key[i] && (flag = strcmp(key, ptr->key[i])) == 0) {
                return ptr;
            }
            else if (flag < 0) {
                ptr = ptr->ptr[i];
            }
            else {
                ptr = ptr->ptr[i + 1];
            }
        }
    }

    return NULL;
}

Node *findMin(BTree *tree)
{
    Node *ptr = tree->root;
    while (ptr) {
        if (ptr->ptr[0]) {
            ptr = ptr->ptr[0];
        }
        else {
            return ptr;
        }
    }

    return ptr;
}

void dfs(Node *ptr)
{
    if (!ptr) {
        return;
    }

    if (ptr->ptr[0]) {
        dfs(ptr->ptr[0]);
    }

    if (ptr->key[0]) {
        printf("%s ", ptr->key[0]);
    }

    if (ptr->ptr[1]) {
        dfs(ptr->ptr[1]);
    }

    if (ptr->key[1]) {
        printf("%s ", ptr->key[1]);
    }

    if (ptr->ptr[2]) {
        dfs(ptr->ptr[2]);
    }

    if (ptr->key[2]) {
        printf("%s ", ptr->key[2]);
    }

    if (ptr->ptr[3]) {
        dfs(ptr->ptr[3]);
    }
}

int roundTree(BTree *tree, char *key)
{
    if (strcmp(key, "-1") == 0) {
        dfs(tree->root);
        return 0;
    }
    else {
        Node *ptr = findKey(tree, key);
        if (ptr) {
            dfs(ptr);
            return 0;
        }
        else {
            return 1;  // ключ не найден
        }
    }
}

void printTree(Node *ptr, int lvl)
{
    if (!ptr) {
        return;
    }

    printTree(ptr->ptr[0], lvl + 1);
    if (ptr->key[0]) {
        for (int i = 0; i < lvl; ++i) printf("\t");
        printf("%s\n", ptr->key[0]);
    }
    printTree(ptr->ptr[1], lvl + 1);
    if (ptr->key[1]) {
        for (int i = 0; i < lvl; ++i) printf("\t");
        printf("%s\n", ptr->key[1]);
    }
    printTree(ptr->ptr[2], lvl + 1);
    if (ptr->key[2]) {
        for (int i = 0; i < lvl; ++i) printf("\t");
        printf("%s\n", ptr->key[2]);
    }
    printTree(ptr->ptr[3], lvl + 1);
}

int showTree(BTree *tree, char *key)
{
    if (strcmp(key, "-1") == 0 && tree->root) {
        printTree(tree->root, 0);
        return 0;
    }
    else {
        Node *ptr = findKey(tree, key);
        if (ptr) {
            printTree(ptr, 0);
            return 0;
        }
        else {
            return 1;  // ключ не найден
        }
    }
}
