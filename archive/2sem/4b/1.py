import time
import random
import subprocess


def generate(N):
    with open('tests.txt', 'w') as file:
        print(N, file=file)
        for i in range(N):
            x = random.randint(0, 10000)
            print(x, file=file)
            print(x, file=file)





def test(finput, foutput):
    tic = time.perf_counter()

    subprocess.call('./main', stdin=finput, stdout=foutput)

    toc = time.perf_counter()
    return round(toc - tic, 15)

inp = open('c_input.txt', 'r')
out = open('out.txt', 'w')

results = []
test_amount = [300, 500, 1000, 3000, 5000, 10000, 20000, 40000]

for amount in test_amount:
    k = 100
    tmp_s = 0
    for i in range(k):
        generate(amount)
        tmp_s += test(inp, out)
    print(amount)
    results.append(tmp_s / k)

print(results)

inp.close()
out.close()

