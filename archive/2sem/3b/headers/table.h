#pragma once
#include <stddef.h>
#include <stdio.h>
#include "list.h"
#include "item.h"
#include "table_struct.h"

int createTable(Table *table, size_t msize1, size_t msize2);
int clearTable(Table *table);
int showTable(FILE *out, Table *table);
List *compositeKeySearch(Table *table, char *key1, char *key2);
int compositeKeyDelete(Table *table, char *key1, char *key2);

int compareItem(Item *f, Item *s);
Item *createItem(Table *table, char *key1, char *key2, char *info);
char *getItem(Table *table, Item *item);
int clearItem(Item *item);
