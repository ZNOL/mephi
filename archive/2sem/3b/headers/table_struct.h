#pragma once
#include "item.h"
#include <stdio.h>

typedef struct KeySpace1 {
    int busy;

    char *key;

    Item *item;
} KeySpace1;

typedef struct KeySpace2 {
    char *key;

    int release;

    Item *item;

    struct KeySpace2 *next;
} KeySpace2;

typedef struct Table {
    char *table_name;
    FILE *data;

    KeySpace1 *ks1;
    KeySpace2 **ks2;

    size_t msize1;
    size_t msize2;

    size_t csize1;
    size_t csize2;
} Table;