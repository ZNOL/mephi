#pragma once
#include <stdio.h>
#include "table.h"

char *makeFName(const char *f, const char *s);

int ks1TableRead(Table *table, FILE *ks1_d);
int ks2TableRead(Table *table, FILE *ks2_d);
int fileCreateTable(Table *table, char *fname);

int ks1TableWrite(Table *table, FILE *ks1_d);
int ks2TableWrite(Table *table, FILE *ks2_d);
int fileWriteTable(Table *table);
