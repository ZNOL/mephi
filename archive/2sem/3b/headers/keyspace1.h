#pragma once
#include "table.h"

int keySpace1GarbageCollector(Table *table);
int keySpace1Search(Table *table, char *key);
List *keySpace1ListSearch(Table *table, char *key);
Item *keySpace1Item(Table *table, Item *tmp);
int keySpace1Insert(Table *table, char *key, Item *item);
int keySpace1Delete(Table *table, char *key, Item *item);
