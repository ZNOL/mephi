#pragma once
#include "../headers/table.h"

int dialog(const char *msgs[], int NMsgs);
int D_createTable(Table *table);
int D_Add(Table *table);
int D_Show(Table *table);
int D_Find(Table *table);
int D_Delete(Table *table);