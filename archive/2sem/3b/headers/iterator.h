#pragma once
#include "list.h"

typedef struct Iterator {
    ListItem *ptr;
} Iterator;

Iterator begin(List *list);
Item *value(Iterator it);
Iterator next(Iterator it);
