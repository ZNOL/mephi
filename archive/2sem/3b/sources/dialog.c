#include <stdlib.h>
#include <string.h>
#include "../headers/io.h"
#include "../headers/table.h"
#include "../headers/list.h"
#include "../headers/keyspace1.h"
#include "../headers/keyspace2.h"
#include "../headers/file_table.h"

int dialog(const char *msgs[], int N)
{
    puts("");
    for (size_t i = 0; i < N; ++i) {
        puts(msgs[i]);
    }
    puts("");

    char *errmsg = "";
    int rc;
    do {
        puts(errmsg);
        errmsg = "Неверный номер пункта меня. Повторите ввод";
        if (!getInt(&rc)) {
            rc = 0;
        }
    } while (rc < 0 || rc >= N);

    return rc;
}

int D_createTable(Table *table)
{
    int msize1 = -1, msize2 = -1;
    table->ks1 = NULL;
    table->ks2 = NULL;
    table->data = NULL;

    char *fname = readline_(stdin, "Введите название файла таблицы: ");
    if (!fname) {
        fname = (char *)calloc(4, sizeof(char));
        memcpy(fname, "tab", 3);
    }
    if (!fileCreateTable(table, fname)) {
        return 0;
    }

    char *data_fname = makeFName(fname, "_data.tab");
    FILE *data_d = fopen(data_fname, "w+b");
    free(data_fname);

    table->table_name = fname;
    if (table->data) {
        fclose(table->data);
    }
    table->data = data_d;

    while (msize1 <= 0) {
        puts("Введите размер просматриваемой таблицы");
        if (!getInt(&msize1)) {
            puts("Ошибка при вводе размера таблицы");
            return 1;
        }
    }

    while (msize2 <= 0) {
        puts("Введите размер перемешанной таблицы");
        if (!getInt(&msize2)) {
            puts("Ошибка при вводе размера таблицы");
            return 1;
        }
    }

    if (createTable(table, (size_t)msize1, (size_t)msize2)) {
        return 1;
    }

    return 0;
}

int D_Add(Table *table)
{
    char *key1 = readline_(stdin, "Введите 1-ый ключ: ");
    char *key2 = readline_(stdin, "Введите 2-ой ключ: ");
    char *info = readline_(stdin, "Введите информацию: ");

    Item *tmp = createItem(table, key1, key2, info);
    free(info);
    if (!tmp) {
        free(key1);
        free(key2);
        free(info);
        return -1;  // ошибка с выделением памяти
    }

    const char *errors[] = {
        "",
        "Дублирование ключей в пространстве",
        "Переполнение пространства",
    };

    int flag1 = keySpace1Insert(table, key1, tmp);
    if (flag1) {
        puts(errors[flag1]);
        clearItem(tmp);
        return 0;
    }

    int flag2 = keySpace2Insert(table, key2, tmp);
    if (flag2) {
        clearItem(tmp);
        return 0;
    }

    return 0;
}

int D_Find(Table *table)
{
    const char *msgs[] = {
        "0. Отмена",
        "1. Поиск по составному",
        "2. Поиск по ключу из 1ого",
        "3. Поиск по ключу из 2ого",
        "4. Поиск по ключу и версии из 2ого",
    };
    const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

    int rc, release;
    char *key, *key1, *key2;
    List *list;
    while ((rc = dialog(msgs, NMsgs))) {
        switch (rc)
        {
        case 0:
            return 0;
            break;
        case 1:
            key1 = readline_(stdin, "Введите ключ для 1ого пространства: ");
            key2 = readline_(stdin, "Введите ключ для 2ого пространства: ");
            list = compositeKeySearch(table, key1, key2);
            printList(table, list);
            clearList(list);
            free(key1);
            free(key2);
            break;
        case 2:
            key = readline_(stdin, "Введите ключ: ");
            list = keySpace1ListSearch(table, key);
            printList(table, list);
            clearList(list);
            free(key);
            break;
        case 3:
            key = readline_(stdin, "Введите ключ: ");
            list = keySpace2Search(table, key, NULL, -1);
            printList(table, list);
            clearList(list);
            free(key);
            break;
        case 4:
            key = readline_(stdin, "Введите ключ: ");
            printf("Введите номер версии: ");
            getInt(&release);
            list = keySpace2Search(table, key, NULL, release);
            printList(table, list);
            clearList(list);
            free(key);
            break;
        }
    }
    return 0;
}

int D_Delete(Table *table)
{
    const char *msgs[] = {
        "0. Отмена",
        "1. Удаление по составному ключу",
        "2. Удаление по ключу из 1ой таблицы",
        "3. Удаление по дапазону ключей из 1ой таблицы",
        "4. Удаление по ключу из 2ой таблицы",
        "5. Удаление по ключу и версии из 2ой таблицы",
    };
    const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

    int rc, release;
    char *key, *key1, *key2;
    while ((rc = dialog(msgs, NMsgs))) {
        switch (rc)
        {
        case 0:
            return 0;
            break;
        case 1:
            key1 = readline_(stdin, "Введите ключ для 1ого пространства: ");
            key2 = readline_(stdin, "Введите ключ для 2ого пространства: ");
            if (compositeKeyDelete(table, key1, key2)) {
                puts("Информация по составному ключу не найдена");
            }
            free(key1);
            free(key2);
            break;
        case 2:
            key = readline_(stdin, "Введите ключ: ");
            keySpace1Delete(table, key, NULL);
            free(key);
            break;
        case 3:
            int x = -1;
            puts("Введи количество ключей");
            getInt(&x);
            for (int i = 0; i < x; ++i) {
                key = readline_(stdin, "Введите ключ: ");
                keySpace1Delete(table, key, NULL);
                free(key);
            }
            break;
        case 4:
            key = readline_(stdin, "Введите ключ: ");
            keySpace2Delete(table, key, NULL, -1);
            free(key);
            break;
        case 5:
            key = readline_(stdin, "Введите ключ: ");
            printf("Введите номер версии: ");
            getInt(&release);
            keySpace2Delete(table, key, NULL, release);
            free(key);
            break;
        }
    }
    return 0;
}

int D_Show(Table *table)
{
    int x;
    puts("Введите 0, если выводить в стандартный поток\n1, если в файл");
    getInt(&x);

    if (x == 0) {
        showTable(stdout, table);
    }
    else {
        char *name = readline_(stdin, "Введите файл: ");
        FILE *file = fopen(name, "w");
        free(name);

        if (!file) {
            puts("Ошибка при открытии файла");
            return 0;
        }

        showTable(file, table);
        fclose(file);
    }

    return 0;
}
