#include <stdio.h>
#include "../headers/table.h"
#include "../headers/dialog.h"
#include "../headers/keyspace1.h"
#include "../headers/file_table.h"

int main()
{
    Table table;
    if (D_createTable(&table)) {
        puts("Ошибка при создании таблицы.");
        clearTable(&table);
        return 1;
    }

    const char *msgs[] = {
        "0. Выход из программы",
        "1. Добавление",
        "2. Поиск",
        "3. Удаление",
        "4. Вывод",
        "5. Реорганизация просматриваемой таблицы"
    };
    const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

    int (*fptr[])(Table *) = {
        NULL,
        D_Add,
        D_Find,
        D_Delete,
        D_Show,
        keySpace1GarbageCollector
    };

    int rc;
    while ((rc = dialog(msgs, NMsgs))) {
        if (fptr[rc](&table)) {
            break;
        }
    }

    fileWriteTable(&table);
    clearTable(&table);

    return 0;
}