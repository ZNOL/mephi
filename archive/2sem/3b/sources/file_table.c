#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/io.h"
#include "../headers/table.h"
#include "../headers/item.h"
#include "../headers/keyspace1.h"
#include "../headers/keyspace2.h"

char *makeFName(const char *f, const char *s)
{
    char *str = (char *)calloc(strlen(f) + strlen(s) + 1, sizeof(char));
    str = strcat(str, f);
    str = strcat(str, s);
    return str;
}

int ks1TableRead(Table *table, FILE *ks1_d)
{
    fseek(ks1_d, 0L, SEEK_SET);
    fread(&table->msize1, sizeof(int), 1, ks1_d);
    fread(&table->csize1, sizeof(int), 1, ks1_d);
    table->ks1 = (KeySpace1 *)calloc(table->msize1, sizeof(KeySpace1));
    for (int i = 0; i < table->msize1; ++i) {
        int tmp_busy = 0, tmp_len = 0;
        fread(&tmp_busy, sizeof(int), 1, ks1_d);
        char *key = NULL;
        Item *new = NULL;
        if (tmp_busy) {
            fread(&tmp_len, sizeof(int), 1, ks1_d);
            key = (char *)malloc(sizeof(char) * tmp_len);
            fread(key, sizeof(char), tmp_len, ks1_d);

            new = (Item *)malloc(sizeof(Item));
            fread(new, sizeof(Item), 1, ks1_d);
            new->key1 = key;
            new->key2 = NULL;
        }

        (table->ks1)[i] = (KeySpace1){.busy = tmp_busy, .key = key, .item = new};
    }

    return 0;
}

int ks2TableRead(Table *table, FILE *ks2_d)
{
    fseek(ks2_d, 0L, SEEK_SET);
    fread(&table->msize2, sizeof(int), 1, ks2_d);
    fread(&table->csize2, sizeof(int), 1, ks2_d);
    table->ks2 = (KeySpace2 **)calloc(table->msize2, sizeof(KeySpace2 *));
    for (int i = 0; i < table->csize2; ++i) {
        int release, tmp_len = 0;
        fread(&release, sizeof(int), 1, ks2_d);

        fread(&tmp_len, sizeof(int), 1, ks2_d);
        char *key = (char *)malloc(sizeof(char) * tmp_len);
        fread(key, sizeof(char), tmp_len, ks2_d);

        Item *new = NULL;
        Item *tmp_item = (Item *)malloc(sizeof(Item));
        fread(tmp_item, sizeof(Item), 1, ks2_d);

        new = keySpace1Item(table, tmp_item);
        new->key2 = key;

        free(tmp_item);

        int idx = hash(key, table->msize2);
        KeySpace2 *tmp = keySpace2Create(key, release, new);
        tmp->next = (table->ks2)[idx];
        (table->ks2)[idx] = tmp;
    }

    return 0;
}

int fileCreateTable(Table *table, char *fname)
{
    char *ks1_fname = makeFName(fname, "_ks1.tab");    //  9
    char *ks2_fname = makeFName(fname, "_ks2.tab");    //  9
    char *data_fname = makeFName(fname, "_data.tab");  // 10

    FILE *ks1_d = fopen(ks1_fname, "r+b");
    FILE *ks2_d = fopen(ks2_fname, "r+b");
    FILE *data_d = fopen(data_fname, "r+b");

    table->table_name = fname;
    if (data_d) {
        table->data = data_d;
    }
    else {
        table->data = NULL;
    }
    table->msize1 = 0;
    table->csize1 = 0;
    table->msize2 = 0;
    table->csize2 = 0;

    free(ks1_fname);
    free(ks2_fname);
    free(data_fname);
    if (!ks1_d || !ks2_d || !data_d){
        return 1;
    }

    ks1TableRead(table, ks1_d);

    ks2TableRead(table, ks2_d);

    fclose(ks1_d);
    fclose(ks2_d);

    return 0;
}

int ks1TableWrite(Table *table, FILE *ks1_d)
{
    fwrite(&table->msize1, sizeof(int), 1, ks1_d);
    fwrite(&table->csize1, sizeof(int), 1, ks1_d);
    for (int i = 0; i < table->msize1; ++i) {
        fwrite(&(table->ks1)[i].busy, sizeof(int), 1, ks1_d);
        if ((table->ks1)[i].busy) {
            int tmp_len = strlen((table->ks1)[i].key) + 1;
            fwrite(&tmp_len, sizeof(int), 1, ks1_d);
            fwrite((table->ks1)[i].key, sizeof(char), tmp_len, ks1_d);
            fwrite((table->ks1)[i].item, sizeof(Item), 1, ks1_d);
        }
    }

    return 0;
}

int ks2TableWrite(Table *table, FILE *ks2_d)
{
    fwrite(&table->msize2, sizeof(int), 1, ks2_d);
    fwrite(&table->csize2, sizeof(int), 1, ks2_d);
    for (int i = 0; i < table->msize2; ++i) {
        KeySpace2 *ptr = (table->ks2)[i];
        while (ptr) {
            fwrite(&ptr->release, sizeof(int), 1, ks2_d);
            int tmp_len = strlen(ptr->key) + 1;
            fwrite(&tmp_len, sizeof(int), 1, ks2_d);
            fwrite(ptr->key, sizeof(char), tmp_len, ks2_d);
            fwrite(ptr->item, sizeof(Item), 1, ks2_d);

            ptr = ptr->next;
        }
    }

    return 0;
}

int fileWriteTable(Table *table)
{
    char *ks1_fname = makeFName(table->table_name, "_ks1.tab");
    char *ks2_fname = makeFName(table->table_name, "_ks2.tab");

    FILE *ks1_d = fopen(ks1_fname, "wb");
    FILE *ks2_d = fopen(ks2_fname, "wb");
    free(ks1_fname);
    free(ks2_fname);

    ks1TableWrite(table, ks1_d);
    ks2TableWrite(table, ks2_d);

    fclose(ks1_d);
    fclose(ks2_d);

    return 0;
}
