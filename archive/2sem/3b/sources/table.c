#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/list.h"
#include "../headers/table.h"
#include "../headers/keyspace1.h"
#include "../headers/keyspace2.h"

int createTable(Table *table, size_t msize1, size_t msize2)
{
    table->msize1 = msize1;
    table->msize2 = msize2;
    table->csize1 = 0;
    table->csize2 = 0;

    table->ks1 = (KeySpace1 *)calloc(msize1, sizeof(KeySpace1));
    table->ks2 = (KeySpace2 **)calloc(msize2, sizeof(KeySpace2 *));
    if (!(table->ks1) || !(table->ks2)) {
        return 1;
    }

    return 0;
}

int clearTable(Table *table)
{
    for (int i = 0; i < (table->msize1); ++i) {
        if ((table->ks1)[i].busy) {
            clearItem((table->ks1)[i].item);
        }
    }

    if (table->ks1) {
        free(table->ks1);
    }

    for (int i = 0; i < table->msize2; ++i) {
        while ((table->ks2)[i]) {
            KeySpace2 *tmp = (table->ks2)[i];
            (table->ks2)[i] = (table->ks2)[i]->next;
            free(tmp);
        }
    }

    if (table->ks2) {
        free(table->ks2);
    }

    if (table->table_name) {
        free(table->table_name);
    }

    if (table->data) {
        fclose(table->data);
    }

    table->csize1 = 0;
    table->msize1 = 0;
    table->csize2 = 0;
    table->msize2 = 0;

    return 0;
}

int showTable(FILE *out, Table *table)
{
    fprintf(stdout, "Заполненность 1-ого пространства %ld/%ld\n", table->csize1, table->msize1);

    fputs("--------------------\n", out);
    for (int i = 0; i < table->msize1; ++i) {
        if ((table->ks1)[i].busy) {
            fprintf(out, "idx = %d | busy = %d\n", i, (table->ks1)[i].busy);
            char *item = getItem(table, (table->ks1)[i].item);
            fprintf(out, "key = %s\ninfo = %s\n", (table->ks1)[i].key, item);
            free(item);
            fputs("--------------------\n", out);
        }
    }

    fprintf(out, "Заполененность 2-ого пространства %ld\n", table->csize2);
    for (int i = 0; i < table->msize2; ++i) {
        KeySpace2 *tmp = (table->ks2)[i];
        while (tmp) {
            fputs("--------------------\n", out);
            char *item = getItem(table, tmp->item);
            fprintf(out, "key = %s\nrelease = %d\ninfo = %s\n", tmp->key, tmp->release, item);
            free(item);
            fputs("--------------------\n", out);
            tmp = tmp->next;
        }
    }

    return 0;
}

List *compositeKeySearch(Table *table, char *key1, char *key2)
{
    List *list = createList();

    List *tmp = keySpace2Search(table, key2, NULL, -1);
    if (tmp->amount == 0) {
        return list;  // ключ2 не найден
    }
    ListItem *ptr = tmp->head;
    while (ptr) {
        if (!strcmp(ptr->item->key1, key1)) {
            pushList(list, ptr->item);
        }
        ptr = ptr->next;
    }
    clearList(tmp);

    return list;
}

int compositeKeyDelete(Table *table, char *key1, char *key2)
{
    int idx = keySpace1Search(table, key1);
    if (idx == -1) {
        return -1;  // ключ1 не найден
    }

    if (strcmp((table->ks1)[idx].key, key2)) {
        return -2;  // ключ2 не найден
    }

    keySpace1Delete(table, key1, NULL);

    return 0;
}

char *getItem(Table *table, Item *item)
{
    char *res = (char *)calloc(item->len, sizeof(char));
    fseek(table->data, item->offset, SEEK_SET);
    fread(res, sizeof(char), item->len, table->data);
    // res[item->len - 1] = '\0';z
    return res;
}

int compareItem(Item *f, Item *s)
{
    if (f->len == s->len && f->offset == s->offset) {
        return 1;
    }
    else {
        return 0;
    }
}

Item *createItem(Table *table, char *key1, char *key2, char *info)
{
    Item *new = (Item *)calloc(1, sizeof(Item));

    new->key1 = key1;
    new->key2 = key2;

    fseek(table->data, 0L, SEEK_END);
    new->len = strlen(info) + 1;
    new->offset = ftell(table->data);
    fwrite(info, sizeof(char), new->len, table->data);

    return new;
}

int clearItem(Item *item)
{
    free(item->key1);
    free(item->key2);
    free(item);

    return 0;
}
