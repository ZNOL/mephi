#pragma once

#define ull unsigned long long int

typedef struct Node {
    unsigned long long int key;

    char *info;

    struct Node *parent;

    struct Node *left;
    struct Node *right;
} Node;

// добавить дерево

void clearTree(Node **root);

int addTree(Node **root, ull key, char *info);

Node *findKey(Node **root, ull key);
Node *getMinTree(Node **root);

int deleteKey(Node **root, ull key);

int showTree(Node **root, long long int key);

int roundTree(Node **root, long long int key);
