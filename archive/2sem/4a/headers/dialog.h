#pragma once
#include "bintree.h"

int dialog(const char *msgs[], int NMsgs);

int D_Add(Node **root);
int D_Delete(Node ** root);
int D_Show(Node **root);
int D_Round(Node **root);
int D_Find(Node **root);
int D_MinFind(Node **root);
int D_FileInput(Node **root);
