#include "../headers/bintree.h"
#include <stdio.h>
#include <stdlib.h>

void dfs2(Node *ptr, FILE *file)
{
    if (!ptr) {
        return;
    }

    if (ptr->left) {
        fprintf(file, "%lld -- %lld\n", ptr->key, ptr->left->key);
        dfs2(ptr->left, file);
    }

    if (ptr->right) {
        fprintf(file, "%lld -- %lld\n", ptr->key, ptr->right->key);
        dfs2(ptr->right, file);
    }
}

int printGraphViz(Node **root)
{
    if (*root == NULL) {
        return 1;
    }

    FILE *file = fopen("graph.gv","w");
    fprintf(file, "graph G {\n");

    dfs2(*root, file);
    fprintf(file, "}");
    fclose(file);

    system("dot -Tpng graph.gv -o file.png");
    return 0;
}