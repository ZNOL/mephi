#include <stdio.h>
#include "../headers/bintree.h"
#include "../headers/dialog.h"
#include "../headers/graphviz.h"

int main()
{
    Node *root = NULL;

    const char *msgs[] = {
        "0. Выход из программы",
        "1. Добавление",
        "2. Удаление",
        "3. Вывод дерева",
        "4. Обход дерева",
        "5. Поиск по ключу",
        "6. Поиск минимума",
        "7. Загрузка из файла",
        "8. Graphviz"
    };
    const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

    int (*fptr[])(Node **) = {
        NULL,
        D_Add,
        D_Delete,
        D_Show,
        D_Round,
        D_Find,
        D_MinFind,
        D_FileInput,
        printGraphViz,
    };

    int rc;
    while ((rc = dialog(msgs, NMsgs))) {
        if (fptr[rc](&root)) {
            break;
        }
    }

    clearTree(&root);

    return 0;
}
