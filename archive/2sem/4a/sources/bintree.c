#include <stdio.h>
#include <stdlib.h>
#include "../headers/bintree.h"

#define ull unsigned long long int

void clearNode(Node *ptr)
{
    free(ptr->info);
    free(ptr);
}

void  clearTree(Node **root)
{
    Node *ptr = *root;
    while (ptr) {
        if (ptr->left) {
            ptr = ptr->left;
        }
        else if (ptr->right) {
            ptr = ptr->right;
        }
        else {
            Node *tmp = ptr;
            ptr = ptr->parent;
            if (ptr) {
                if (tmp == ptr->right) {
                    ptr->right = NULL;
                }
                else {
                    ptr->left = NULL;
                }
            }

            clearNode(tmp);
        }
    }

    return;
}

int addTree(Node **root, unsigned long long int key, char *info)
{
    Node *new = (Node *)calloc(1, sizeof(Node));
    new->key = key;
    new->info = info;

    if (*root == NULL) {
        *root = new;
        return 0;
    }

    Node *parent = NULL;
    Node *ptr = *root;
    while (ptr) {
        parent = ptr;
        if (key < ptr->key) {
            ptr = ptr->left;
        }
        else {
            ptr = ptr->right;
        }
    }

    new->parent = parent;
    if (key < parent->key) {
        parent->left = new;
    }
    else {
        parent->right = new;
    }

    return 0;
}

Node *findKey(Node **root, ull key)
{
    Node *ptr = *root;
    while (ptr) {
        if (key == ptr->key) {
            return ptr;
        }
        else {
            if (key < ptr->key) {
                ptr = ptr->left;
            }
            else {
                ptr = ptr->right;
            }
        }
    }

    return NULL;
}

Node *getMinTree(Node **root)
{
    Node *ptr = *root;
    while (ptr) {
        if (ptr->left) {
            ptr = ptr->left;
        }
        else {
            return ptr;
        }
    }

    return ptr;
}

void removeChild(Node *child)
{
    if (!child) {
        return;
    }

    Node *parent = child->parent;
    if (parent) {
        if (parent->left == child) {
            parent->left = NULL;
        }
        else {
            parent->right = NULL;
        }
    }
}

int deleteKey(Node **root, ull key)
{
    Node *y = findKey(root, key);
    if (!y) {
        return 1; // ключ не найден
    }
    Node *parent = y->parent;

    if (y->left == NULL || y->right == NULL) {
        Node *new = NULL;
        if (y->left == NULL) {
            new = y->right;
        }
        else {
            new = y->left;
        }

        removeChild(new);

        if (parent) {
            if (y == parent->right) {
                parent->right = new;
            }
            else {
                parent->left = new;
            }
        }
        else {
            *root = new;
        }

        if (new) {
            new->parent = parent;
        }

    }
    else {
        Node *tmp = NULL;
        if (y->left) {
            tmp = y->left;
            while (tmp) {
                if (tmp->right) {
                    tmp = tmp->right;
                }
                else if (tmp->left) {
                    tmp = tmp->left;
                }
                else {
                    break;
                }
            }
        }
        else {
            tmp = y->right;
            while (tmp) {
                if (tmp->left) {
                    tmp = tmp->left;
                }
                else if (tmp->right) {
                    tmp = tmp->right;
                }
                else {
                    break;
                }
            }
        }

        removeChild(tmp);

        if (parent == NULL) {
            *root = tmp;
        }
        else {
            if (y == parent->left) {
                parent->left = tmp;
            }
            else {
                parent->right = tmp;
            }
        }
// когда забираешь нужно удалить ребенка в родителя
        if (tmp) {
            tmp->parent = parent;
            if (y->left != tmp) {
                tmp->left = y->left;
            }
            if (y->right != tmp) {
                tmp->right = y->right;
            }

            if (y->left && y->left != tmp) {
                y->left->parent = tmp;
            }
            if (y->right && y->right != tmp) {
                y->right->parent = tmp;
            }
        }

    }

    clearNode(y);
    return 0;
}

void dfs(Node *ptr)
{
    if (!ptr) {
        return;
    }
    // добавить вывод в файл
    if (ptr->right) {
        dfs(ptr->right);
    }

    printf("%lld ", ptr->key);

    if (ptr->left) {
        dfs(ptr->left);
    }
}

int roundTree(Node **root, long long int key)
{
    if (key == -1) {
        dfs(*root);
        return 0;
    }
    else {
        Node *ptr = findKey(root, (ull)key);
        if (ptr) {
            dfs(ptr);
            return 0;
        }
        else {
            return 1; // ключ не найден
        }
    }
}

void printTree(Node *p, int lvl)
{
    if (p) {
        printTree(p->left, lvl + 1);
        for (int i = 0; i < lvl; ++i) printf("\t\t");
        printf("%lld\n", p->key);
        printTree(p->right, lvl + 1);
    }
}

int showTree(Node **root, long long int key)
{
    if (key == -1 && root) {
        printTree(*root, 0);
        return 0;
    }
    else {
        Node *ptr = findKey(root, (ull)key);
        if (ptr) {
            printTree(ptr, 0);
            return 0;
        }
        else {
            return 1; // ключ не найден
        }
    }
}
