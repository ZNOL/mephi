#include <stdlib.h>
#include "../headers/io.h"
#include "../headers/bintree.h"

// должны вернуть 0, если всё хорошо

#define ull unsigned long long int

int dialog(const char *msgs[], int N)
{
    puts("");
    for (size_t i = 0; i < N; ++i) {
        puts(msgs[i]);
    }
    puts("");

    char *errmsg = "";
    long long int rc;
    do {
        puts(errmsg);
        errmsg = "Неверный номер пункта меню. Повторите ввод";
        if (!getInt(stdin, &rc)) {
            rc = 0;
        }
    } while (rc < 0 || rc >= N);

    return rc;
}


int D_Add(Node **root)
{
    long long int amount = 1;
    printf("Введите количество вершин для добавления: ");
    getInt(stdin, &amount);

    while (amount-- > 0) {
        long long int key = -1;
        printf("Ключ: ");
        getInt(stdin, &key);

        char *info = readline_(stdin, "Поле info: ");

        addTree(root, key, info);
    }
    return 0;
}

int D_Delete(Node ** root)
{
    long long int key = -1;
    printf("Введите ключ для удаления: ");
    getInt(stdin, &key);

    deleteKey(root, (ull)key);

    return 0;
}

int D_Show(Node **root)
{
    long long int key = -1;
    printf("Введите начальный ключ для вывода дерева или -1 для вывода c корня: ");
    getInt(stdin, &key);

    showTree(root, key);

    return 0;
}

int D_Round(Node **root)
{
    long long int key = -1;
    printf("Введите начальный ключ для обхода дерева или -1 для вывода c корня: ");
    getInt(stdin, &key);

    roundTree(root, key);

    return 0;
}

int D_Find(Node **root)
{
    long long int key = -1;
    printf("Введите ключ для поиска: ");
    getInt(stdin, &key);

    Node *ptr = findKey(root, (ull)key);
    if (ptr) {
        printf("key = %lld | info = %s\n", ptr->key, ptr->info);
    }
    else {
        printf("Ключ не найден\n");
    }

    return 0;
}

int D_MinFind(Node **root)
{
    Node *ptr = getMinTree(root);
    if (ptr) {
        printf("key = %lld | info = %s\n", ptr->key, ptr->info);
    }
    else {
        printf("Дерево пустое\n");
    }

    return 0;
}

int D_FileInput(Node **root)
{
    char *filename = readline_(stdin, "Введите название файла: ");

    FILE *file = fopen(filename, "r");
    if (!file) {
        printf("Неверное название файла\n");
        free(filename);
        return 0;
    }

    long long amount = -1;
    getInt(file, &amount);
    while (amount-- > 0) {
        long long int key = -1;
        if (!getInt(file, &key)) {
            break;
        }
        char *info = readline_(file, "");

        addTree(root, key, info);
    }

    free(filename);
    fclose(file);

    return 0;
}