#include <stdlib.h>
#include "../../headers/list.h"

List *create_list()
{
    return (List *)calloc(1, sizeof(List));
}

void clear_list(List *list)
{
    while (list -> head) {
        Item *tmp = list -> head;
        list -> head = list -> head -> next;
        free(tmp);
    }
    free(list);
}

int push_list(List *list, char data)
{
    if (!data) {
        return 1;
    }
    
    Item *new = (Item *)malloc(sizeof(Item));
    if (!new) {
        return 1;
    }

    new->data = data;
    new->next = list->head;
    list->head = new;
    
    return 0;
}

int pop_list(List *list)
{
    if (!list->head) {
        return 1;
    }

    Item *tmp = list->head;
    list->head = list->head->next;

    free(tmp);

    return 0;
}

char top_list(List *list)
{
    if (!list->head) {
        return '\0';
    }
    else {
        return list->head->data;
    }
}
