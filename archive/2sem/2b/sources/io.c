#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *readline_(FILE *stream, const char *prompt)
{
    if (stream == stdin) printf("%s", prompt);

    char buf[BUFSIZ] = {0};
    char *res = NULL;
    int len = 0;
    int n = 0;

    do {
        n = fscanf(stream, "%8192[^\n]", buf);
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        }
        else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            res = (char *)realloc(res, str_len + 1);
            memcpy(res + len, buf, chunk_len);
            len = str_len;
        }
        else {
            fscanf(stream, "%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    }
    else {
        res = (char *)calloc(1, sizeof(char));
    }

    return res;
}

long long int get_digit(char *prompt)
{
    printf("%s", prompt);
    long long int ans = 0;
    int flag = -1;
    while ((flag = scanf("%lld", &ans)) != 1)
    {
        if (flag == -1) {
            ans = 0;
            printf("\nКонец файла.\n");
            break;
        }
        printf("\nОшибка при считывании числа.\nПовторите ввод.\n");
        scanf("%*[^\n]");
    }
    return ans;
}

void stdin_clear()
{
    scanf("%*[^\n]");
    scanf("%*c");
}
