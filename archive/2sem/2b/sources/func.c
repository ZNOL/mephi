#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../headers/stack.h"

char *solve(char *in)
{
    Stack *stack = create_stack();
    int length = strlen(in), new_length = 0;
    char *out = (char *)calloc(3 * (length + 1), sizeof(char));

    int error = 0;
    int wasDigit = 0;
    for (int i = 0; i < length; ++i) {
        char chr = in[i];
        if (wasDigit && (chr == '+' || chr == '-' || chr == '/' || chr == '*' || chr == '(' || chr == ')')) {
            out[new_length++] = ' ';
            wasDigit = 0;
        }

        if (chr == '+' || chr == '-') {
            char data = top(stack);
            while (data != '\0' && data != '(') {
                out[new_length++] = data;
                pop(stack);
                data = top(stack);
            }         
            error += push(stack, ' ');   
            error += push(stack, chr);
        }
        else if (chr == '*' || chr == '/') {
            char data = top(stack);
            while (data != '\0' && data != '(' && data != '+' && data != '-') {
                out[new_length++] = data;
                pop(stack);
                data = top(stack);
            }
            error += push(stack, ' ');
            error += push(stack, chr);
        }
        else if (chr == '(') {
            push(stack, chr);
        }
        else if (chr == ')') {
            char data = top(stack);
            while (data != '\0' && data != '(') {
                out[new_length++] = data;
                pop(stack);
                data = top(stack);
            } 
            if (data != '(') {
                printf("Неверная введённая формула!\n");
            }
            pop(stack);
        }
        else if (chr != ' ') {
            out[new_length++] = chr;
            wasDigit = 1;
        }

        if (error) {
            printf("Переполнение при записи в стек\n");
            error = 0;
        }
    }
    if (wasDigit) {
        out[new_length++] = ' ';
    }

    char data = top(stack);
    while (data != '\0') {
        if (data == '(') {
            printf("Неверная введённая формула!\n");
        }
        out[new_length++] = data;
        pop(stack);
        data = top(stack);
    }

    clear_stack(stack);

    return out;
}
