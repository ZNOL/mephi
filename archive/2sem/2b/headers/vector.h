#pragma once

typedef struct Vector {
    int top;
    char *seq;
    int maxsize;
} Vector;

Vector *create_vector();
void clear_vector(Vector *vector);
int push_vector(Vector *vector, char data);
int pop_vector(Vector *vector);
char top_vector(Vector *vector);
