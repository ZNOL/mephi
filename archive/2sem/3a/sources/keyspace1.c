#include <stddef.h>
#include "string.h"
#include "../headers/table.h"
#include "../headers/list.h"
#include "../headers/keyspace2.h"

int keySpace1Search(Table *table, char *key)
{
    for (int i = 0; i < table->msize1; ++i) {
        if ((table->ks1)[i].busy && !strcmp(key, (table->ks1)[i].key)) {
            return i;
        }
    }
    return -1;
}

List *keySpace1ListSearch(Table *table, char *key)
{
    List *list = createList();

    int idx;
    if ((idx = keySpace1Search(table, key)) != -1) {
        pushList(list, (table->ks1)[idx].item);
    }

    return list;
}

int keySpace1GarbageCollector(Table *table)
{
    int i = 0, j = 0;
    table->csize1 = 0;
    while (i < table->msize1) {
        if ((table->ks1)[i].busy == 0) {
            ++i;
        }
        else {
            (table->ks1)[j] = (table->ks1)[i];
            if (i != j) {
                (table->ks1)[i].busy = 0;
            }
            ++table->csize1;
            ++j;
            ++i;
        }
    }

    return 0;
}

int keySpace1Insert(Table *table, char *key, Item *item)
{
    if (keySpace1Search(table, key) != -1) {
        return 1;  // дублирование ключей
    }

    if (table->csize1 == table->msize1) {
        keySpace1GarbageCollector(table);
        if (table->csize1 == table->msize1) {
            return 2;  // переполнение
        }
    }

    table->ks1[table->csize1] = (KeySpace1){.busy = 1, .key = key, .item = item};
    table->ks1[table->csize1].item->p1 = &(table->ks1)[table->csize1];
    ++table->csize1;

    return 0;
}

int keySpace1Delete(Table *table, char *key, Item *item)
{
    int idx;
    if ((idx = keySpace1Search(table, key)) == -1) {
        return -1;  // элемент не найден
    }

    KeySpace1 *ptr = &(table->ks1)[idx];
    ptr->busy = 0;
    if (!item) {
        keySpace2Delete(table, ptr->item->key2, ptr->item, -1);
        clearItem(ptr->item);
    }

    return 0;
}
