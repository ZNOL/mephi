#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "../headers/list.h"
#include "../headers/table.h"
#include "../headers/keyspace1.h"

#define max(a, b) ((a > b) ? a : b)

int hash(char *str, int mod)
{
    int hash = INT_MAX;
    for (int i = 0; str[i]; ++i) {
        hash = 37 * hash + str[i];
    }
    return abs(hash) % mod;
}

List *keySpace2Search(Table *table, char *key, Item *item, int release)
{
    int idx = hash(key, table->msize2);

    List *list = createList();

    if (!(table->ks2)[idx]) {
        return list;
    }

    KeySpace2 *tmp = (table->ks2)[idx];
    while (tmp) {
        if ((release == -1 || tmp->release == release) &&
            (item == NULL || item == tmp->item) &&
            !strcmp(key, tmp->key)) {
            pushList(list, tmp->item);
            list->maxRelease = max(list->maxRelease, tmp->release);
        }
        tmp = tmp->next;
    }

    return list;
}

KeySpace2 *keySpace2Create(char *key, int release, Item *item)
{
    KeySpace2 *new = (KeySpace2 *)calloc(1, sizeof(KeySpace2));

    new->key = key;
    new->release = release;
    new->item = item;
    new->next = NULL;

    return new;
}

int keySpace2Insert(Table *table, char *key, Item *item)
{
    int idx = hash(key, table->msize2);
    List *search = keySpace2Search(table, key, NULL, -1);

    KeySpace2 *tmp = keySpace2Create(key, search->maxRelease + 1, item);
    tmp->next = (table->ks2)[idx];
    (table->ks2)[idx] = tmp;
    ++table->csize2;

    clearList(search);

    return 0;
}

int keySpace2SimpleDelete(Table *table, char *key, Item *item, int release, KeySpace2 *ptr)
{
    if (ptr && (release == -1 || release == ptr->release) &&
        (item == NULL || item == ptr->item) &&
        (!strcmp(key, ptr->key))) {

        if (!item) {
            keySpace1Delete(table, ptr->item->key1, ptr->item);
            clearItem(ptr->item);
        }
        --table->csize2;
        return 1;
    }
    return 0;
}

int keySpace2Delete(Table *table, char *key, Item *item, int release)
{
    int idx = hash(key, table->msize2);

    KeySpace2 *ptr = (table->ks2)[idx], *tmp = NULL;

    while (keySpace2SimpleDelete(table, key, item, release, ptr)) {
        (table->ks2)[idx] = ptr->next;
        tmp = ptr;
        ptr = ptr->next;
        free(tmp);
    }

    ptr = (table->ks2)[idx];
    KeySpace2 *prev = ptr;
    tmp = NULL;
    if (ptr) ptr = ptr->next;

    while (ptr) {
        if (keySpace2SimpleDelete(table, key, item, release, ptr)) {
            prev->next = ptr->next;
            tmp = ptr;
        }
        prev = ptr;
        ptr = ptr->next;
        if (tmp) {
            free(tmp);
            tmp = NULL;
        }
    }

    return 0;
}
