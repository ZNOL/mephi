#include <stdio.h>
#include <stdlib.h>
#include "../headers/table.h"
#include "../headers/list.h"
#include "../headers/iterator.h"

ListItem *createListItem(Item *item)
{
    ListItem *new = (ListItem *)calloc(1, sizeof(ListItem));

    new->item = item;

    return new;
}

List *createList()
{
    List *new = (List *)calloc(1, sizeof(List));

    new->amount = 0;
    new->maxRelease = 0;

    new->head = NULL;
    new->tail = NULL;

    return new;
}

int pushList(List *list, Item *item)
{
    ListItem *new = createListItem(item);
    if (!new) {
        return -1;  // Ошибка с выделением памяти
    }

    if (!list->head) {
        list->head = new;
    }

    if (list->tail) {
        list->tail->next = new;
    }
    list->tail = new;
    ++list->amount;

    return 0;
}

void printList(List *list)
{
    printf("%d элемент(/а/ов)\n", list->amount);
    for (Iterator it = begin(list); it.ptr != NULL; it = next(it)) {
        printf("key1 = %s | key2 = %s | info = %s\n", value(it)->key1, value(it)->key2, value(it)->info);
    }
}

void clearList(List *list)
{
    ListItem *ptr = list->head;
    while (ptr) {
        ListItem *tmp = ptr;
        ptr = ptr->next;
        free(tmp);
    }
    free(list);
}
