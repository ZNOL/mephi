#include "../headers/iterator.h"

Iterator begin(List *list)
{
    return (Iterator){.ptr=list->head};
}

Item *value(Iterator it)
{
    return it.ptr->item;
}

Iterator next(Iterator it)
{
    return (Iterator){.ptr=it.ptr->next};
}
