#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void stdinClear()
{
    scanf("%*[^\n]");
    scanf("%*c");
}

char *readline_(FILE *stream, const char *prompt)
{
    if (stream == stdin) printf("%s", prompt);

    char buf[BUFSIZ] = {0};
    char *res = NULL;
    int len = 0;
    int n = 0;

    do {
        n = fscanf(stream, "%8192[^\n]", buf);
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        }
        else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            res = (char *)realloc(res, str_len + 1);
            memcpy(res + len, buf, chunk_len);
            len = str_len;
        }
        else {
            fscanf(stream, "%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    }
    else {
        res = (char *)calloc(1, sizeof(char));
    }

    return res;
}

int getInt(int *ans)
{
    int flag = 0;
    while ((flag = scanf("%d", ans)) != 1) {
        if (flag == -1) {
            return 0;
        }
        printf("Ошибка при считывании числа. Повторите ввод\n");
        scanf("%*[^\n]");
    }
    stdinClear();
    return 1;
}
