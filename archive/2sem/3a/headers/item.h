#pragma once

typedef struct Item {
    char *info;

    char *key1;
    char *key2;

    void *p1;
    void *p2;
} Item;