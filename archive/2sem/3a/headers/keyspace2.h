#pragma once
#include "table.h"
#include "list.h"

int hash(char *str, int mod);
List *keySpace2Search(Table *table, char *key, Item *item, int release);
KeySpace2 *keySpace2Create(char *key, int release, Item *item);
int keySpace2Insert(Table *table, char *key, Item *item);
int keySpace2SimpleDelete(Table *table, char *key, Item *item, int release, KeySpace2 *ptr);
int keySpace2Delete(Table *table, char *key, Item *item, int release);
