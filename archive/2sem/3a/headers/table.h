#pragma once
#include <stddef.h>
#include <stdio.h>
#include "list.h"
#include "item.h"

typedef struct KeySpace1 {
    int busy;

    char *key;

    Item *item;
} KeySpace1;

typedef struct KeySpace2 {
    char *key;

    int release;

    Item *item;

    struct KeySpace2 *next;
} KeySpace2;

typedef struct Table {
    KeySpace1 *ks1;
    KeySpace2 **ks2;

    size_t msize1;
    size_t msize2;

    size_t csize1;
    size_t csize2;
} Table;

int createTable(Table *table, size_t msize1, size_t msize2);
int clearTable(Table *table);
int showTable(FILE *out, Table *table);
List *compositeKeySearch(Table *table, char *key1, char *key2);
int compositeKeyDelete(Table *table, char *key1, char *key2);

Item *createItem(char *key1, char *key2, char *info);
int clearItem(Item *item);
