#pragma once
#include "item.h"

typedef struct ListItem {
    Item *item;

    struct ListItem *next;
} ListItem;

typedef struct List {
    int amount;
    int maxRelease;

    ListItem *head;
    ListItem *tail;
} List;

ListItem *createListItem(Item *item);
List *createList();
int pushList(List *list, Item *item);
void printList(List *list);
void clearList(List *list);
