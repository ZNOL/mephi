#include <stdio.h>
#include "../headers/struct.h"

void matrix_output(Matrix *matrix)
{
    printf("\nКоличество строк в матрице = %d\n", matrix->n);
    for (int i = 0; i < matrix->n; ++i) {
        for (int j = 0; j < matrix->lines[i].m; ++j) {
            printf("%5d ", matrix->lines[i].line[j]);
        }
        printf("\n");
    }
    printf("\n\n");
}
