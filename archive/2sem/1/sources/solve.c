#include <stdio.h>
#include <stdlib.h>
#include "../headers/struct.h"

int *solve(Matrix *matrix)
{
    int *tmp = (int *)calloc(matrix->n, sizeof(int));
    for (int i = 0; i < matrix->n; ++i) {
        for (int j = 0; j < matrix->lines[i].m; ++j) {
            tmp[i] += matrix->lines[i].line[j];
        }
    }

    int *ans = (int *)calloc(matrix->n, sizeof(int));
    for (int i = 0; i < matrix->n; ++i) {
        for (int j = 0; j < matrix->lines[i].m; ++j) {
            if ((double)tmp[i] / matrix->lines[i].m < matrix->lines[i].line[j]) {
                ans[i] += matrix->lines[i].line[j];
            }
        }
    }
    free(tmp);

    return ans;
}
