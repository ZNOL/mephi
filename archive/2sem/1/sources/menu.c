#include <stdlib.h>
#include "../headers/menu.h"
#include "../headers/struct.h"
#include "../headers/input.h"
#include "../headers/output.h"
#include "../headers/solve.h"

void menu()
{
    Matrix *matrix = NULL;

    const char menu_text[][128] = {
        "1. Ввод с консоли", "2. Вывести матрицу в консоль",
        "3. Вывести вектор b", "4. Очистить матрицу",
        "5. Выход из программы", "",
    };

    enum MENU mn = 0;
    while (mn != Exit)
    {
        for (int _ = 0; _ < 6; ++_) {
            printf("%s\n", menu_text[_]);
        }

        mn = get_digit("");
        stdin_clear();
        printf("\n");
        switch (mn)
        {
        case ConsoleRead:
            if (matrix) {
                matrix = matrix_clear(matrix);
            }
            matrix = matrix_input();
            break;
        case ConsoleWrite:
            if (matrix) {
                matrix_output(matrix);
            }
            else {
                printf("Матрица ещё пуста\n");
            }
            break;
        case Solve:
            if (matrix) {
                int *b = solve(matrix);

                printf("Полученный вектор длины %d: ", matrix->n);
                for (int i = 0; i < matrix->n; ++i) {
                    printf("%4d ", b[i]);
                }
                printf("\n\n");

                free(b);
            }
            else {
                printf("Матрица ещё пуста\n");
            }
            break;
        case Clear:
            if (matrix) {
                matrix = matrix_clear(matrix);
            }
            else {
                printf("Матрица ещё пуста\n");
            }
            break;
        case Exit:
            if (matrix) {
                matrix = matrix_clear(matrix);
            }
            printf("Выход из программы\n");
            break;
        default:
            printf("Неверный номер.\n");
            break;
        }
    }    
}