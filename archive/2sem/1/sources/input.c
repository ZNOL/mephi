#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/struct.h"

typedef long long int ll;

char *readline_(FILE *stream, const char *prompt)
{
    if (stream == stdin) printf("%s", prompt);

    char buf[BUFSIZ] = {0};
    char *res = NULL;
    int len = 0;
    int n = 0;

    do {
        n = fscanf(stream, "%8192[^\n]", buf);  
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        }
        else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            res = (char *)realloc(res, str_len + 1);
            memcpy(res + len, buf, chunk_len);
            len = str_len;
        }
        else {
            fscanf(stream, "%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    }
    else {
        res = (char *)calloc(1, sizeof(char));
    }

    return res;
}

ll get_digit(char *prompt)
{
    printf("%s", prompt);
    ll ans = 0;
    int flag = -1;
    while ((flag = scanf("%lld", &ans)) != 1)
    {
        if (flag == -1) {
            ans = 5;
            printf("\nКонец файла.\n");
            break;
        }
        printf("\nОшибка при считывании числа.\nПовторите ввод.\n");
        scanf("%*[^\n]");
    }
    // scanf("%*c");
    return ans;
}

void stdin_clear()
{
    scanf("%*[^\n]");
    scanf("%*c");
}

Matrix *matrix_input()
{
    int n = -1;

    while ((n = get_digit("Введите число строк в матрице: ")) <= 0) {
        printf("Неверное количество. Введите заново.\n");
        stdin_clear();
    }
    stdin_clear();

    Matrix *matrix = matrix_create(n);
    for (int i = 0; i < matrix->n; ++i) {
        int m = -1;
        printf("Введите число элементов в %d строке: ", i + 1);
        while ((m = get_digit("")) < 0) {
            printf("Неверное количество. Введите заново.\n");
            stdin_clear();
        }
        stdin_clear();

        matrix->lines[i].m = m;
        matrix->lines[i].line = (int *)malloc(sizeof(int) * m);

        printf("Введите %d чисел\n", m);
        for (int j = 0; j < m; ++j) {
            matrix->lines[i].line[j] = get_digit("");
        }
        stdin_clear();
    }

    return matrix;
}
