#include <stdlib.h>
#include "../headers/struct.h"

Matrix *matrix_clear(Matrix *matrix)
{
    for (int i = 0; i < matrix->n; ++i) {
        free(((matrix->lines)[i]).line);
    }
    free(matrix->lines);
    free(matrix);

    return NULL;
}

Matrix *matrix_create(int n)
{
    Matrix *new_matrix = (Matrix *)calloc(1, sizeof(Matrix));
    new_matrix->n = n;
    new_matrix->lines = (Line *)malloc(sizeof(Line) * n);
    return new_matrix;    
}
