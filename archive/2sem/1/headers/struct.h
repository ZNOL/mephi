#pragma once

typedef struct Line {
    int m;
    int *line;
} Line;

typedef struct Matrix {
    int n;
    Line *lines;
} Matrix;

Matrix *matrix_create(int n);
Matrix *matrix_clear(Matrix *matrix);
