#pragma once
#include <stdio.h>

typedef long long int ll;

char *readline_(FILE *stream, const char *prompt);
void stdin_clear();
ll get_digit(char *prompt);
Matrix *matrix_input();
