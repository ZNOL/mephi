#pragma once

#include "struct.h"

enum MENU {
    ConsoleRead=1,
    ConsoleWrite,
    Solve,
    Clear,
    Exit,
};

void menu();
