CREATE TYPE "product_document_type" AS ENUM (
    'SALE',
    'REFUND'
    );

CREATE TYPE "payment_order_type" AS ENUM (
    'INCOMING',
    'OUTCOMING'
    );

CREATE TYPE "sex_type" AS ENUM (
    'MALE',
    'FEMALE',
    'OTHER'
    );
