CREATE OR REPLACE PROCEDURE update_parent_product_group(new_parent int, group_ids int[]) AS
$$
DECLARE
    i INTEGER;
BEGIN
    FOREACH i IN ARRAY group_ids
        LOOP
            IF i = new_parent THEN
                RAISE EXCEPTION 'Прямая циклическая ссылка при попытке назначить родителем группу с ID=%', i;
            END IF;

            DECLARE
                parent_current_id INT;
            BEGIN
                parent_current_id := new_parent;

                WHILE parent_current_id IS NOT NULL
                    LOOP
                        parent_current_id :=
                                (SELECT parent_product_group_id FROM product_group WHERE id = parent_current_id);

                        -- Если мы становимся родителями себя
                        IF parent_current_id = i THEN
                            RAISE EXCEPTION 'Обнаружена циклическая ссылка при попытке назначить группе с ID=% нового родителя с ID=%', i, new_parent;
                        END IF;
                    END LOOP;
            END;
        END LOOP;

    UPDATE product_group
    SET parent_product_group_id=new_parent
    WHERE id = ANY (group_ids);

    RETURN;
END;
$$ LANGUAGE plpgsql;

-- ==========
--   TESTS
-- ==========
-- DATA FOR TEST

/*
A - B - C - F
 \   \
  E   D
       \
        G

H -
*/

INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('A', 'Group A', NULL);
INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('B', 'Group B', (SELECT id FROM product_group WHERE name = 'A'));
INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('C', 'Group C', (SELECT id FROM product_group WHERE name = 'B'));
INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('D', 'Group D', (SELECT id FROM product_group WHERE name = 'B'));
INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('E', 'Group E', (SELECT id FROM product_group WHERE name = 'A'));
INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('F', 'Group F', (SELECT id FROM product_group WHERE name = 'C'));
INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('G', 'Group G', (SELECT id FROM product_group WHERE name = 'D'));
INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('H', 'Group H', NULL);

SELECT product_group.id,
       product_group.name,
       product_group.description,
       product_group.parent_product_group_id,
       pg.name AS parent_name
FROM product_group
         LEFT JOIN product_group pg ON product_group.parent_product_group_id = pg.id;

-- Several updates
CALL update_parent_product_group(7, ARRAY [8, 9, 11, 12]);

SELECT product_group.id,
       product_group.name,
       product_group.description,
       product_group.parent_product_group_id,
       pg.name AS parent_name
FROM product_group
         LEFT JOIN product_group pg ON product_group.parent_product_group_id = pg.id;

CALL update_parent_product_group(7, ARRAY [8]);
CALL update_parent_product_group(8, ARRAY [11]);
CALL update_parent_product_group(11, ARRAY [12]);
CALL update_parent_product_group(12, ARRAY [9]);

SELECT product_group.id,
       product_group.name,
       product_group.description,
       product_group.parent_product_group_id,
       pg.name AS parent_name
FROM product_group
         LEFT JOIN product_group pg ON product_group.parent_product_group_id = pg.id;

-- CYCLE
CALL update_parent_product_group(9, ARRAY [7]);
