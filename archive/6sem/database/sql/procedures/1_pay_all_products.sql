CREATE OR REPLACE FUNCTION create_payment_orders(
    _organization_id uuid,
    _target_currency varchar(3),
    _employee_to_role_id INTEGER)
    RETURNS TABLE
            (
                invoice_id       integer,
                payment_order_id uuid,
                invoice_sum      numeric,
                payment_sum      numeric
            )
AS
$$
DECLARE
    inv_id       integer;
    inv_currency varchar(3);
    pd_type      product_document_type;
    po_uuid      uuid;
    po_type      payment_order_type;
    total_sum    numeric;
    exch_rate    numeric;
BEGIN
    FOR inv_id IN (SELECT id FROM invoice WHERE organization_id = _organization_id)
        LOOP
            SELECT invoice.payment_order_id INTO po_uuid FROM invoice WHERE id = inv_id;

            IF po_uuid IS NULL THEN
                SELECT sum(p.price * pd.quantity), pd.type
                INTO total_sum, pd_type
                FROM product_document pd
                         JOIN product p ON pd.product_id = p.id
                WHERE pd.invoice_id = inv_id
                  AND p.is_active = true
                GROUP BY pd.type;

                SELECT currency_code
                INTO inv_currency
                FROM invoice
                WHERE id = inv_id;

                SELECT e.value
                INTO exch_rate
                FROM exchange_rate e
                WHERE e.source_currency = inv_currency
                  AND e.target_currency = _target_currency
                ORDER BY date DESC
                LIMIT 1;

                IF exch_rate IS NULL THEN
                    IF inv_currency = _target_currency THEN
                        SELECT 1 INTO exch_rate;
                    ELSE
                        RAISE EXCEPTION 'unknown exchange rate';
                    END IF;
                END IF;

                total_sum = total_sum * exch_rate;

                po_uuid = (SELECT uuid_generate_v4());

                IF pd_type = 'SALE' THEN
                    SELECT 'INCOMING' INTO po_type;
                ELSE
                    SELECT 'OUTCOMING' INTO po_type;
                end if;

                INSERT INTO payment_order(id, processed, date, type, total_sum, currency_code, employee_to_role_id)
                VALUES (po_uuid, FALSE, current_timestamp, po_type, total_sum, _target_currency, _employee_to_role_id);

                UPDATE invoice
                SET payment_order_id = po_uuid
                WHERE id = inv_id;

                invoice_id := inv_id;
                payment_order_id := po_uuid;
                invoice_sum := total_sum / exch_rate;
                payment_sum := total_sum;

                RETURN NEXT;
            END IF;
        END LOOP;
END
$$ LANGUAGE plpgsql;

-- ==========
--   TESTS
-- ==========
-- DATA FOR TEST

INSERT INTO "organization" ("id", "name", "address", "category", "license_number", "phone_number", "bank_details")
VALUES (uuid_generate_v4(), 'NEED_PAY', '1415 Small Town Rd', 'Healthcare', 'XY8989', '555-0204', 'BANK1005');

INSERT INTO "role"("name")
VALUES ('TEST_PAY_ALL_PRODUCTS');

INSERT INTO "employee" (full_name, supervisor_id)
VALUES ('MANAGER', NULL);

INSERT INTO "employee_to_role" ("id", "employee_id", "role_id")
VALUES ((SELECT id FROM employee WHERE full_name = 'MANAGER'),
        (SELECT id FROM employee WHERE full_name = 'MANAGER'),
        (SELECT id FROM role WHERE name = 'TEST_PAY_ALL_PRODUCTS'));

INSERT INTO currency ("code", "country", "description")
VALUES ('ABA', 'R', 'R'),
       ('BAB', 'R', 'R');

INSERT INTO exchange_rate ("source_currency", "target_currency", "date", "value")
VALUES ('ABA', 'BAB', CURRENT_DATE, 2);

INSERT INTO "product_info" ("article", "name", "certificate_number", "packaging", "company_name")
VALUES (uuid_generate_v4(), 'TEST_FOR_PAY', 'RU98809', 'Box', 'Aboba INC'),
       (uuid_generate_v4(), 'TEST_FOR_PAY2', 'EN88989', 'Box', 'Aboba INC');

INSERT INTO "product" (is_active, article, price, currency_code, quantity, employee_to_role_id)
VALUES (TRUE, (SELECT article FROM product_info WHERE name = 'TEST_FOR_PAY'), 10, 'BAB', 1000,
        (SELECT id FROM employee e WHERE e.full_name = 'MANAGER')),
       (TRUE, (SELECT article FROM product_info WHERE name = 'TEST_FOR_PAY2'), 10, 'ABA', 1000,
        (SELECT id FROM employee e WHERE e.full_name = 'MANAGER'));

INSERT INTO "invoice" ("date", "currency_code", "payment_order_id", "organization_id")
VALUES (CURRENT_DATE, 'BAB', NULL, (SELECT id FROM organization WHERE name = 'NEED_PAY')),
       (CURRENT_DATE, 'ABA', NULL, (SELECT id FROM organization WHERE name = 'NEED_PAY'));

INSERT INTO "product_document" (id, type, date, product_id, quantity, exchange_rate_id, employee_to_role_id, invoice_id)
VALUES (uuid_generate_v4(), 'SALE', CURRENT_DATE,
        (SELECT p.id
         FROM product p
                  JOIN product_info pi ON pi.article = p.article
         WHERE pi.name = 'TEST_FOR_PAY'
           AND p.is_active = true),
        10,
        NULL,
        (SELECT id FROM employee e WHERE e.full_name = 'MANAGER'),
        (SELECT id
         FROM invoice
         WHERE invoice.currency_code = 'BAB')),
       (uuid_generate_v4(), 'REFUND', CURRENT_DATE,
        (SELECT p.id
         FROM product p
                  JOIN product_info pi ON pi.article = p.article
         WHERE pi.name = 'TEST_FOR_PAY2'
           AND p.is_active = true),
        20,
        NULL,
        (SELECT id FROM employee e WHERE e.full_name = 'MANAGER'),
        (SELECT id
         FROM invoice
         WHERE invoice.currency_code = 'ABA'));

SELECT *
FROM invoice
WHERE currency_code = 'ABA'
   OR currency_code = 'BAB';

SELECT *
FROM product_document pd
JOIN product p ON p.id = pd.product_id
WHERE p.currency_code = 'ABA'
   OR p.currency_code = 'BAB';

-- CHECK PAYMENT
SELECT *
FROM payment_order
WHERE employee_to_role_id = (SELECT id FROM public.employee e WHERE e.full_name = 'MANAGER');

SELECT *
FROM create_payment_orders(
        (SELECT id FROM organization WHERE name = 'NEED_PAY'),
        'BAB',
        (SELECT id FROM employee WHERE full_name = 'MANAGER')
    );

-- CHECK PAYMENT
SELECT *
FROM payment_order
WHERE employee_to_role_id = (SELECT id FROM public.employee e WHERE e.full_name = 'MANAGER');
