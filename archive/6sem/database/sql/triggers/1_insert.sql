CREATE OR REPLACE FUNCTION check_product_quantity()
    RETURNS TRIGGER AS
$check_product_quantity$
DECLARE
    current_quantity INTEGER;
BEGIN
    SELECT quantity
    INTO current_quantity
    FROM product
    WHERE id = NEW.product_id
      AND is_active = true;

    IF current_quantity IS NULL THEN
        RAISE EXCEPTION 'No such product';
    END IF;

    IF NEW.type = 'SALE' THEN
        IF current_quantity < NEW.quantity THEN
            RAISE EXCEPTION 'Not enough items on stock';
        END IF;

        UPDATE product
        SET quantity = current_quantity - NEW.quantity
        WHERE id = NEW.product_id
          AND is_active = true;
    ELSIF NEW.type = 'REFUND' THEN
        UPDATE product
        SET quantity = current_quantity + NEW.quantity
        WHERE id = NEW.product_id
          AND is_active = true;
    END IF;

    RETURN NEW;
END
$check_product_quantity$ LANGUAGE plpgsql;

CREATE TRIGGER product_document_trigger
    BEFORE INSERT OR UPDATE
    ON product_document
    FOR EACH ROW
EXECUTE PROCEDURE check_product_quantity();

-- ==========
--   TESTS
-- ==========
-- DATA FOR TEST

INSERT INTO "organization" ("id", "name", "address", "category", "license_number", "phone_number", "bank_details")
VALUES (uuid_generate_v4(), 'TEST1', '1415 Small Town Rd', 'Healthcare', 'LN8888', '555-0204', 'BANK1005'),
       (uuid_generate_v4(), 'TEST2', '1415 Small Town Rd', 'Healthcare', 'LN9999', '555-0204', 'BANK1005'),
       (uuid_generate_v4(), 'TEST3', '1415 Small Town Rd', 'Healthcare', 'LN7777', '555-0204', 'BANK1005');

INSERT INTO "employee" (full_name, supervisor_id)
VALUES ('NICE MAN', NULL);

INSERT INTO "role"("name")
VALUES ('TEST_INSERT');

INSERT INTO "employee_to_role"(id, "employee_id", "role_id")
VALUES ((SELECT id FROM employee WHERE full_name = 'NICE MAN'),
        (SELECT id FROM employee WHERE full_name = 'NICE MAN'), (SELECT id FROM role WHERE name = 'TEST_INSERT'));

INSERT INTO currency ("code", "country", "description")
VALUES ('HYI', 'USA', 'NICE C_CK');

INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('TEST_FOR_SALE', 'TEST TEST TEST', NULL),
       ('TEST_FOR_SALE_ERR', 'TEST TEST TEST', NULL),
       ('TEST_FOR_REFUND', 'TEST TEST TEST', NULL);

INSERT INTO "product_info" ("article", "name", "certificate_number", "packaging", "company_name")
VALUES (uuid_generate_v4(), 'TEST_FOR_SALE', 'RU12345', 'Box', 'Aboba INC'),
       (uuid_generate_v4(), 'TEST_FOR_SALE_ERR', 'RU12345', 'Box', 'Aboba INC'),
       (uuid_generate_v4(), 'TEST_FOR_REFUND', 'RU23456', 'Box', 'Aboba2 INC');

INSERT INTO "product" (is_active, article, price, currency_code, quantity, employee_to_role_id, product_group_id)
VALUES (TRUE, (SELECT article FROM product_info WHERE name = 'TEST_FOR_SALE'), 228, 'HYI', 100,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN'),
        (SELECT id FROM product_group WHERE name = 'TEST_FOR_SALE')),
       (TRUE, (SELECT article FROM product_info WHERE name = 'TEST_FOR_SALE_ERR'), 337, 'HYI', 5,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN'),
        (SELECT id FROM product_group WHERE name = 'TEST_FOR_SALE_ERR')),
       (TRUE, (SELECT article FROM product_info WHERE name = 'TEST_FOR_REFUND'), 1488, 'HYI', 0,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN'),
        (SELECT id FROM product_group WHERE name = 'TEST_FOR_REFUND'));

INSERT INTO "invoice" ("date", "currency_code", "payment_order_id", "organization_id")
VALUES (CURRENT_DATE, 'HYI', NULL, (SELECT id FROM organization WHERE name = 'TEST1')),
       (CURRENT_DATE, 'HYI', NULL, (SELECT id FROM organization WHERE name = 'TEST2')),
       (CURRENT_DATE, 'HYI', NULL, (SELECT id FROM organization WHERE name = 'TEST3'));

-- CHECK
SELECT *
FROM invoice
WHERE currency_code = 'HYI';

SELECT *
FROM product
WHERE product_group_id IN (SELECT id
                           FROM product_group
                           WHERE name = 'TEST_FOR_SALE'
                              OR name = 'TEST_FOR_SALE_ERR'
                              OR name = 'TEST_FOR_REFUND');

-- BEFORE TESTS
SELECT *
FROM product_document pd
         JOIN product p ON p.id = pd.product_id
WHERE p.currency_code = 'HYI';

-- NO ERROR: old_quantity=100, product_quantity=99, new_quantity=1
INSERT INTO "product_document" (id, type, date, product_id, quantity, exchange_rate_id, employee_to_role_id, invoice_id)
VALUES (uuid_generate_v4(), 'SALE', CURRENT_DATE,
        (SELECT p.id
         FROM product p
                  JOIN product_info pi ON pi.article = p.article
         WHERE pi.name = 'TEST_FOR_SALE'
           AND p.is_active = true),
        99,
        NULL,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN'),
        (SELECT id
         FROM invoice
         WHERE invoice.organization_id = (SELECT id FROM organization WHERE name = 'TEST1')));


-- CHECK NO ERROR
SELECT *
FROM product
WHERE product_group_id IN (SELECT id
                           FROM product_group
                           WHERE name = 'TEST_FOR_SALE'
                              OR name = 'TEST_FOR_SALE_ERR'
                              OR name = 'TEST_FOR_REFUND');

-- ERROR: "not enough items on stock"
INSERT INTO "product_document" (id, type, date, product_id, quantity, exchange_rate_id, employee_to_role_id, invoice_id)
VALUES (uuid_generate_v4(), 'SALE', CURRENT_DATE,
        (SELECT p.id
         FROM product p
                  JOIN product_info pi ON pi.article = p.article
         WHERE pi.name = 'TEST_FOR_SALE'
           AND p.is_active = true),
        6,
        NULL,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN'),
        (SELECT id
         FROM invoice
         WHERE invoice.organization_id = (SELECT id FROM organization WHERE name = 'TEST2')));

-- CHECK ERROR
SELECT *
FROM product
WHERE product_group_id IN (SELECT id
                           FROM product_group
                           WHERE name = 'TEST_FOR_SALE'
                              OR name = 'TEST_FOR_SALE_ERR'
                              OR name = 'TEST_FOR_REFUND');

-- REFUND: old_quantity 0, quantity 228, new_quantity 228
INSERT INTO "product_document" (id, type, date, product_id, quantity, exchange_rate_id, employee_to_role_id, invoice_id)
VALUES (uuid_generate_v4(), 'REFUND', CURRENT_DATE,
        (SELECT p.id
         FROM product p
                  JOIN product_info pi ON pi.article = p.article
         WHERE pi.name = 'TEST_FOR_REFUND'
           AND p.is_active = true),
        228,
        NULL,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN'),
        (SELECT id
         FROM invoice
         WHERE invoice.organization_id = (SELECT id FROM organization WHERE name = 'TEST3')));

-- CHECK REFUND
SELECT *
FROM product
WHERE product_group_id IN (SELECT id
                           FROM product_group
                           WHERE name = 'TEST_FOR_SALE'
                              OR name = 'TEST_FOR_SALE_ERR'
                              OR name = 'TEST_FOR_REFUND');