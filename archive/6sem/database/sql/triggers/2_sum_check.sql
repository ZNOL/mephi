CREATE OR REPLACE FUNCTION update_total_sum()
    RETURNS TRIGGER AS
$update_payment_sum$
DECLARE
    old_quantity          integer;
    product_price         decimal;
    payment_order_id      uuid;
    processed             bool;
    product_currency_code varchar(3);
    invoice_currency_code varchar(3);
    currency_rate         DECIMAL;
    product_sum           DECIMAL;
BEGIN
    SELECT currency_code INTO product_currency_code FROM product WHERE product.id = NEW.product_id;
    SELECT quantity INTO old_quantity FROM product_document WHERE id = NEW.id;
    SELECT price INTO product_price FROM product WHERE id = NEW.product_id;
    SELECT invoice.payment_order_id, invoice.currency_code
    INTO payment_order_id, invoice_currency_code
    FROM invoice
    WHERE id = NEW.invoice_id;

    IF payment_order_id IS NOT NULL THEN
        SELECT payment_order.processed INTO processed FROM payment_order WHERE id = payment_order_id;

        IF processed = TRUE THEN
            RAISE EXCEPTION 'Cannot modify processed payment order';
        END IF;

        IF product_currency_code = invoice_currency_code THEN
            SELECT 1 INTO currency_rate;
        ELSIF NEW.exchange_rate_id IS NOT NULL THEN
            SELECT value
            INTO currency_rate
            FROM exchange_rate
            WHERE exchange_rate_id = NEW.exchange_rate_id;
        ELSE
            SELECT value
            INTO currency_rate
            FROM exchange_rate
            WHERE source_currency = product_currency_code
              AND target_currency = invoice_currency_code
            ORDER BY date DESC
            LIMIT 1;
        END IF;

        IF currency_rate IS NULL THEN
            RAISE EXCEPTION 'No conversion for currencies';
        END IF;

        product_sum = (NEW.quantity - old_quantity) * product_price * currency_rate;

        UPDATE payment_order
        SET total_sum = total_sum + product_sum
        WHERE id = payment_order_id;
    END IF;

    RETURN NEW;
END
$update_payment_sum$ LANGUAGE plpgsql;

CREATE TRIGGER update_total_sum_trigger
    BEFORE UPDATE OF quantity
    ON product_document
    FOR EACH ROW
EXECUTE PROCEDURE update_total_sum();

-- ==========
--   TESTS
-- ==========

-- DATA FOR TEST
INSERT INTO "organization" ("id", "name", "address", "category", "license_number", "phone_number", "bank_details")
VALUES (uuid_generate_v4(), 'TEST4', '1415 Small Town Rd', 'Healthcare', 'XY8888', '555-0204', 'BANK1005');

INSERT INTO "role"("name")
VALUES ('TEST_SUM_CHECK');

INSERT INTO "employee" (full_name, supervisor_id)
VALUES ('NICE MAN2', NULL);

INSERT INTO "employee_to_role" ("id", "employee_id", "role_id")
VALUES ((SELECT id FROM employee WHERE full_name = 'NICE MAN2'),
        (SELECT id FROM employee WHERE full_name = 'NICE MAN2'),
        (SELECT id FROM role WHERE name = 'TEST_SUM_CHECK'));

INSERT INTO currency ("code", "country", "description")
VALUES ('ZZZ', 'R', '\(-_-)/'),
       ('VVV', 'R', '\(0-0)/');

INSERT INTO "product_group" ("name", "description", "parent_product_group_id")
VALUES ('TEST_FOR_SUM', 'TEST TEST TEST', NULL),
       ('TEST_FOR_SUM2', 'TEST TEST TEST', NULL);

INSERT INTO "product_info" ("article", "name", "certificate_number", "packaging", "company_name")
VALUES (uuid_generate_v4(), 'TEST_FOR_SUM', 'RU88888', 'Box', 'Aboba INC'),
       (uuid_generate_v4(), 'TEST_FOR_SUM2', 'EN88888', 'Box', 'Aboba INC');

INSERT INTO "product" (is_active, article, price, currency_code, quantity, employee_to_role_id, product_group_id)
VALUES (TRUE, (SELECT article FROM product_info WHERE name = 'TEST_FOR_SUM'), 10, 'ZZZ', 1000,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2'),
        (SELECT id FROM product_group WHERE name = 'TEST_FOR_SUM')),
       (TRUE, (SELECT article FROM product_info WHERE name = 'TEST_FOR_SUM2'), 10, 'VVV', 1000,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2'),
        (SELECT id FROM product_group WHERE name = 'TEST_FOR_SUM2'));

-- TEST IN SELF

SELECT *
FROM product
WHERE currency_code = 'ZZZ'
   OR currency_code = 'VVV';

-- CHECK PAYMENT
SELECT *
FROM payment_order
WHERE employee_to_role_id = (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2');

INSERT INTO "invoice" ("date", "currency_code", "payment_order_id", "organization_id")
VALUES (CURRENT_DATE, 'ZZZ', NULL, (SELECT id FROM organization WHERE name = 'TEST4'));

-- CHECK INVOICE
SELECT *
FROM invoice
WHERE currency_code = 'ZZZ';

INSERT INTO "product_document" (id, type, date, product_id, quantity, exchange_rate_id, employee_to_role_id, invoice_id)
VALUES (uuid_generate_v4(), 'SALE', CURRENT_DATE,
        (SELECT p.id
         FROM product p
                  JOIN product_info pi ON pi.article = p.article
         WHERE pi.name = 'TEST_FOR_SUM'
           AND p.is_active = true),
        0,
        NULL,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2'),
        (SELECT id
         FROM invoice
         WHERE invoice.organization_id = (SELECT id FROM organization WHERE name = 'TEST4'))),
       (uuid_generate_v4(), 'SALE', CURRENT_DATE,
        (SELECT p.id
         FROM product p
                  JOIN product_info pi ON pi.article = p.article
         WHERE pi.name = 'TEST_FOR_SUM2'
           AND p.is_active = true),
        0,
        NULL,
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2'),
        (SELECT id
         FROM invoice
         WHERE invoice.organization_id = (SELECT id FROM organization WHERE name = 'TEST4')));

SELECT *
FROM product_document
         JOIN product on product.id = product_document.product_id
WHERE product.currency_code = 'ZZZ'
   OR product.currency_code = 'VVV';

INSERT INTO "payment_order" ("id", "processed", "date", "type", "total_sum", "currency_code", "employee_to_role_id")
VALUES (uuid_generate_v4(), FALSE, '2023-01-03 11:00:00', 'OUTCOMING', 0, 'ZZZ',
        (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2'));

SELECT *
FROM payment_order
WHERE employee_to_role_id = (SELECT id FROM public.employee e WHERE e.full_name = 'NICE MAN2');

UPDATE invoice
SET payment_order_id=(SELECT id FROM payment_order WHERE currency_code = 'ZZZ')
WHERE currency_code = 'ZZZ';

-- CHECK INVOICE
SELECT *
FROM invoice
WHERE currency_code = 'ZZZ';

-- Change quantity.
UPDATE product_document
SET quantity=100
WHERE employee_to_role_id = (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2')
  AND product_id = (SELECT id FROM product WHERE currency_code = 'ZZZ');

SELECT *
FROM product_document
         JOIN product on product.id = product_document.product_id
WHERE product.currency_code = 'ZZZ';

-- CHECK PAYMENT
SELECT *
FROM payment_order
WHERE employee_to_role_id = (SELECT id FROM public.employee e WHERE e.full_name = 'NICE MAN2');

UPDATE product_document
SET quantity=80
WHERE employee_to_role_id = (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2')
  AND product_id = (SELECT id FROM product WHERE currency_code = 'ZZZ');

-- CHECK PAYMENT
SELECT *
FROM payment_order
WHERE employee_to_role_id = (SELECT id FROM public.employee e WHERE e.full_name = 'NICE MAN2');

UPDATE product_document
SET quantity=0
WHERE employee_to_role_id = (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2')
  AND product_id = (SELECT id FROM product WHERE currency_code = 'ZZZ');

-- CHECK PAYMENT
SELECT *
FROM payment_order
WHERE employee_to_role_id = (SELECT id FROM public.employee e WHERE e.full_name = 'NICE MAN2');

-- TEST exchange_rates

INSERT INTO exchange_rate ("source_currency", "target_currency", "date", "value")
VALUES ('VVV', 'ZZZ', CURRENT_DATE, 2);

-- CHECK PAYMENT
SELECT *
FROM payment_order
WHERE employee_to_role_id = (SELECT id FROM public.employee e WHERE e.full_name = 'NICE MAN2');

UPDATE product_document
SET quantity=2
WHERE employee_to_role_id = (SELECT id FROM employee e WHERE e.full_name = 'NICE MAN2')
  AND product_id = (SELECT id FROM product WHERE currency_code = 'VVV');

-- CHECK PAYMENT
SELECT *
FROM payment_order
WHERE employee_to_role_id = (SELECT id FROM public.employee e WHERE e.full_name = 'NICE MAN2');
