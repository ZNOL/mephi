WITH SalesData AS (SELECT pd.product_id,
                          SUM(pd.quantity)                                AS total_sold,
                          COUNT(pd.id)                                    AS sales_count,
                          EXTRACT(DAY FROM (CURRENT_DATE - MIN(pd.date))) AS total_days
                   FROM product_document pd
                   WHERE pd.date >= '2023-01-01'
                     AND pd.date <= CURRENT_DATE -- Менять дату тут
                   GROUP BY pd.id),
     Inventory AS (SELECT p.id,
                          p.article,
                          pi.name AS product_name,
                          p.quantity
                   FROM product p
                            JOIN
                        product_info pi ON p.article = pi.article
                   WHERE p.is_active = true)
SELECT i.product_name,
       i.quantity                                            AS current_inventory,
       COALESCE(sd.total_sold, 0)                            AS sales_quantity,
       COALESCE(sd.total_days / NULLIF(sd.total_sold, 0), 0) AS average_days_per_sale,
       COALESCE(sd.total_sold / NULLIF(sd.total_days, 0), 0) AS units_sold_per_day,
       CASE
           WHEN COALESCE(sd.total_sold / NULLIF(sd.total_days, 0), 0) > 0
               THEN i.quantity / (sd.total_sold / sd.total_days)
           END                                               AS forecast_days_remaining
FROM Inventory i
         JOIN
     SalesData sd ON i.id = sd.product_id
ORDER BY i.product_name;