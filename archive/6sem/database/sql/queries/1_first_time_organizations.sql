SELECT SUM(pd.quantity)                   as total_quantity,
       SUM(pd.quantity * p.price)         as total_cost,
       COUNT(DISTINCT p.product_group_id) as product_group_count,
       invoice.organization_id            as organization_id,
       invoice.date                       as date_of_application,
       invoice.currency_code              as currency,
       CASE
           WHEN po.processed = true
               THEN 'Yes'
           ELSE 'No'
           END                            as invoice_paid,
       CASE
           WHEN po.processed = true
               THEN po.date
           END                            as processed_date,
       e.full_name                        AS employee_full_name,
       CURRENT_DATE - invoice.date        AS days_since_first_contact
FROM invoice
         JOIN payment_order po on invoice.payment_order_id = po.id
         JOIN product_document pd on invoice.id = pd.invoice_id
         JOIN product p on pd.product_id = p.id
         JOIN employee_to_role etr ON p.employee_to_role_id = etr.id
         JOIN employee e ON etr.employee_id = e.id
WHERE 1 = (SELECT COUNT(*) FROM invoice inv WHERE invoice.organization_id = inv.organization_id)
GROUP BY invoice.organization_id, invoice.date, invoice.currency_code, invoice_paid, processed_date, e.full_name, days_since_first_contact;
