WITH ABS AS (SELECT p.article                     AS article,
                    current_product.price         AS current_price,
                    current_product.currency_code AS currency,
                    COUNT(p.id)                   AS price_change_count,
                    AVG(p.price)                  AS average_price
             FROM product p
                      JOIN
                  (SELECT article, price, currency_code
                   FROM product
                   WHERE is_active = true) current_product ON p.article = current_product.article
             WHERE p.article IN (SELECT article FROM product WHERE is_active = false)
             GROUP BY p.article, current_product.price, current_product.currency_code
             ORDER BY price_change_count DESC)
SELECT *
FROM ABS
WHERE price_change_count = (SELECT MAX(ABS.price_change_count) FROM ABS);