CREATE EXTENSION IF NOT EXISTS tablefunc;

SELECT *
FROM crosstab(
             $$
             WITH ExchangeRates AS (SELECT er."source_currency", er."target_currency", er."date", er."value"
                                    FROM "exchange_rate" er
                                             INNER JOIN (SELECT "source_currency", "target_currency", MAX("date") AS max_date
                                                         FROM "exchange_rate"
                                                         GROUP BY "source_currency", "target_currency") er_max
                                                        ON er."source_currency" = er_max."source_currency" AND
                                                           er."target_currency" = er_max."target_currency" AND
                                                           er."date" = er_max.max_date),
                  DecartCurrencyTable AS (SELECT currency1.code AS first_code,
                                                 currency2.code AS second_code,
                                                 er.value
                                          FROM currency as currency1
                                                   CROSS JOIN currency as currency2
                                                   FULL JOIN ExchangeRates er on currency1.code = er.source_currency AND
                                                                                 currency2.code = er.target_currency),
                  EnhanceCurrencyTable AS (SELECT DecartCurrencyTable.first_code::TEXT,
                                                  DecartCurrencyTable.second_code::TEXT,
                                                  CASE
                                                      WHEN DecartCurrencyTable.first_code = DecartCurrencyTable.second_code
                                                          THEN 1::TEXT
                                                      WHEN COALESCE(DecartCurrencyTable.value, 1 / t2.value) IS NOT NULL
                                                          THEN COALESCE(DecartCurrencyTable.value, 1 / t2.value)::TEXT
                                                      ELSE '-'::TEXT
                                         END AS value
                              FROM DecartCurrencyTable
                                       JOIN DecartCurrencyTable t2
                                            ON DecartCurrencyTable.first_code = t2.second_code AND
                                               DecartCurrencyTable.second_code = t2.first_code)
  SELECT first_code, second_code, value FROM EnhanceCurrencyTable ORDER BY 1
                     $$
         )
         AS pivot("first_code" TEXT, "AUD" TEXT, "CAD" TEXT, "CHF" TEXT, "EUR" TEXT, "GBP" TEXT, "JPY" TEXT,
                  "USD" TEXT);