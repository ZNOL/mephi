WITH RECURSIVE
    ParentGroups AS (
        -- anchor member
        SELECT id,
               parent_product_group_id,
               name,
               name as top_level_name -- начальное значение для самой верхней группы
        FROM product_group
        WHERE parent_product_group_id IS NULL

        UNION ALL

        -- recursive term
        SELECT pg.id,
               pg.parent_product_group_id,
               pg.name,
               p.top_level_name -- сохраняем значение самой верхней группы
        FROM product_group pg
                 INNER JOIN ParentGroups p ON pg.parent_product_group_id = p.id),

    ProductDetails AS (SELECT pi.name AS product_name,
                              p.quantity,
                              pg.name AS group_name,
                              pg.parent_product_group_id,
                              pg.id   AS pg_id
                       FROM product p
                                JOIN product_info pi ON pi.article = p.article
                                JOIN product_group pg ON p.product_group_id = pg.id
                       WHERE p.is_active = true)

SELECT pd.product_name,
       pd.quantity       AS quantity_in_stock,
       pd.group_name     AS parent_group_name,
       pg.top_level_name AS top_level_group_name
FROM ProductDetails pd
         JOIN ParentGroups pg ON pd.pg_id = pg.id -- Самая верхняя группа
ORDER BY pd.product_name;