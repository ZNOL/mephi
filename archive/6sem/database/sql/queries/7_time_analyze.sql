SELECT * FROM product;

DROP INDEX IF EXISTS idx_product_price;
DROP INDEX IF EXISTS idx_product_article;
DROP INDEX IF EXISTS idx_product_info_article;

set enable_seqscan = on;

SELECT *
FROM product p
         JOIN product_info pi on pi.article = p.article
WHERE 100 < p.price
  AND p.price < 200;

CREATE INDEX idx_product_price ON product (price);
CREATE INDEX idx_product_article ON product (article);
CREATE INDEX idx_product_info_article ON product_info (article);

ANALYZE product;
ANALYZE product_info;
--
set enable_seqscan = off;

SELECT *
FROM product p
         JOIN product_info pi on pi.article = p.article
WHERE 100 < p.price
  AND p.price < 200;

