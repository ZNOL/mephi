WITH DateStats AS (SELECT date_trunc('week', CURRENT_DATE) - interval '100 weeks' as start_date,
                          date_trunc('week', CURRENT_DATE - interval '0 day')     as end_date),
     FilteredDocuments AS (SELECT EXTRACT(DOW FROM pd.date) AS day_of_week,
                                  pd.product_id,
                                  pd.quantity,
                                  (pd.quantity * p.price)   AS total_price
                           FROM product_document pd
                                    JOIN product p ON p.id = pd.product_id
                                    JOIN DateStats ds ON pd.date BETWEEN ds.start_date AND ds.end_date),
     AggregatedData AS (SELECT day_of_week,
                               COUNT(*)         as total_documents,
                               AVG(quantity)    as avg_quantity,
                               SUM(quantity)    as total_quantity,
                               AVG(total_price) as avg_total_price,
                               MAX(total_price) as max_total_price,
                               MIN(total_price) as min_total_price
                        FROM FilteredDocuments
                        GROUP BY day_of_week),
     TopProductPerDay AS (SELECT sub.day_of_week,
                                 sub.product_id
                          FROM (SELECT day_of_week,
                                       product_id,
                                       ROW_NUMBER() OVER (PARTITION BY day_of_week ORDER BY SUM(quantity) DESC) AS rank
                                FROM FilteredDocuments
                                GROUP BY day_of_week, product_id) sub
                          WHERE sub.rank = 1)
SELECT CASE
           WHEN ad.day_of_week = 0 THEN 'Sunday'
           WHEN ad.day_of_week = 1 THEN 'Monday'
           WHEN ad.day_of_week = 2 THEN 'Tuesday'
           WHEN ad.day_of_week = 3 THEN 'Wednesday'
           WHEN ad.day_of_week = 4 THEN 'Thursday'
           WHEN ad.day_of_week = 5 THEN 'Friday'
           WHEN ad.day_of_week = 6 THEN 'Saturday'
           END               AS day_of_week,
       ad.total_documents,
       ad.total_quantity / 4 AS average_documents,
       ad.total_quantity,
       ad.avg_quantity,
       ad.avg_total_price,
       ad.max_total_price,
       ad.min_total_price,
       tp.product_id    AS top_selling_product_id
FROM AggregatedData ad
         JOIN
     TopProductPerDay tp ON ad.day_of_week = tp.day_of_week
ORDER BY ad.day_of_week;