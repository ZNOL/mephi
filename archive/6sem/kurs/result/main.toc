\babel@toc {russian}{}\relax 
\contentsline {chapter}{\numberline {1}Проектирование блока операций}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Алгоритм выполнений операций}{3}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Алгоритм операции умножение}{3}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Алгоритм операции вычитания}{4}{subsection.1.1.2}%
\contentsline {subsection}{\numberline {1.1.3}Примеры}{5}{subsection.1.1.3}%
\contentsline {subsubsection}{Примеры операции умножения}{5}{subsection.1.1.3}%
\contentsline {subsubsection}{Примеры операции вычитания}{7}{table.caption.2}%
\contentsline {section}{\numberline {1.2}Функциональная схема блока операций}{8}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Проектирование логических элементом блока операций}{8}{subsection.1.2.1}%
\contentsline {subsubsection}{Регистр первого операнда RA}{8}{subsection.1.2.1}%
\contentsline {subsubsection}{Регистр второго операнда RB}{9}{table.caption.5}%
\contentsline {subsubsection}{Регистр частичных сумм RRT}{9}{table.caption.6}%
\contentsline {subsection}{\numberline {1.2.2}Регистр результата RR}{10}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Регистр признака RPR}{10}{subsection.1.2.3}%
\contentsline {subsubsection}{Комбинационная схема КС1}{11}{table.caption.9}%
\contentsline {subsubsection}{Комбинационная схема КС2}{11}{table.caption.10}%
\contentsline {subsubsection}{Комбинационная схема КС3}{12}{table.caption.11}%
\contentsline {subsubsection}{Комбинационная схема КС4}{13}{table.caption.12}%
\contentsline {subsubsection}{Комбинационная схема КС5}{13}{table.caption.13}%
\contentsline {subsubsection}{Сумматор SM}{14}{table.caption.14}%
\contentsline {subsubsection}{Логическая схема АЛУ}{15}{table.caption.14}%
\contentsline {section}{\numberline {1.3}Управляющие сигналы и сигналы-признаки блока операций}{16}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Микропрограммы выполнения операций}{16}{subsection.1.3.1}%
\contentsline {subsubsection}{Микропрограмма операции умножения}{16}{subsection.1.3.1}%
\contentsline {subsubsection}{Микропрограмма операции вычитания}{17}{subsection.1.3.1}%
\contentsline {chapter}{\numberline {2}Проектирование схемного местного устройства управления}{19}{chapter.2}%
\contentsline {section}{\numberline {2.1}Проектирование датчиков состояний}{19}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Выражение для D-входов триггеров}{19}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Выражение для управляющих сигналов}{20}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Логическая схема МУУ}{20}{section.2.2}%
\contentsline {section}{\numberline {2.3}Размещение схемы арифметико-логического устройства на кристале}{21}{section.2.3}%
\contentsline {section}{\numberline {2.4}Временное моделирование}{21}{section.2.4}%
\contentsline {section}{\numberline {2.5}Функциональное моделирование АЛУ}{23}{section.2.5}%
\contentsline {chapter}{\numberline {3}Проектирование блока управления командами}{25}{chapter.3}%
\contentsline {section}{\numberline {3.1}Система памяти}{25}{section.3.1}%
\contentsline {section}{\numberline {3.2}Формат команд и способы адресации}{25}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Регистровая адресация}{26}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Автодекрементная адресация (вариант 2)}{26}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Формат команд}{26}{subsection.3.2.3}%
\contentsline {section}{\numberline {3.3}Алгоритм работы БУК}{28}{section.3.3}%
\contentsline {section}{\numberline {3.4}Функциональная схема БУК}{29}{section.3.4}%
\contentsline {section}{\numberline {3.5}Логическая схема блока управления командами}{30}{section.3.5}%
\contentsline {section}{\numberline {3.6}Схема алгоритма микропрограммы выполнения команд}{31}{section.3.6}%
\contentsline {section}{\numberline {3.7}Тестирование БУК}{31}{section.3.7}%
\contentsline {chapter}{\numberline {4}Проектирование блока выработки микрокоманд}{36}{chapter.4}%
\contentsline {section}{\numberline {4.1}Определение формата микрокоманд}{36}{section.4.1}%
\contentsline {section}{\numberline {4.2}Модифицированный алгоритм микропрограммы выполнения команд}{37}{section.4.2}%
\contentsline {section}{\numberline {4.3}Cхема БМК}{38}{section.4.3}%
\contentsline {section}{\numberline {4.4}Заключение}{41}{section.4.4}%
\contentsline {section}{\numberline {4.5}Использованная литература}{42}{section.4.5}%
