#include <cstdint>
#include <iostream>
#include <queue>
#include <vector>

using namespace std;
namespace fast {
#define iom                                                                    \
  cin.tie(0);                                                                  \
  cout.tie(0);                                                                 \
  ios_base::sync_with_stdio(0);
#define out cout.flush();
#define endl "\n"

typedef size_t szt;
typedef long long ll;
typedef long double ld;

typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
typedef pair<ld, ld> pdd;

#define sqr(x) (x) * (x)
#define sz(x) (int)(x).size()
#define all(x) x.begin(), x.end()

#define pb push_back
#define eb emplace_back
#define ins insert
#define mp make_pair
#define F first
#define S second
} // namespace fast
using namespace fast;

int n, m;
vector<vector<int>> gr;
vector<int> distances;

void bfs(int start) {
  distances[start] = 0;

  queue<int> q;
  q.push(start);
  while (!q.empty()) {
    auto v = q.front();
    q.pop();

    for (const auto &u : gr[v]) {
      if (distances[u] > distances[v] + 1) {
        q.push(u);
        distances[u] = distances[v] + 1;
      }
    }
  }
}

pair<int, int> find_max_value(vector<int> &array) { // <value, idx>
  if (array.empty()) {
    return {-1, -1};
  }

  int mx = array[0];
  int idx = 0;
  for (int i = 1; i < array.size(); ++i) {
    if (array[i] > mx) {
      mx = array[i];
      idx = i;
    }
  }

  return {mx, idx};
}

int find_diameter() {
  int ans = INT32_MIN;
  for (int start = 0; start < n; ++start) {
    for (int i = 0; i < n; ++i) {
      distances[i] = INT32_MAX;
    }

    bfs(start);

    auto [mx, mx_idx] = find_max_value(distances);
    ans = max(ans, mx);
  }
  return ans;
}

void solve() {
  cin >> n >> m;

  gr.resize(n);
  for (int i = 0; i < m; ++i) {
    int u, v;
    cin >> u >> v;

    gr[u].push_back(v);
    gr[v].push_back(u);
  }

  distances.resize(n);

  cout << find_diameter() << endl;
}

int main() {
  iom;

  int T = 1;
  // cin >> T;
  while (T--) {
    solve();
  }

  out;
  return 0;
}
