#include <cstdint>
#include <iostream>
#include <vector>

using namespace std;
namespace fast {
#define iom                                                                    \
  cin.tie(0);                                                                  \
  cout.tie(0);                                                                 \
  ios_base::sync_with_stdio(0);
#define out cout.flush();
#define endl "\n"

typedef size_t szt;
typedef long long ll;
typedef long double ld;

typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
typedef pair<ld, ld> pdd;

#define sqr(x) (x) * (x)
#define sz(x) (int)(x).size()
#define all(x) x.begin(), x.end()

#define pb push_back
#define eb emplace_back
#define ins insert
#define mp make_pair
#define F first
#define S second
} // namespace fast
using namespace fast;

int n, m;
vector<vector<int>> gr;
vector<char> used;
vector<int> distances;

void dfs(int u) {
  used[u] = 1;

  for (const auto &v : gr[u]) {
    if (!used[v] && distances[v] > distances[u] + 1) {
      distances[v] = distances[u] + 1;
      dfs(v);
    }
  }
}

pair<int, int> find_max_value(vector<int> &array) { // <value, idx>
  if (array.empty()) {
    return {-1, -1};
  }

  int mx = array[0];
  int idx = 0;
  for (int i = 1; i < array.size(); ++i) {
    if (array[i] > mx) {
      mx = array[i];
      idx = i;
    }
  }

  return {mx, idx};
}

int find_diameter() {
  for (int i = 0; i < n; ++i) {
    distances[i] = INT32_MAX;
    used[i] = 0;
  }

  distances[0] = 0;
  dfs(0);

  auto [mx1, mx_idx1] = find_max_value(distances);

  for (int i = 0; i < n; ++i) {
    distances[i] = INT32_MAX;
    used[i] = 0;
  }
  distances[mx_idx1] = 0;
  dfs(mx_idx1);

  auto [mx, mx_idx] = find_max_value(distances);

  return mx;
}

void solve() {
  cin >> n >> m;

  gr.resize(n);
  for (int i = 0; i < m; ++i) {
    int u, v;
    cin >> u >> v;

    gr[u].push_back(v);
    gr[v].push_back(u);
  }

  used.resize(n);
  distances.resize(n);

  cout << find_diameter() << endl;
}

int main() {
  iom;

  int T = 1;
  // cin >> T;
  while (T--) {
    solve();
  }

  out;
  return 0;
}
