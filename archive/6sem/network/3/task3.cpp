#include <cstdint>
#include <iostream>
#include <queue>
#include <vector>

using namespace std;
namespace fast {
#define iom                                                                    \
  cin.tie(0);                                                                  \
  cout.tie(0);                                                                 \
  ios_base::sync_with_stdio(0);
#define out cout.flush();
#define endl "\n"

typedef size_t szt;
typedef long long ll;
typedef long double ld;

typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
typedef pair<ld, ld> pdd;

#define sqr(x) (x) * (x)
#define sz(x) (int)(x).size()
#define all(x) x.begin(), x.end()

#define pb push_back
#define eb emplace_back
#define ins insert
#define mp make_pair
#define F first
#define S second
} // namespace fast
using namespace fast;

struct edge {
  int u, v, cap, flow;

  edge(int _u, int _v, int _cap, int _flow)
      : u(_u), v(_v), cap(_cap), flow(_flow) {}
};

int n, m;
vector<vector<int>> gr;
vector<edge> edges;

void add_edge(int u, int v, int cap) {
  gr[u].push_back(edges.size());
  edges.emplace_back(u, v, cap, 0);

  gr[v].push_back(edges.size());
  edges.emplace_back(v, u, 0, 0);
}

bool bfs(int s, int t, vector<int> &d) {
  queue<int> q;
  q.push(s);

  d.assign(n, -1);
  d[s] = 0;
  while (!q.empty() && d[t] == -1) {
    int u = q.front();
    q.pop();

    for (size_t i = 0; i < gr[u].size(); ++i) {
      int idx = gr[u][i];
      int v = edges[idx].v;

      if (d[v] == -1 && edges[idx].flow < edges[idx].cap) {
        q.push(v);
        d[v] = d[u] + 1;
      }
    }
  }

  // cout << "finish bfs with d[t] = " << d[t] << endl;
  return d[t] != -1;
}

int dfs(int u, int t, int flow, vector<int> &ptr, const vector<int> &d) {
  // cout << "find flow = " << flow << " from u = " << u << " and stock = " << t
  // << endl;

  if (!flow) {
    return 0;
  }

  if (u == t) {
    return flow;
  }

  for (; ptr[u] < gr[u].size(); ++ptr[u]) {
    int idx = gr[u][ptr[u]];
    int v = edges[idx].v;

    // cout << "going to v = " << v << endl;

    if (d[v] != d[u] + 1) {
      continue;
    }

    int new_flow = min(flow, edges[idx].cap - edges[idx].flow);
    int pushed = dfs(v, t, new_flow, ptr, d);
    if (pushed) {
      edges[idx].flow += pushed;
      edges[idx ^ 1].flow -= pushed;
      return pushed;
    }
  }

  return 0;
}

int find_max_flow(int start, int finish) {
  int flow = 0;
  vector<int> d(n, -1);

  while (true) {
    if (!bfs(start, finish, d)) {
      break;
    }

    vector<int> ptr(n, 0);
    while (int pushed = dfs(start, finish, INT32_MAX, ptr, d)) {
      flow += pushed;
    }
  }
  return flow;
} // O(n^2 * m)

int find_edge_connectivity() {
  int ans = INT32_MAX;
  for (int u = 0; u < n; ++u) {
    for (int v = u + 1; v < n; ++v) {

      for (int i = 0; i < edges.size(); i += 2) {
        edges[i] = edge(edges[i].u, edges[i].v, 1, 0);
        edges[i ^ 1] = edge(edges[i].u, edges[i].v, 0, 0);
      }

      auto flow = find_max_flow(u, v);
      ans = min(ans, flow);
    }
  }
  return ans;
}

void solve() {
  cin >> n >> m;

  gr.resize(n);
  vector<int> degree(n);
  for (int i = 0; i < m; ++i) {
    int u, v;
    cin >> u >> v;

    add_edge(u, v, 1);
    add_edge(v, u, 1);

    degree[u]++;
    degree[v]++;
  }

  int mn = INT32_MAX;
  for (int i = 0; i < n; ++i) {
    mn = min(mn, degree[i]);
  }

  cout << "min degree = " << mn << endl;

  cout << find_edge_connectivity() << endl;
}

int main() {
  iom;

  int T = 1;
  // cin >> T;
  while (T--) {
    solve();
  }

  out;
  return 0;
}
