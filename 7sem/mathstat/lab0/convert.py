import pandas as pd

df = pd.read_csv("out_grouped.csv")

df['tradedate'] = pd.to_datetime(df['tradedate'])
df['tradedate'] = df['tradedate'].astype(int) // 10**9

df['updated_at'] = pd.to_datetime(df['updated_at'])
df['updated_at'] = df['updated_at'].astype(int) // 10**9

"""
VALUE	Объем	FIXED	16.2	Объем совершенных сделок аукциона, выраженный в валюте расчетов. В течение аукциона показывает ожидаемый объем.
VOLUME	Количество	INTEGER	16	Объем совершенных сделок, в единицах ценных бумаг. В течение аукциона показывает ожидаемый объем.
NUMTRADES	Сделок	INTEGER	9	Количество заключенных сделок
"""

df.to_csv("out_grouped_converted.csv")
