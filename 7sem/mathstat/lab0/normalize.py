import pandas as pd

df = pd.read_csv("out_grouped_converted.csv")

min_tradedate = min(df['tradedate'])

df['tradedate'] -= min_tradedate

min_updated_at = min(df['updated_at'])
df['updated_at'] -= min_updated_at

df.to_csv("out_norm.csv")
