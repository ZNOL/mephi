import pandas as pd

df = pd.read_csv("out.csv")

df = df[(df['value'] != 0) & (df['volume'] != 0) & (df['numtrades'] != 0)]

market_names = ['moexboard', 'ndm', 'repo', 'ccp', 'shares', 'otc']
for market in market_names:
    df[f'is_{market}'] = 0

for index, row in df.iterrows():
    df.at[index, f'is_{row["market_name"]}'] = 1

df = df.drop(columns=["market_name"])

print(df)

df.to_csv("out_grouped.csv")
