import requests
import pandas as pd
import datetime
from matplotlib import pyplot as plt

# https://iss.moex.com/iss/reference/
# https://www.moex.com/ru/issue.aspx?board=TQBR&code=YDEX

"""
      Unnamed: 0 market_name               market_title engine   tradedate secid         value   volume  numtrades           updated_at
0              0      shares                Рынок акций  stock  2021-12-16  YNDX  8.530175e+09  1817894      82131  2022-11-14 09:52:56
1              1         ndm  Режим переговорных сделок  stock  2021-12-16  YNDX  8.146018e+08   175086          4  2022-11-14 09:52:56
2              2         otc                        ОТС  stock  2021-12-16  YNDX  4.216070e+08    90353         29  2022-11-14 09:52:56

По f(market_name, engine, tradedate, secid, numtrades) = value 
"""
"""
из последовательности без нарушения времени удалить 20% данных
Для обучения использовать несколько раз предыдущий шаг
"""


def download_data():
    amount_of_days = 1000
    finish_date = datetime.datetime.now()

    df = None
    for day_to_subtract in range(amount_of_days):
        cur_date = finish_date - datetime.timedelta(days=day_to_subtract)

        str_date = cur_date.strftime("%Y-%m-%d")
        j = requests.get(
            f'https://iss.moex.com/iss/securities/YNDX/aggregates.json?date={str_date}').json()
        data = [{k: r[i] for i, k in enumerate(
            j['aggregates']['columns'])} for r in j['aggregates']['data']]

        cur_df = pd.DataFrame(data)
        if df is None:
            df = cur_df
        else:
            df = pd.concat([cur_df, df])
    return df


def load_data():
    df = pd.read_csv("in.csv")
    return df


df = load_data()

print('Count of rows', len(df))
print('Different market_names', list(set(df['market_name'])))

print(df)

df.to_csv("out.csv")
plt.plot(list(df['volume']))
plt.savefig("all_volumes.png")
