import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

"""
,Unnamed: 0.1,Unnamed: 0,market_title,engine,tradedate,secid,value,volume,numtrades,updated_at,is_moexboard,is_ndm,is_repo,is_ccp,is_shares,is_otc
"""


def plot_variable_vs_value(df, variable):
    plt.figure(figsize=(10, 6))
    # sns.pointplot(x=variable, y='value', data=df, palette='viridis')
    sns.scatterplot(x=variable, y='value', data=df)
    # sns.lineplot(x='tradedate', y='value', data=df)
    plt.title(f'Value vs {variable}')
    # plt.show()
    plt.savefig(f"graph_{variable}.png")


df = pd.read_csv("out_grouped.csv")

plot_variable_vs_value(df, 'engine')
plot_variable_vs_value(df, 'tradedate')
plot_variable_vs_value(df, 'secid')
plot_variable_vs_value(df, 'volume')
plot_variable_vs_value(df, 'numtrades')
plot_variable_vs_value(df, 'is_moexboard')
plot_variable_vs_value(df, 'is_ndm')
plot_variable_vs_value(df, 'is_repo')
plot_variable_vs_value(df, 'is_ccp')
plot_variable_vs_value(df, 'is_shares')
plot_variable_vs_value(df, 'is_otc')


print(df)
