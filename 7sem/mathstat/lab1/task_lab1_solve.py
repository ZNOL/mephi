import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt


df = pd.read_csv("task_lab1_data.csv")

x = np.array(df["x"]).reshape(-1, 1)
y = np.array(df["y"])

model = LinearRegression()
model.fit(x, y)

print(df)

slope = model.coef_[0]
intercept = model.intercept_

print(f"f(x) = {slope:.2f} * x + {intercept:.2f}")

plt.scatter(x, y, color='blue', label='Data Points')
plt.plot(x, model.predict(x), color='red',
         linewidth=2, label='Linear Regression Line')
plt.xlabel('Индекс розничных цен на продукты питания (x)')
plt.ylabel('Индекс промышленного производства (y)')
plt.legend()
plt.show()
